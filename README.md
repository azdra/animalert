<p align="center">
    <img src="front/public/static/logo_white_text.webp" alt="Animalert logo"/>
</p>

  <p align="center">Animalert est une plateforme en ligne dédiée à la recherche et à la récupération d'animaux de compagnie perdus. Son objectif principal est d'aider les propriétaires d'animaux à retrouver leurs compagnons disparus en facilitant la diffusion d'informations et en mobilisant la communauté locale
</p>


## Prérequis

Avant d'installer et d'utiliser ce projet, assurez-vous d'avoir les éléments suivants :

- Docker - https://www.docker.com/

## Installation

Suivez les étapes ci-dessous pour installer et configurer le projet :

#### 1. Clonez le projet depuis le dépôt GitLab :

```shell
$ git clone https://gitlab.com/azdra/animalert.git

$ cd animalert
```

#### 2. Installation de l'environnement de développement
```shell
$ cp .env.dist .env
$ docker pull azdracito/animalert-back:latest
$ docker pull azdracito/animalert-front:latest
```

## Exécution de l'application

```shell
$ make start
```

## Utilisation
L'application back-rend est accessible à l'adresse suivante : [http://localhost:3000](http://localhost:3000)

L'application front-end est accessible à l'adresse suivante : [http://localhost:3001](http://localhost:3001)

Maildev est accessible à l'adresse suivante : [http://localhost:3002](http://localhost:3002)

## Auteurs

Ce projet a été développé par [Déb](https://gitlab.com/deb-brunier) & [Baptiste](https://gitlab.com/azdra).

## Licence

Ce projet est sous licence [MIT](LICENSE). Vous pouvez consulter le fichier [LICENSE](LICENSE) pour plus de détails.
