start: stop pull
	@docker compose up

stop:
	@docker compose down

restart: stop start

migration-generate:
	@docker compose exec app npm run migration:generate

migration-run:
	@docker compose exec app npm run migration:run

migration-revert:
	@docker compose exec app npm run migration:revert

.PHONY: fixtures
fixtures:
	@docker compose exec app npm run fixtures

lint:
	@docker compose exec app npm run lint
	@docker compose exec app npm run format
	@docker compose exec front npm run lint

pull:
	@docker compose pull