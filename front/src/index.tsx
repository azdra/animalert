import 'bootstrap';
import React from 'react';
import {createRoot} from 'react-dom/client';
import '@scss/index.scss';
import Router from '@app/router/router';
import {Provider} from 'react-redux';
import {store} from '@app/store/store';
import {ToastContainer} from 'react-toastify';

const container = document.getElementById('root');
const root = createRoot(container!); // createRoot(container!) if you use TypeScript
root.render(
    <Provider store={store}>
      <Router/>
      <ToastContainer/>
    </Provider>,
);
