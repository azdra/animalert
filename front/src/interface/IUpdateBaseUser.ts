export interface IUpdateBaseUser {
  firstname: string | null;
  lastname: string | null;
  address: string | null;
}
