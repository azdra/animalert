export interface IPhoto {
  createdAt: Date
  id: number
  name: string
  path: string
  updatedAt: Date
}
