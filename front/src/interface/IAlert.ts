import {IAddress} from '@app/interface/IAddress';
import {EAnimalType} from '@app/enum/EAnimalType';
import {IPhoto} from '@app/interface/IPhoto';
import {EAlertCategory} from '@app/enum/EAlertCategory';

export interface IAlert {
  id: number;
  name: string | null;
  category: EAlertCategory;
  type: EAnimalType;
  breed: string | null;
  hair: string | null;
  color: string | null;
  description: string | null;
  status: string | null;
  trackNumber: string | null;
  createdAt: string | null;
  updatedAt: string | null;
  location: IAddress;
  photos: IPhoto[];
}
