export interface IApiAddress {
  city: string;
  citycode: string;
  context: string;
  distance: number;
  housenumber: string;
  id: string;
  importance: number;
  label: string;
  name: string;
  postcode: string;
  score: number;
  street: string;
  type: string;
  x: number;
  y: number;
}
