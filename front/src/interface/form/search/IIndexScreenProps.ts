import {EAlertCategory} from '@app/enum/EAlertCategory';
import {EAnimalType} from '@app/enum/EAnimalType';
import {EAlertStatus} from '@app/enum/EAlertStatus';

export interface IIndexScreenProps {
  trackNumber: string | null;
  category: EAlertCategory | null;
  type: EAnimalType | null;
  status: EAlertStatus | null;
  take: number
  page: number
}
