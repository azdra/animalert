import {IAlert} from '@app/interface/IAlert';

export interface ISearchAlertFormProps {
  refreshAlerts: (alerts: IAlert[]) => void;
}
