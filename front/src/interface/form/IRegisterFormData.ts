export interface IRegisterFormData {
  firstname: string
  lastname: string
  email: string
  password: string
  confirmPassword: string
  phone: string
  address: string
}
