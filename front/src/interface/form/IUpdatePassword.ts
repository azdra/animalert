export interface IUpdatePassword {
  oldPassword: string
  password: string
  confirmPassword: string
}
