export interface LoginFormData {
  username: string
  password: string
  failedLogin: string
}
