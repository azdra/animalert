export interface IAlertFormStepButtonsProps {
  previousStep: () => void;
  step: number
}
