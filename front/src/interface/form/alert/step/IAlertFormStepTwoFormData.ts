export interface IAlertFormStepTwoFormData {
  name: string
  breed: string | null
  type: string
  sterilized: boolean
}
