import {Control} from 'react-hook-form';
import {IAlertFormData} from '@app/interface/form/alert/IAlertFormData';
import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';

export interface IAlertMultipleFormData {
  nextStep: () => void;
  previousStep: () => void;
  control: Control<IAlertFormData>;
  errors: any;
  handleSubmit: any;
  step: number;
  view: ECreateAlertScreenView
}
