import {EAlertCategory} from '@app/enum/EAlertCategory';
import {EAnimalType} from '@app/enum/EAnimalType';
import {EAlertStatus} from '@app/enum/EAlertStatus';

export interface IAlertFormData {
  name: string | null;
  category: EAlertCategory
  type: EAnimalType ;
  breed: string | null;
  hair: string | null;
  color: string | null;
  description: string | null;
  status: EAlertStatus
  trackNumber: string | null;
  location: string;
  sex: string | null;
  sterilized: boolean;
  files: File[];
}
