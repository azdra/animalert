import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';

export interface IAlertFormBaseProps {
  view: ECreateAlertScreenView
}
