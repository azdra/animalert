import React from 'react';

export interface IAlertFormDescriptionProps {
  title: string,
  description?: string,
  children?: React.ReactNode
}
