import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';

export interface ICreateAlertScreenProps {
  view: ECreateAlertScreenView
}
