import {IAnimal} from '@app/interface/IAnimal';

export interface IAddMyAnimalForm extends IAnimal {
  files: []
}
