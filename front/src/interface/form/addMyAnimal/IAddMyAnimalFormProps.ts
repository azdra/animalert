import {IAnimal} from '@app/interface/IAnimal';

export interface IAddMyAnimalFormProps {
  cancel: () => void;
  defaultValues?: IAnimal;
}
