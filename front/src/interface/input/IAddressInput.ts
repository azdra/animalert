import {Control, FieldErrors} from 'react-hook-form';

export interface IAddressInput {
  control: Control<any>;
  errors: FieldErrors;
  name: string;
  defaultInputValue?: string;
}
