import {EAnimalType} from '@app/enum/EAnimalType';
import {EAnimalSex} from '@app/enum/EAnimalSex';

export interface IInputChoice<T> {
  onChange: (option: T) => void;
  register: any;
  error: any;
  defaultValue?: string;
}

export interface IInputChoiceWithName<T> extends IInputChoice<T> {
  name: string;
  options: IInputChoiceListOption[];
}

export interface IInputChoiceListOption {
  value: string | EAnimalType | EAnimalSex;
  label: string;
  icon: JSX.Element;
}

export interface IAnimalChoiceTypeListOption extends IInputChoiceListOption {
  value: EAnimalType
}

export interface IAnimalChoiceSexListOption extends IInputChoiceListOption {
  value: EAnimalSex
}
