import {EAnimalType} from '@app/enum/EAnimalType';
import {EAnimalSex} from '@app/enum/EAnimalSex';
import {IPhoto} from '@app/interface/IPhoto';
import {IAlert} from '@app/interface/IAlert';

export interface IAnimal {
  id?: number | undefined;
  name: string;
  type: EAnimalType
  breed: string | null;
  sex: EAnimalSex;
  sterilized: boolean;
  color: string | null;
  hair: string | null;
  trackNumber: string | null;
  description: string | null;
  weight: string | null;
  width: string | null;
  alerts: IAlert[];
  photos: IPhoto[];
}
