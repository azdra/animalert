import React from 'react';

export interface IBaseMyProfileProps {
  children: React.ReactNode;
}
