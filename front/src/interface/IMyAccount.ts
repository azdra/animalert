import {IUser} from '@app/interface/IUser';
import {IAnimal} from '@app/interface/IAnimal';

export interface IMyAccount extends IUser {
  animals: IAnimal[]
  alerts: {
    active: [],
    resolved: []
  }
}
