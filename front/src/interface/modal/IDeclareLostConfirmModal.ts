import {IAnimal} from '@app/interface/IAnimal';

export interface IDeclareLostConfirmModal {
  id: string;
  animal: IAnimal;
}
