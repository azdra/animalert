import {IAnimal} from '@app/interface/IAnimal';

export interface IRemoveAnimalConfirmModal {
  id: string;
  animal: IAnimal;
}
