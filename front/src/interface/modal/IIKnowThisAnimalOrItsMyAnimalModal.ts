import {IAlert} from '@app/interface/IAlert';

export interface IIKnowThisAnimalOrItsMyAnimalModal {
  alert: IAlert;
  id: string
}
