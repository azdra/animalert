import {IAddress} from '@app/interface/IAddress';
import {IUpdateBaseUser} from '@app/interface/IUpdateBaseUser';

export interface IUser extends IUpdateBaseUser {
  id: number | null
  email: string | null
  password: string | null
  phone: string | null
  access_token: string
  address: IAddress | null
  avatar: string | null
}
