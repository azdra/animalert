export interface IAddress {
  label: string;
  city: string;
  postcode: string;
  street: string;
  housenumber: string;
  latitude: number;
  longitude: number;
}
