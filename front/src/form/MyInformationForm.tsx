import React, {FC} from 'react';
import {useForm} from 'react-hook-form';
import {useAppSelector} from '@app/hooks';
import {updateMyAccountRequest} from '@app/api/userRequest';
import {IUpdateBaseUser} from '@app/interface/IUpdateBaseUser';
import {useNavigate} from 'react-router';
import {RouteConfig} from '@app/config/route.config';
import placeholderConfig from '@app/config/placeholder.config';
import {aToast} from '@app/assets/js/toast';
import AddressInput from '@components/ui/input/AddressInput';
import MyProfileSubmitButton from '@components/MyProfile/MyProfileSubmitButton';

const MyInformationForm: FC = () => {
  const {user} = useAppSelector((state) => state.user);
  const navigate = useNavigate();
  const {control, register, handleSubmit, formState: {errors}} = useForm<IUpdateBaseUser>({
    defaultValues: {
      firstname: user.firstname,
      lastname: user.lastname,
      address: user.address?.label,
    },
  });

  const submit = (data: IUpdateBaseUser) => {
    updateMyAccountRequest(data).then(() => {
      aToast.success('Votre compte a bien été mis à jour');
    }).catch((err) => {
      aToast.error(err.response.data.message);
    });
  };

  const back = () => {
    navigate(RouteConfig.myProfile.path);
  };

  return <form onSubmit={handleSubmit(submit)}>
    {/* FIRSTNAME AND LASTNAME */}
    <div className="form-group required mb-4 row">
      <div className="col-12 col-sm-6">
        <label htmlFor="lastname" className="form-label">Nom</label>
        <input placeholder={placeholderConfig.lastname} type="text" className={`form-control ${
          errors.lastname ? 'is-invalid' : ''
        }`} id="lastname" {...register('lastname', {required: true})} />
        {errors.lastname && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
      </div>
      <div className="col-12 col-sm-6 mt-4 mt-sm-0">
        <label htmlFor="firstname" className="form-label">Prénom</label>
        <input placeholder={'Charlène'} type="text" className={`form-control ${
          errors.firstname ? 'is-invalid' : ''
        }`} id="firstname" {...register('firstname', {required: true})} />
        {errors.firstname && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
      </div>
    </div>

    {/* ADDRESS */}
    <AddressInput control={control} errors={errors} name={'address'} defaultInputValue={user.address?.label}/>

    <div className="my-3"/>

    {/* SUBMIT */}
    <MyProfileSubmitButton back={back}/>
  </form>;
};

export default MyInformationForm;
