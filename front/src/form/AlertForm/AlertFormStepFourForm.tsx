import React, {FC, useEffect, useRef} from 'react';
import AlertFormStepButton from '@components/AlertForm/AlertFormStepButton';
import {IAlertMultipleFormData} from '@app/interface/form/alert/IAlertMultipleFormData';
import {useNavigate} from 'react-router';
import {IAlertFormData} from '@app/interface/form/alert/IAlertFormData';
import {createAlert} from '@app/api/alertRequest';
import {aToast} from '@app/assets/js/toast';
import {RouteConfig} from '@app/config/route.config';
import {alertSlicer} from '@app/store/alertStore';
import {useAppDispatch} from '@app/hooks';

const AlertFormStepFourForm: FC<IAlertMultipleFormData> = (props: IAlertMultipleFormData) => {
  const [image, setImage] = React.useState<File[]>([]);
  const inputFileRef = useRef();
  const navigation = useNavigate();
  const dispatch = useAppDispatch();
  const onSubmit = async (data: IAlertFormData) => {
    const form = new FormData();

    for (const [key, value] of Object.entries(data)) {
      if (value !== null && value !== undefined) {
        form.append(key, value);
      }
    }

    if (image) {
      for (let i = 0; i < image.length; i++) {
        form.append('files', image[i]);
      }
    }

    createAlert(form).then((res) => {
      aToast.success('Annonce créée avec succès');
      navigation(RouteConfig.home.path);
      dispatch(alertSlicer.actions.initAlerts(res));
    });
  };

  const onImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    props.control._formValues['files'] = [...e.target.files];
    setImage(props.control._formValues['files']);
  };

  useEffect(() => {
    setImage([...props.control._formValues['files']]);
  }, []);

  const removeImage = (index: number) => {
    image.splice(index, 1);
    setImage([...image]);
    props.control._formValues['files'] = [...image];
  };

  return <form onSubmit={props.handleSubmit(onSubmit)}>
    <div className="row row-cols-4 g-4 mb-4">
      {
        image && image.length > 0 && image.map((photo, index) => {
          return <div key={index} className="col col-my-animal-delete-image">
            <div className="card">
              <div className={'card-delete-image'} onClick={() => removeImage(index)}><i
                className="bi bi-trash"></i></div>
              <img src={URL.createObjectURL(photo)} height={'194px'} className="card-img-top object-fit-cover"
                alt="..." />
            </div>
          </div>;
        })
      }
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="files" className="form-label">Photos</label>
      <input {...props.control.register('files', {required: false, onChange: onImageChange})} ref={inputFileRef} type="file" multiple className={`form-control ${
        props.errors.files ? 'is-invalid' : ''
      }`} id="files" />
    </div>

    <AlertFormStepButton step={props.step} previousStep={props.previousStep} />
  </form>
  ;
};

export default AlertFormStepFourForm;
