import React, {FC} from 'react';
import {EAnimalType} from '@app/enum/EAnimalType';
import {IInputChoiceListOption} from '@app/interface/input/InputChoice';
import placeholderConfig from '@app/config/placeholder.config';
import AnimalChoiceType from '@components/ui/input/AnimalChoiceType';
import AnimalChoiceSex from '@components/ui/input/AnimalChoiceSex';
import AlertFormStepButton from '@components/AlertForm/AlertFormStepButton';
import {IAlertMultipleFormData} from '@app/interface/form/alert/IAlertMultipleFormData';
import {IAlertFormStepTwoFormData} from '@app/interface/form/alert/step/IAlertFormStepTwoFormData';
import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';

const AlertFormStepTwoForm: FC<IAlertMultipleFormData> = (props: IAlertMultipleFormData) => {
  const [type, setType] = React.useState<string | null>(props.control._formValues['type']);

  const onSubmit = (data: IAlertFormStepTwoFormData) => {
    props.nextStep();
  };

  const handleAnimalChange = (option: IInputChoiceListOption) => {
    props.control._formValues['type'] = option.value;
    setType(option.value);
  };

  const handleSexChange = (option: IInputChoiceListOption) => {
    props.control._formValues['sex'] = option.value;
  };

  return <form onSubmit={props.handleSubmit(onSubmit)}>
    {/* PET NAME */}
    <div className={`form-group mb-4 ${
      ECreateAlertScreenView.LOST_PET === props.view ? 'required' : ''
    }`}>
      <label htmlFor="phone" className="form-label">Nom de l'animal</label>
      <input type="tel" className={`form-control ${
        props.control.getFieldState('name').error ? 'is-invalid' : ''
      }`} id="phone" {...props.control.register('name', {required: ECreateAlertScreenView.LOST_PET === props.view})}
      placeholder={placeholderConfig.pet.name} />
      {props.control.getFieldState('name').error && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <div className="row row-cols-2">
      <div className="col-12 col-md">
        {/* PET TYPE */}
        <div className={'form-group required mb-4'}>
          <label htmlFor="type" className="form-label">Type d'animal</label>
          <AnimalChoiceType
            error={props.control.getFieldState('type').error}
            onChange={handleAnimalChange}
            // @ts-ignore
            register={props.control.register('type', {required: true})}
            defaultValue={props.control._formValues['type']}
          />
          {props.control.getFieldState('type').error &&
              <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
      <div className="col-12 col-md">
        {/* PET SEX */}
        <div className={`form-group mb-4 ${
          ECreateAlertScreenView.LOST_PET === props.view ? 'required' : ''
        }`}>
          <label htmlFor="type" className="form-label">Sexe de l'animal</label>
          <AnimalChoiceSex
            error={props.control.getFieldState('sex').error}
            onChange={handleSexChange}
            register={props.control.register('sex', {required: ECreateAlertScreenView.LOST_PET === props.view})}
            defaultValue={props.control._formValues['sex']}
          />
          {props.control.getFieldState('sex').error &&
              <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
    </div>

    {/* PET BREED */}
    {
      type && type !== EAnimalType.OTHER &&
        <>
          <div className={`form-group mb-4 ${
            ECreateAlertScreenView.LOST_PET === props.view ? 'required' : ''
          }`}>
            <label htmlFor="type" className="form-label">Race du {type === EAnimalType.DOG ? 'chien' : 'chat'}</label>
            <input type="text" className={`form-control ${
                  props.control.getFieldState('breed').error ? 'is-invalid' : ''
            }`} id="breed" {...props.control.register('breed', {required: ECreateAlertScreenView.LOST_PET === props.view})}
            placeholder={type === EAnimalType.CAT ? placeholderConfig.pet.breedCat : placeholderConfig.pet.breedDog} />
            {props.control.getFieldState('breed').error &&
                  <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
          </div>

          {
            props.view === ECreateAlertScreenView.LOST_PET && <div className="mb-4 form-check">
              <input className="form-check-input" type="checkbox"
                id="sterilized" {...props.control.register('sterilized', {required: false})} />
              <label className="form-check-label" htmlFor="sterilized">Stérilisé</label>
            </div>
          }
        </>
    }

    <AlertFormStepButton step={props.step} previousStep={props.previousStep} />
  </form>
  ;
};

export default AlertFormStepTwoForm;
