import React, {FC} from 'react';
import AddressInput from '@components/ui/input/AddressInput';
import AlertFormStepButton from '@components/AlertForm/AlertFormStepButton';
import {IAlertMultipleFormData} from '@app/interface/form/alert/IAlertMultipleFormData';

const AlertFormStepOne: FC<IAlertMultipleFormData> = (props: IAlertMultipleFormData) => {
  const onSubmit = () => {
    props.nextStep();
  };

  return <form onSubmit={props.handleSubmit(onSubmit)}>
    <AddressInput name={'location'} control={props.control} errors={props.errors} defaultInputValue={props.control._formValues['location']}/>
    <AlertFormStepButton step={props.step} previousStep={props.previousStep}/>
  </form>;
};

export default AlertFormStepOne;
