import React, {FC} from 'react';
import AlertFormStepButton from '@components/AlertForm/AlertFormStepButton';
import {IAlertMultipleFormData} from '@app/interface/form/alert/IAlertMultipleFormData';
import {IAlertFormStepTwoFormData} from '@app/interface/form/alert/step/IAlertFormStepTwoFormData';

const AlertFormStepThreeForm: FC<IAlertMultipleFormData> = (props: IAlertMultipleFormData) => {
  const onSubmit = (data: IAlertFormStepTwoFormData) => {
    props.nextStep();
  };

  return <form onSubmit={props.handleSubmit(onSubmit)}>
    <div className="mb-4 form-group">
      <label htmlFor="color" className="form-label">Couleur de poils</label>
      <input type="text" className={`form-control ${
        props.control.getFieldState('color').error ? 'is-invalid' : ''
      }`} id="color" aria-describedby="emailHelp" {...props.control.register('color', {required: false})}
      placeholder={'Noir'} />
      {props.control.getFieldState('color').error && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="hair" className="form-label">Type de poils</label>
      <input type="text" className={`form-control ${
        props.control.getFieldState('hair').error ? 'is-invalid' : ''
      }`} id="hair" aria-describedby="emailHelp" {...props.control.register('hair', {required: false})}
      placeholder={'Mi-long'} />
      {props.control.getFieldState('hair').error && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <AlertFormStepButton step={props.step} previousStep={props.previousStep} />
  </form>;
};

export default AlertFormStepThreeForm;
