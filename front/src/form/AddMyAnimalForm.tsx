import React, {FC, useEffect} from 'react';
import {useForm} from 'react-hook-form';
import {IAddMyAnimalForm} from '@app/interface/form/IAddMyAnimalForm';
import {useAppDispatch} from '@app/hooks';
import {animalSlicer} from '@app/store/animalStore';
import {aToast} from '@app/assets/js/toast';
import {IAddMyAnimalFormProps} from '@app/interface/form/addMyAnimal/IAddMyAnimalFormProps';
import {addMyAnimalRequest, removeImageFromAnimalRequest, updateMyAnimalRequest} from '@app/api/animalsRequest';
import AnimalChoiceType from '@components/ui/input/AnimalChoiceType';
import MyProfileSubmitButton from '@components/MyProfile/MyProfileSubmitButton';
import placeholderConfig from '@app/config/placeholder.config';
import AnimalChoiceSex from '@components/ui/input/AnimalChoiceSex';
import {
  IAnimalChoiceSexListOption,
  IAnimalChoiceTypeListOption,
} from '@app/interface/input/InputChoice';
import {IPhoto} from '@app/interface/IPhoto';
import {IAnimal} from '@app/interface/IAnimal';
import {EAnimalType} from '@app/enum/EAnimalType';

const AddMyAnimalForm: FC<IAddMyAnimalFormProps> = (props: IAddMyAnimalFormProps) => {
  const dispatch = useAppDispatch();
  const [image, setImage] = React.useState<IPhoto[]>([]);

  const {register, handleSubmit, formState: {errors}, setValue, watch} = useForm<IAddMyAnimalForm>({
    defaultValues: {
      id: undefined,
      name: undefined,
      hair: null,
      breed: null,
      trackNumber: null,
      sex: undefined,
      sterilized: false,
      weight: null,
      width: null,
      color: null,
      type: undefined,
      files: undefined,
    },
  });

  const typeWatch = watch('type', EAnimalType.OTHER);

  useEffect(() => {
    if (props.defaultValues) {
      for (const [key, value] of Object.entries(props.defaultValues)) {
        if (value !== null && value !== undefined) {
          setValue(key as keyof Partial<IAnimal>, value);
        }
      }


      setImage(props.defaultValues?.photos ?? []);
    }
  }, [props.defaultValues]);

  const addAnimalRequest = (form: FormData) => {
    addMyAnimalRequest(form).then((response) => {
      dispatch(animalSlicer.actions.initAnimals(response));
      aToast.success('Animal ajouté avec succès');
      props.cancel();
    }).catch(() => {
      aToast.error('Erreur lors de l\'ajout de l\'animal');
    });
  };

  const updateAnimalRequest = (form: FormData) => {
    if (props.defaultValues?.id) {
      updateMyAnimalRequest(props.defaultValues.id.toString(), form).then((response) => {
        dispatch(animalSlicer.actions.initAnimals(response));
        aToast.success('Animal modifié avec succès');
        props.cancel();
      }).catch(() => {
        aToast.error('Erreur lors de la modification de l\'animal');
      });
    }
  };

  const onSubmit = (data: IAddMyAnimalForm) => {
    const form = new FormData();

    for (const [key, value] of Object.entries(data)) {
      if (value !== null && value !== undefined) {
        form.append(key, value);
      }
    }

    if (data.files) {
      for (let i = 0; i < data.files.length; i++) {
        form.append('files', data.files[i]);
      }
    }

    if (props.defaultValues?.id) {
      updateAnimalRequest(form);
    } else {
      addAnimalRequest(form);
    }
  };

  const cancel = () => {
    props.cancel();
  };

  const handleAnimalChange = (option: IAnimalChoiceTypeListOption) => {
    setValue('type', option.value);
  };

  const handleSexChange = (option: IAnimalChoiceSexListOption) => {
    setValue('sex', option.value);
  };

  const removeImageFromAnimal = (photo: IPhoto) => {
    if (props.defaultValues?.id) {
      removeImageFromAnimalRequest(props.defaultValues.id.toString(), photo.id.toString()).then(() => {
        aToast.success('Photo supprimée avec succès');
        setImage(image.filter((p) => p.id !== photo.id));
        dispatch(animalSlicer.actions.removePhoto({animalId: props.defaultValues?.id, photoId: photo.id}));
      });
    }
  };

  const onChangeInputFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      const files = e.target.files;
      setImage([...image, ...files]);
    }
  };

  return <form onSubmit={handleSubmit(onSubmit)}>
    <div className="row">
      <div className="col-12 col-md">
        {/* PET TYPE */}
        <div className={'form-group required mb-4'}>
          <label htmlFor="type" className="form-label">Type d'animal</label>
          <AnimalChoiceType
            error={errors.type}
            onChange={handleAnimalChange}
            // @ts-ignore
            register={register('type', {required: true})}
            defaultValue={props.defaultValues?.type}
          />
          {errors.type && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
      <div className="col-12 col-md">
        {/* PET SEX */}
        <div className={'form-group required mb-4'}>
          <label htmlFor="type" className="form-label">Sexe de l'animal</label>
          <AnimalChoiceSex error={errors.sex} onChange={handleSexChange}
            register={register('sex', {required: true})} defaultValue={props.defaultValues?.sex} />
          {errors.type && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
    </div>

    <div className="row">
      <div className="col">
        <div className="mb-4 form-group required">
          <label htmlFor="name" className="form-label">Nom de l'animal</label>
          <input type="text" className={`form-control ${
            errors.name ? 'is-invalid' : ''
          }`} id="name" {...register('name', {required: true})}
          placeholder={placeholderConfig.pet.name} />
          {errors.name && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
      <div className="col">
        <div className="mb-4 form-group">
          <label htmlFor="trackNumber" className="form-label">Numéro d'identification</label>
          <input type="text" className={`form-control ${
            errors.trackNumber ? 'is-invalid' : ''
          }`} id="trackNumber" aria-describedby="trackNumberHelp" {...register('trackNumber', {required: false})}
          placeholder={placeholderConfig.pet.trackNumber} />
          {errors.trackNumber && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
          <div id="trackNumberHelp" className="form-text">Il est important de renseigner ce champ</div>
        </div>
      </div>
    </div>

    <div className="row">
      <div className="mb-4 form-group">
        <label htmlFor="race" className="form-label">Race</label>
        <input type="text" className={`form-control ${
          errors.breed ? 'is-invalid' : ''
        }`} id="race" aria-describedby="emailHelp" {...register('breed', {required: false})}
        placeholder={placeholderConfig.pet.breedCat} />
        {errors.breed && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
      </div>
    </div>

    <div className="row">
      <div className="col">
        <div className="mb-4 form-group">
          <label htmlFor="color" className="form-label">Couleur de poils</label>
          <input type="text" className={`form-control ${
            errors.color ? 'is-invalid' : ''
          }`} id="color" {...register('color', {required: false})}
          placeholder={placeholderConfig.pet.color} />
          {errors.color && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
      <div className="col">
        <div className="mb-4 form-group">
          <label htmlFor="hair" className="form-label">Type de poils</label>
          <input type="text" className={`form-control ${
            errors.hair ? 'is-invalid' : ''
          }`} id="hair" {...register('hair', {required: false})}
          placeholder={placeholderConfig.pet.hair} />
          {errors.hair && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
    </div>

    <div className="row">
      <div className="col">
        <div className="mb-4 form-group">
          <label htmlFor="weight" className="form-label">Poids</label>
          <input type="text" className={`form-control ${
            errors.weight ? 'is-invalid' : ''
          }`} id="weight" {...register('weight', {required: false})}
          placeholder={placeholderConfig.pet.weight} />
          {errors.weight && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
      <div className="col">
        <div className="mb-4 form-group">
          <label htmlFor="trackNumber" className="form-label">Taille</label>
          <input type="text" className={`form-control ${
            errors.width ? 'is-invalid' : ''
          }`} id="width" {...register('width', {required: false})}
          placeholder={placeholderConfig.pet.width} />
          {errors.width && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>
    </div>

    <div className="mb-4 form-check form-switch">
      <input className="form-check-input" type="checkbox" role="switch" id="sterilized" {...register('sterilized')} />
      <label className="form-check-label" htmlFor="sterilized">Stérilisé</label>
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="description" className="form-label">Description</label>
      <textarea className={`form-control ${
        errors.description ? 'is-invalid' : ''
      }`} id="description" aria-describedby="emailHelp" {...register('description', {required: false})}
      placeholder={typeWatch !== EAnimalType.OTHER ? (typeWatch === EAnimalType.DOG ? placeholderConfig.pet.dogDescription : placeholderConfig.pet.catDescription) : 'blablabla...'} />
      {errors.description && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="files" className="form-label">Photos</label>
      <input type="file" multiple className={`form-control ${
        errors.files ? 'is-invalid' : ''
      }`} id="files" {...register('files', {required: false, onChange: onChangeInputFile})} />
    </div>

    <div className="row row-cols-4 g-4 mb-4">
      {
        image.map((photo, index) => {
          return <div key={index} className="col col-my-animal-delete-image">
            <div className="card">
              <div className={'card-delete-image'} onClick={() => removeImageFromAnimal(photo)}><i
                className="bi bi-trash"></i></div>
              <div className={'ratio ratio-1x1'}>
                <img src={!photo.id ? URL.createObjectURL(photo) : `http://localhost:3000${photo.path}`}
                  className="w-100 h-100 object-fit-cover" alt="..." />
              </div>

            </div>
          </div>;
        })
      }
    </div>

    <MyProfileSubmitButton back={cancel} validText={props.defaultValues?.id ? 'Modifier' : 'Enregistrer'} />
  </form>;
};

export default AddMyAnimalForm;
