import React, {FC, useEffect} from 'react';
import placeholderConfig from '@app/config/placeholder.config';
import {EAlertCategory} from '@app/enum/EAlertCategory';
import {EAnimalType} from '@app/enum/EAnimalType';
import {EAlertStatus} from '@app/enum/EAlertStatus';
import {useForm} from 'react-hook-form';
import {IIndexScreenProps} from '@app/interface/form/search/IIndexScreenProps';
import {useAppDispatch, useAppSelector} from '@app/hooks';
import {resetSearch, searchInitialState, setFilter} from '@app/store/searchStore';
import * as bootstrap from 'bootstrap';

interface ISearchAlertFormProps {
  searchAlert: (data: IIndexScreenProps) => void;
}

const SearchAlertForm: FC<ISearchAlertFormProps> = (props: ISearchAlertFormProps) => {
  const dispatch = useAppDispatch();
  const filterForm = useAppSelector((state) => state.search.filter);
  const [modal, setModal] = React.useState<HTMLDivElement | null>(null);

  const {register, handleSubmit, reset, watch} = useForm<IIndexScreenProps>({
    values: filterForm,
  });

  useEffect(() => {
    const modalInstance = bootstrap.Offcanvas.getOrCreateInstance(document.getElementById('offcanvasExample') as HTMLElement);
    if (props.searchAlert && modalInstance) {
      props.searchAlert(filterForm);
      setModal(modalInstance);
    }
  }, []);

  const resetFilter = () => {
    reset();
    dispatch(resetSearch());
    props.searchAlert(searchInitialState);
    if (modal) {
      modal.hide();
    }
  };

  watch((data) => {
    dispatch(setFilter(data));
  });

  return (
    <form onSubmit={handleSubmit(props.searchAlert)}>
      <div className="mb-4 form-group">
        <label htmlFor="name" className="form-label">N° d'identification :</label>
        <input type="text" className={`form-control`} id="name"
          aria-describedby="emailHelp" {...register('trackNumber', {required: false})}
          placeholder={placeholderConfig.pet.trackNumber} />
      </div>

      <div className="mb-4">
        <label className="form-check-label">Type d'alerte :</label>
        <div className="row mt-2">
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAlertCategory.LOST}
                id="category_lost" {...register('category', {required: false})} />
              <label className="form-check-label" htmlFor="category_lost">Perdu</label>
            </div>
          </div>
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAlertCategory.FOUND}
                id="category_found" {...register('category', {required: false})} />
              <label className="form-check-label" htmlFor="category_found">Trouvé</label>
            </div>
          </div>
        </div>
        <div className="row my-2">
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAlertCategory.SEEN}
                id="category_seen" {...register('category', {required: false})} />
              <label className="form-check-label" htmlFor="category_seen">Vu</label>
            </div>
          </div>
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAlertCategory.STOLEN}
                id="category_stolen" {...register('category', {required: false})} />
              <label className="form-check-label" htmlFor="category_stolen">Volé</label>
            </div>
          </div>
        </div>
      </div>

      <div className="mb-4">
        <label className="form-check-label">Espèce :</label>
        <div className="row mt-2">
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAnimalType.CAT}
                id="type_cat" {...register('type', {required: false})} />
              <label className="form-check-label" htmlFor="type_cat">Chat</label>
            </div>
          </div>
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAnimalType.DOG}
                id="type_dog" {...register('type', {required: false})} />
              <label className="form-check-label" htmlFor="type_dog">Chien</label>
            </div>
          </div>
        </div>
        <div className="row my-2">
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAnimalType.OTHER}
                id="type_other" {...register('type', {required: false})} />
              <label className="form-check-label" htmlFor="type_other">Autre</label>
            </div>
          </div>
        </div>
      </div>

      <div className="mb-4">
        <label className="form-check-label">Catégorie :</label>
        <div className="row mt-2">
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAlertStatus.NEW}
                id="status_new" {...register('status', {required: false})} />
              <label className="form-check-label" htmlFor="status_new">En attente</label>
            </div>
          </div>
          <div className="col">
            <div className="form-check">
              <input type="radio" className="form-check-input" value={EAlertStatus.RESOLVED}
                id="status_resolved" {...register('status', {required: false})} />
              <label className="form-check-label" htmlFor="status_resolved">Résolu</label>
            </div>
          </div>
        </div>
      </div>

      <div className={'text-center d-flex flex-column'}>
        <button type={'submit'} className="btn btn-primary my-1 text-white">Appliquer les filtres</button>
        <button onClick={resetFilter} type={'button'} className="button-primary-link my-1 text-center">Réinitialler les
          filtres
        </button>
      </div>
    </form>
  );
};

export default SearchAlertForm;
