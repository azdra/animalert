import React, {FC} from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate} from 'react-router';
import {RouteConfig} from '@app/config/route.config';
import {useAppDispatch, useAppSelector} from '@app/hooks';
import {updateAccountEmailRequest} from '@app/api/userRequest';
import {aToast} from '@app/assets/js/toast';
import placeholderConfig from '@app/config/placeholder.config';
import {IMyAccountParameterEmailFormData} from '@app/interface/form/IMyAccountParameterEmailFormData';
import MyProfileSubmitButton from '@components/MyProfile/MyProfileSubmitButton';
import {userSlicer} from '@app/store/userStore';

const MyAccountParameterEmailForm: FC = () => {
  const {user} = useAppSelector((state) => state.user);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const {register, handleSubmit, formState: {errors}, setError} = useForm<IMyAccountParameterEmailFormData>({
    defaultValues: {
      email: user.email ?? '',
    },
  });

  const submit = (data: IMyAccountParameterEmailFormData) => {
    updateAccountEmailRequest(data.email).then(() => {
      navigate(RouteConfig.login.path);
      aToast.success('Email modifié avec succès');
      localStorage.removeItem('token');
      dispatch(userSlicer.actions.clearUser());
    }).catch((error) => {
      console.error(error);
      setError('email', {
        type: 'manual',
        message: 'Le mot de passe est incorrect',
      });
    });
  };

  const cancel = () => {
    navigate(RouteConfig.myProfileAccountParameters.path);
  };

  return <form onSubmit={handleSubmit(submit)}>
    {/* EMAIL */}
    <div className="form-group required mb-4">
      <label htmlFor="email" className="form-label">Adresse mail</label>
      <input type="email" className={`form-control ${
        errors.email ? 'is-invalid' : ''
      }`} id="email" {...register('email', {required: true})}
      placeholder={placeholderConfig.email} />
      {errors.email && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    {/* SUBMIT */}
    <MyProfileSubmitButton back={cancel} />
  </form>;
};

export default MyAccountParameterEmailForm;
