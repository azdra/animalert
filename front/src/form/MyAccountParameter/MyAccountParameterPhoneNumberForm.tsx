import React, {FC} from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate} from 'react-router';
import {RouteConfig} from '@app/config/route.config';
import {useAppDispatch, useAppSelector} from '@app/hooks';
import {updateAccountPhoneRequest} from '@app/api/userRequest';
import {aToast} from '@app/assets/js/toast';
import placeholderConfig from '@app/config/placeholder.config';
import {IMyAccountParameterPhoneNumberFormData} from '@app/interface/form/IMyAccountParameterPhoneNumberFormData';
import MyProfileSubmitButton from '@components/MyProfile/MyProfileSubmitButton';
import {userSlicer} from '@app/store/userStore';

const
  MyAccountParameterPhoneNumberForm: FC = () => {
    const {user} = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();

    const navigate = useNavigate();
    const {register, handleSubmit, formState: {errors}, setError} = useForm<IMyAccountParameterPhoneNumberFormData>({
      defaultValues: {
        phone: user.phone ?? '',
      },
    });

    const onSubmit = (data: IMyAccountParameterPhoneNumberFormData) => {
      updateAccountPhoneRequest(data.phone).then(() => {
        aToast.success('Numéro de téléphone modifié avec succès');
        dispatch(userSlicer.actions.updatePhoneNumber(data.phone));
      }).catch(() => {
        setError('phone', {
          type: 'manual',
          message: 'Le format du numéro de téléphone est incorrect',
        });
      });
    };

    const cancel = () => {
      navigate(RouteConfig.myProfileAccountParameters.path);
    };

    return <form onSubmit={handleSubmit(onSubmit)} >
      {/* phone */}
      <div className="form-group required mb-4">
        <label htmlFor="phone" className="form-label">N° de téléphone</label>
        <input type="tel" className={`form-control ${
          errors.phone ? 'is-invalid' : ''
        }`} id="phone" {...register('phone', {required: true})}
        placeholder={placeholderConfig.phone} />
        {errors.phone && <div className={'invalid-feedback'}>{errors.phone?.message ?? 'Ce champ est obligatoire'}</div>}
      </div>

      {/* SUBMIT */}
      <MyProfileSubmitButton back={cancel} />
    </form>;
  };

export default MyAccountParameterPhoneNumberForm;
