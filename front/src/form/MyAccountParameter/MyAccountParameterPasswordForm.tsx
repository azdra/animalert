import React, {FC, useState} from 'react';
import {IUpdatePassword} from '@app/interface/form/IUpdatePassword';
import {Controller, useForm} from 'react-hook-form';
import eye from '@images/eye.svg';
import eyeSlash from '@images/eye-slash.svg';
import {useNavigate} from 'react-router';
import {RouteConfig} from '@app/config/route.config';
import {updateAccountPasswordRequest} from '@app/api/userRequest';
import {aToast} from '@app/assets/js/toast';
import MyProfileSubmitButton from '@components/MyProfile/MyProfileSubmitButton';

const MyAccountParameterPasswordForm: FC = () => {
  const navigate = useNavigate();
  const {control, register, handleSubmit, watch, formState: {errors}, setError} = useForm<IUpdatePassword>({
    defaultValues: {
      oldPassword: '',
      password: '',
      confirmPassword: '',
    },
  });

  const [oldPasswordVisible, setOldPasswordVisible] = useState<boolean>(false);
  const [passwordVisible, setPasswordVisible] = useState<boolean>(false);
  const [confirmPasswordVisible, setConfirmPasswordVisible] = useState<boolean>(false);
  const passwordWatch = watch('password');

  const submit = (data: IUpdatePassword) => {
    updateAccountPasswordRequest(data).then(() => {
      aToast.success('Mot de passe modifié avec succès');
    }).catch((error) => {
      console.error(error);
      setError('oldPassword', {
        type: 'manual',
        message: 'Le mot de passe est incorrect',
      });
    });
  };

  const cancel = () => {
    navigate(RouteConfig.myProfileAccountParameters.path);
  };

  return <form onSubmit={handleSubmit(submit)}>
    {/* PASSWORD */}
    <div className={'form-group required mb-4'}>
      <label htmlFor="oldPassword" className="form-label">Votre ancien mot de passe</label>
      <Controller
        name="oldPassword"
        control={control}
        rules={{
          required: true,
          minLength: 8,
        }}
        render={({field}) => (
          <div className={'input-group has-validation'}>
            <input type={oldPasswordVisible ? 'text' : 'password'} {...field}
              className={`form-control rounded-0 rounded-start ${
                     errors.oldPassword ? 'is-invalid' : ''
              }`} id="oldPassword" placeholder={'*********'} minLength={8} autoComplete={'off'} />
            <button type={'button'} className="input-group-text"
              onClick={() => setOldPasswordVisible(!oldPasswordVisible)}>
              <img src={oldPasswordVisible ? eye : eyeSlash} alt="" width={16} height={16} />
            </button>
            {errors.oldPassword &&
              <div className={'invalid-feedback'}>{errors.oldPassword?.message || 'Ce champ est obligatoire'}</div>}
          </div>
        )}
      />
    </div>

    {/* PASSWORD */}
    <div className={'form-group required mb-4'}>
      <label htmlFor="password" className="form-label">Votre mot de passe</label>
      <Controller
        name="password"
        control={control}
        rules={{
          required: true,
          minLength: 8,
          pattern: {
            value: /^(?=.*[A-Z])(?=.*\d).{8,}$/,
            message: 'Votre mot de passe doit contenir au moins 8 caractères, une majuscule et un chiffre',
          },
        }}
        render={({field}) => (
          <div className={'input-group has-validation'}>
            <input type={passwordVisible ? 'text' : 'password'} {...field}
              className={`form-control rounded-0 rounded-start ${
                     errors.password ? 'is-invalid' : ''
              }`} id="password" placeholder={'*********'} minLength={8} autoComplete={'off'} />
            <button type={'button'} className="input-group-text" onClick={() => setPasswordVisible(!passwordVisible)}>
              <img src={passwordVisible ? eye : eyeSlash} alt="" width={16} height={16} />
            </button>
            {errors.password &&
              <div className={'invalid-feedback'}>{errors.password?.message || 'Ce champ est obligatoire'}</div>}
          </div>
        )}
      />

      <small className="text-secondary">Pour votre sécurité votre mot de passe doit remplir les critères
        suivants:</small>
      <div className={'d-flex'}>
        <div style={{width: '200px'}}>
          <span className={`d-flex align-items-center justify-content-start py-1 ${
            passwordWatch.length >= 8 ? 'text-success' : 'text-secondary'
          }`}>Minimum 8 caractères</span>

          <span className={`d-flex align-items-center justify-content-start py-1 ${
            /[A-Z]/.test(passwordWatch) ? 'text-success' : 'text-secondary'
          }`}>1 Majuscule</span>
        </div>

        <div style={{width: '200px'}}>
          <span className={`d-flex align-items-center justify-content-start py-1 ${
            /[a-z]/.test(passwordWatch) ? 'text-success' : 'text-secondary'
          }`}>1 Minuscule</span>

          <span className={`d-flex align-items-center justify-content-start py-1 ${
            /\d/.test(passwordWatch) ? 'text-success' : 'text-secondary'
          }`}>1 Chiffre</span>
        </div>
      </div>
    </div>


    {/* CONFIRM PASSWORD */}
    <div className="form-group required mb-4">
      <label htmlFor="confirmPassword" className="form-label">Confirmez votre mot de passe</label>
      <div className="input-group has-validation">
        <input type={confirmPasswordVisible ? 'text' : 'password'} className={`form-control rounded-0 rounded-start ${
          errors.confirmPassword ? 'is-invalid' : ''
        }`} id="confirmPassword" {...register('confirmPassword', {
          required: true,
          validate: (value) => value === passwordWatch || 'Les mots de passe ne correspondent pas',
        })}
        placeholder={'*********'} autoComplete={'off'} />
        <button type={'button'} className="input-group-text"
          onClick={() => setConfirmPasswordVisible(!confirmPasswordVisible)}>
          <img src={confirmPasswordVisible ? eye : eyeSlash} alt="" width={16} height={16} />
        </button>
        {errors.confirmPassword &&
          <div className={'invalid-feedback'}>{errors.confirmPassword?.message || 'Ce champ est obligatoire'}</div>}
      </div>
    </div>

    <MyProfileSubmitButton back={cancel} />
  </form>;
};

export default MyAccountParameterPasswordForm;
