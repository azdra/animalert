import React, {FC, useState} from 'react';
import {NavLink} from 'react-router-dom';
import {useForm, Controller} from 'react-hook-form';
import {IRegisterFormData} from '@app/interface/form/IRegisterFormData';
import {RouteConfig} from '@app/config/route.config';
import eye from '@images/eye.svg';
import eyeSlash from '@images/eye-slash.svg';
import placeholderConfig from '@app/config/placeholder.config';
import {registerRequest} from '@app/api/authRequest';
import {aToast} from '@app/assets/js/toast';
import {useNavigate} from 'react-router';
import AddressInput from '@components/ui/input/AddressInput';

const RegisterForm: FC = () => {
  const navigate = useNavigate();
  const {setError, control, register, handleSubmit, watch, formState: {errors}} = useForm<IRegisterFormData>({
    defaultValues: {
      firstname: undefined,
      lastname: undefined,
      phone: undefined,
      email: undefined,
      password: undefined,
      confirmPassword: undefined,
    },
  });
  const [passwordVisible, setPasswordVisible] = useState<boolean>(false);
  const [confirmPasswordVisible, setConfirmPasswordVisible] = useState<boolean>(false);
  const passwordWatch = watch('password', '');

  const onSubmit = (data: IRegisterFormData) => {
    registerRequest(data).then(() => {
      navigate(RouteConfig.login.path);
      aToast.success('Inscription réussie, vous pouvez maintenant vous connecter');
    }).catch(() => {
      setError('root.failedRegister', {
        type: 'manual',
        message: 'Une erreur est survenue lors de l\'inscription, veuillez réessayer plus tard.',
      });
    });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={'card p-4 shadow'}>
      <div className="pb-1">
        <h3 className="pb-2">Nouveau sur Animalert ?</h3>
        <p className={'pb-0'}>Inscrivez-vous pour continuer</p>
      </div>

      {/* FIRSTNAME AND LASTNAME */}
      <div className="form-group required mb-4 row">
        <div className="col">
          <label htmlFor="lastname" className="form-label">Nom</label>
          <input placeholder={placeholderConfig.lastname} type="text" className={`form-control ${
            errors.lastname ? 'is-invalid' : ''
          }`} id="lastname" {...register('lastname', {required: true})} />
          {errors.lastname && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
        <div className="col">
          <label htmlFor="firstname" className="form-label">Prénom</label>
          <input placeholder={placeholderConfig.firstname} type="text" className={`form-control ${
            errors.firstname ? 'is-invalid' : ''
          }`} id="firstname" {...register('firstname', {required: true})} />
          {errors.firstname && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      </div>

      {/* PHONE NUMBER */}
      <div className="form-group required mb-4">
        <label htmlFor="phone" className="form-label">N° de téléphone</label>
        <input type="tel" className={`form-control ${
          errors.phone ? 'is-invalid' : ''
        }`} id="phone" {...register('phone', {required: true})}
        placeholder={placeholderConfig.phone} />
        {errors.phone && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
      </div>

      {/* EMAIL */}
      <div className="form-group required mb-4">
        <label htmlFor="email" className="form-label">Adresse mail</label>
        <input type="email" className={`form-control ${
          errors.email ? 'is-invalid' : ''
        }`} id="email" {...register('email', {required: true})}
        placeholder={placeholderConfig.email} />
        {errors.email && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
      </div>

      {/* PASSWORD */}
      <div className={'form-group required mb-4'}>
        <label htmlFor="password" className="form-label">Votre mot de passe</label>
        <Controller
          name="password"
          control={control}
          rules={{
            required: true,
            minLength: 8,
            pattern: {
              value: /^(?=.*[A-Z])(?=.*\d).{8,}$/,
              message: 'Votre mot de passe doit contenir au moins 8 caractères, une majuscule et un chiffre',
            },
          }}
          render={({field}) => (
            <div className={'input-group has-validation'}>
              <input type={passwordVisible ? 'text' : 'password'} {...field} className={`form-control rounded-0 rounded-start ${
                errors.password ? 'is-invalid' : ''
              }`} id="password" placeholder={placeholderConfig.password} minLength={8} autoComplete={'off'} />
              <button type={'button'} className="input-group-text" onClick={() => setPasswordVisible(!passwordVisible)}>
                <img src={passwordVisible ? eye : eyeSlash} alt="" width={16} height={16} />
              </button>
              {errors.password &&
                <div className={'invalid-feedback'}>{errors.password?.message || 'Ce champ est obligatoire'}</div>}
            </div>
          )}
        />

        <small className="text-secondary">Pour votre sécurité votre mot de passe doit remplir les critères suivants:</small>
        <div className={'d-flex'}>
          <div style={{width: '200px'}}>
            <span className={`d-flex align-items-center justify-content-start py-1 ${
            passwordWatch.length >= 8 ? 'text-success' : 'text-secondary'
            }`}>Minimum 8 caractères</span>

            <span className={`d-flex align-items-center justify-content-start py-1 ${
              /[A-Z]/.test(passwordWatch) ? 'text-success' : 'text-secondary'
            }`}>1 Majuscule</span>
          </div>

          <div style={{width: '200px'}}>
            <span className={`d-flex align-items-center justify-content-start py-1 ${
              /[a-z]/.test(passwordWatch) ? 'text-success' : 'text-secondary'
            }`}>1 Minuscule</span>

            <span className={`d-flex align-items-center justify-content-start py-1 ${
              /\d/.test(passwordWatch) ? 'text-success' : 'text-secondary'
            }`}>1 Chiffre</span>
          </div>
        </div>
      </div>


      {/* CONFIRM PASSWORD */}
      <div className="form-group required mb-4">
        <label htmlFor="confirmPassword" className="form-label">Confirmez votre mot de passe</label>
        <div className="input-group has-validation">
          <input type={confirmPasswordVisible ? 'text' : 'password'} className={`form-control rounded-0 rounded-start ${
            errors.confirmPassword ? 'is-invalid' : ''
          }`} id="confirmPassword" {...register('confirmPassword', {required: true, validate: (value) => value === passwordWatch || 'Les mots de passe ne correspondent pas'})}
          placeholder={placeholderConfig.password} autoComplete={'off'} />
          <button type={'button'} className="input-group-text" onClick={() => setConfirmPasswordVisible(!confirmPasswordVisible)}>
            <img src={confirmPasswordVisible ? eye : eyeSlash} alt="" width={16} height={16} />
          </button>
          {errors.confirmPassword && <div className={'invalid-feedback'}>{errors.confirmPassword?.message || 'Ce champ est obligatoire'}</div>}
        </div>
      </div>

      {/* ADDRESS */}
      <AddressInput control={control} errors={errors} name={'address'} />

      <div className="my-3"/>

      {/* FAILED REGISTER */}
      {errors.root?.failedRegister && <div className={'alert alert-danger'}>{errors.root.failedRegister.message}</div>}

      <button type="submit" className="btn btn-primary py-2 text-light">S'inscrire</button>

      <div className="d-flex flex-row pt-5 align-self-center align-items-center">
        <span>Vous avez déjà un compte ?</span>
        <NavLink className="ms-2" to={RouteConfig.login.path}>Se connecter</NavLink>
      </div>

      <small className={'text-center mt-2'}>En cliquant sur Connexion, vous acceptez nos <a href="">Conditions
        d'utilisation</a> et notre <a
        href="">Politique de confidentialité</a>. Nous ne transmettons jamais vos coordonnées à des tiers.</small>
    </form>
  );
};

export default RegisterForm;
