import React, {FC, useState} from 'react';
import {NavLink} from 'react-router-dom';
import {RouteConfig} from '@app/config/route.config';
import {useForm} from 'react-hook-form';
import {LoginFormData} from '@app/interface/form/ILoginFormData';
import {loginRequest} from '@app/api/authRequest';
import {useAppDispatch} from '@app/hooks';
import {userSlicer} from '@app/store/userStore';
import placeholderConfig from '@app/config/placeholder.config';
import {aToast} from '@app/assets/js/toast';
import {useNavigate} from 'react-router';
import eye from '@images/eye.svg';
import eyeSlash from '@images/eye-slash.svg';
import {animalSlicer} from '@app/store/animalStore';
import {alertSlicer} from '@app/store/alertStore';

const LoginForm: FC = () => {
  const {register, handleSubmit, formState: {errors}, setError, watch, clearErrors} = useForm<LoginFormData>();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [passwordVisible, setPasswordVisible] = useState<boolean>(false);

  const onSubmit = (data: LoginFormData) => {
    loginRequest(data.username, data.password).then((res) => {
      dispatch(userSlicer.actions.setUser(res));
      dispatch(animalSlicer.actions.initAnimals(res.animals));
      dispatch(alertSlicer.actions.initAlerts(res.alerts));
      localStorage.setItem('token', res.access_token);
      aToast.success('Vous êtes connecté, bienvenue !');
      navigate(RouteConfig.home.path);
    }).catch(() => {
      setError('failedLogin', {
        type: 'manual',
        message: 'Identifiant ou mot de passe incorrect',
      });
    });
  };

  watch(() => {
    if (errors.failedLogin) {
      clearErrors('failedLogin');
    }
  });

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="card p-4 shadow">

      <div className="pb-1">
        <h3 className="pb-2">Se connecter</h3>
        <p className={'pb-0'}>Connectez-vous pour gérer les profils de vos animaux.</p>
      </div>

      <div className="my-3 form-group required">
        <label htmlFor="email" className="form-label">Adresse électronique ou numéro de
          téléphone</label>
        <input type="text" className={`form-control ${
          errors.username ? 'is-invalid' : ''
        }`} id="email" aria-describedby="emailHelp" {...register('username', {required: true})}
        placeholder={placeholderConfig.emailOrPhone} />
        <small id="emailHelp" className="form-text">Nous ne communiquerons jamais votre adresse électronique ou
          votre numéro de téléphone à qui que ce soit.</small>
        {errors.username && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
      </div>

      <div className="my-3 form-group required">
        <label htmlFor="password" className="form-label">Mot de passe</label>
        <div className={'input-group has-validation'}>
          <input type={passwordVisible ? 'text' : 'password'} {...register('password', {required: true})}
            className={`form-control rounded-0 rounded-start ${
                   errors.password ? 'is-invalid' : ''
            }`} id="password" placeholder={placeholderConfig.password} minLength={8} autoComplete={'off'} />
          <button type={'button'} className="input-group-text" onClick={() => setPasswordVisible(!passwordVisible)}>
            <img src={passwordVisible ? eye : eyeSlash} alt="" width={16} height={16} />
          </button>
          {errors.password &&
            <div className={'invalid-feedback'}>{errors.password?.message || 'Ce champ est obligatoire'}</div>}
        </div>
      </div>

      <div className={'d-flex justify-content-between my-3 mb-4'}>
        <div className="form-check">
          <input type="checkbox" className="form-check-input" id="check" />
          <label className="form-check-label" htmlFor="check">Rester connecté</label>
        </div>
        <div className="form-check">
          <NavLink to={RouteConfig.login.path}>Mot de passe oublié ?</NavLink>
        </div>
      </div>

      {errors.failedLogin &&
        <div className={'text-center mt-3 mb-4 d-block invalid-feedback'}>Identifiant ou mot de passe incorrect</div>}

      <button type="submit" className="btn btn-primary py-2 text-light">Se connecter</button>

      <div className="d-flex flex-column flex-md-row pt-5 align-self-center align-items-center mb-2 mb-md-0">
        <span>Vous n'avez pas encore de compte ?</span>
        <NavLink className="ms-2" to={RouteConfig.register.path}>Créez-en un</NavLink>
      </div>

      <small className={'text-center mt-2'}>En cliquant sur Connexion, vous acceptez nos <a href="">Conditions
        d'utilisation</a> et notre <a
        href="">Politique de confidentialité</a>. Nous ne transmettons jamais vos coordonnées à des tiers.</small>
    </form>
  );
};

export default LoginForm;
