import React from 'react';

const LostPetForm = () => {
  return (
    <div>
      /front/src/form/LostPetForm.tsx
    </div>
  );
};

export default LostPetForm;

/*
import React, {FC, useState} from 'react';
import {EAlertCategory} from '@app/enum/EAlertCategory';
import {Controller, useForm} from 'react-hook-form';
import {EAnimalType} from '@app/enum/EAnimalType';
import {EAlertStatus} from '@app/enum/EAlertStatus';
import {createAlert} from '@app/api/alertRequest';
import {getAddress} from '@app/api/addressRequest';
import AsyncSelect from 'react-select/async';

export interface LostPetFormProps {
  category: EAlertCategory
}

export interface LostPetFormData {
  name: string | null;
  category: EAlertCategory
  type: EAnimalType | null;
  breed: string | null;
  hair: string | null;
  color: string | null;
  description: string | null;
  status: EAlertStatus
  trackNumber: string | null;
  location: string | null;
  // photos
}

const LostPetForm: FC<LostPetFormProps> = (props: LostPetFormProps) => {
  const [timeoutSearch, setTimeoutSearch] = useState<any>(null);

  const {register, control, handleSubmit, formState: {errors}} = useForm<LostPetFormData>({
    defaultValues: {
      name: null, // no required
      category: props.category, // required
      type: null, // required
      breed: null,
      hair: null,
      color: null,
      description: null,
      status: EAlertStatus.NEW,
      trackNumber: null,
      location: null,
      // photos
      // TODO ADD LOCATION AND PHOTOS
    },
  });

  const loadOptions = (
      inputValue: string,
      callback: (options: any[]) => void,
  ) => {
    if (inputValue.length < 3) return;
    if (timeoutSearch) clearTimeout(timeoutSearch);
    const timeout = setTimeout(() => {
      getAddress(inputValue).then((res) => {
        callback(res.map((item: any) => ({value: item.id, label: item.label})));
      });
    }, 750);
    setTimeoutSearch(timeout);
  };

  const submit = (data: LostPetFormData) => {
    // TODO REQUEST
    // @ts-ignore
    data.location = data.location.label;
    console.log(data);
    createAlert(data).then((response) => {
      console.log(response);
    });
  };

  return <form onSubmit={handleSubmit(submit)}>

    <div className="pb-1">
      <p className={'pb-0'}>{props.category}</p>
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="name" className="form-label">Nom</label>
      <input type="text" className={`form-control ${
        errors.name ? 'is-invalid' : ''
      }`} id="name" aria-describedby="emailHelp" {...register('name')}
      placeholder={'Kiera'} />
      {errors.name && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <div className={'mb-4 form-group required'}>
      <label htmlFor="name" className="form-label">Type</label>
      <div className={'is-invalid'}>
        <div className="form-check form-check-inline">
          <input className={`form-check-input ${
            errors.type ? 'is-invalid' : ''
          }`} type="radio"
          id="type_dog" {...register('type', {required: true})}
          value={EAnimalType.DOG} />
          <label className="form-check-label" htmlFor="type_dog">Chien</label>
        </div>
        <div className="form-check form-check-inline">
          <input className={`form-check-input ${
            errors.type ? 'is-invalid' : ''
          }`} type="radio"
          id="type_cat" {...register('type', {required: true})}
          value={EAnimalType.CAT} />
          <label className="form-check-label" htmlFor="type_cat">Chat</label>
        </div>
        <div className="form-check form-check-inline">
          <input className={`form-check-input ${
            errors.type ? 'is-invalid' : ''
          }`} type="radio" id="type_other"
          value={EAnimalType.OTHER} {...register('type', {required: true})} />
          <label className="form-check-label" htmlFor="type_other">Autre</label>
        </div>
        {errors.type && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
      </div>
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="race" className="form-label">Race</label>
      <input type="text" className={`form-control ${
        errors.race ? 'is-invalid' : ''
      }`} id="race" aria-describedby="emailHelp" {...register('race')}
      placeholder={'Kiera'} />
      {errors.race && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="hair" className="form-label">hair</label>
      <input type="text" className={`form-control ${
        errors.hair ? 'is-invalid' : ''
      }`} id="hair" aria-describedby="emailHelp" {...register('hair')}
      placeholder={'Kiera'} />
      {errors.hair && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="color" className="form-label">color</label>
      <input type="text" className={`form-control ${
        errors.color ? 'is-invalid' : ''
      }`} id="color" aria-describedby="emailHelp" {...register('color')}
      placeholder={'Kiera'} />
      {errors.color && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="description" className="form-label">Description</label>
      <textarea className={`form-control ${
        errors.description ? 'is-invalid' : ''
      }`} id="description" aria-describedby="emailHelp" {...register('description', {required: false})}
      placeholder={'blablabla...'} />
      {errors.description && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    <div className="mb-4 form-group">
      <label htmlFor="trackNumber" className="form-label">trackNumber</label>
      <input type="text" className={`form-control ${
        errors.trackNumber ? 'is-invalid' : ''
      }`} id="trackNumber" aria-describedby="emailHelp" {...register('trackNumber')}
      placeholder={'Kiera'} />
      {errors.trackNumber && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
    </div>

    {/!* ADDRESS *!/}
    <div className="form-group required mb-4">
      <label htmlFor="confirmPassword" className="form-label">Adresse</label>
      <Controller
        name="location"
        control={control}
        rules={{required: true}}
        render={({field}) => (
          <div className={'has-validation'}>
            <AsyncSelect classNamePrefix="react-select" className={`react-select__container ${
              errors.location ? 'is-invalid' : ''
            }`} {...field} cacheOptions loadOptions={loadOptions}
            placeholder={'6 Rue Irvoy, 38000, Grenoble'}
            noOptionsMessage={() => 'Entrez 3 caractères pour commencer la recherche'}
            loadingMessage={() => 'Chargement ...'} />
            {errors.location && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
          </div>
        )}
      />
    </div>

    <div>
      <button type="submit" className="btn btn-primary">Créer mon alert</button>
    </div>
  </form>;
};

export default LostPetForm;
*/
