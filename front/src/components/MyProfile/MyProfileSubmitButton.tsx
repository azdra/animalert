import React, {FC} from 'react';
import {IMyProfileSubmitButton} from '@app/interface/IMyProfileSubmitButton';

const MyProfileSubmitButton: FC<IMyProfileSubmitButton> = (props: IMyProfileSubmitButton) => {
  return <div className={'d-flex flex-sm-row flex-column-reverse'}>
    <button type="button" onClick={props.back} className="button-primary-link mt-2 mt-sm-0">Annuler</button>
    <button type="submit" className="btn btn-primary py-2 text-white flex-grow-1 ms-0 ms-sm-2">{props.validText ?? 'Modifier'}</button>
  </div>;
};

export default MyProfileSubmitButton;
