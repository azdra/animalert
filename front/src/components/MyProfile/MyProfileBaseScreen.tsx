import React, {FC} from 'react';
import Navbar from '@components/Navbar/Navbar';
import {IBaseMyProfileProps} from '@app/interface/IBaseMyProfileProps';

const MyProfileBaseScreen: FC<IBaseMyProfileProps> = (props: IBaseMyProfileProps) => {
  return (
    <>
      <Navbar/>
      <main className={'container mt-2'}>
        <div className="row">
          {props.children}
        </div>
      </main>
    </>
  );
};

export default MyProfileBaseScreen;
