import React, {FC} from 'react';

interface InfoProfileNoDataProps {
  title: string;
  icon: string;
}

const InfoProfileNoData: FC<InfoProfileNoDataProps> = (props: InfoProfileNoDataProps) => {
  return <div style={{height: '200px'}} className={'item-container d-flex align-items-center justify-content-center border rounded position-relative bg-white flex-column'}>
    <i className={`item-icon text-primary fs-1 position-relative top-0 text-warning ${props.icon}`}></i>
    <span className={'fw-bold mt-3 text-center'}>{props.title}</span>
  </div>;
};

export default InfoProfileNoData;
