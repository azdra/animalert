import React, {FC} from 'react';
import {useAppDispatch, useAppSelector} from '@app/hooks';
import {updateAccountAvatarRequest} from '@app/api/userRequest';
import {userSlicer} from '@app/store/userStore';

const InfoProfile: FC = () => {
  const {user} = useAppSelector((state) => state.user);
  const [avatar, setAvatar] = React.useState<string>(`http://localhost:3000/${user.avatar}`);
  const inputUploadFile = React.useRef<HTMLInputElement>(null);
  const dispatch = useAppDispatch();

  const changeImageProfile = () => {
    inputUploadFile.current?.click();
  };

  const onChangeImageProfile = (e: React.ChangeEvent<HTMLInputElement>) => {
    const formData = new FormData();
    formData.append('file', e.target.files![0]);
    updateAccountAvatarRequest(formData).then((res) => {
      dispatch(userSlicer.actions.updateAvatar(res.avatar));
      setAvatar(`http://localhost:3000/${res.avatar}`);
    });
  };

  return (
    <>
      <main className="mt-4">
        <div className="d-flex w-100 justify-content-center align-items-start flex-column py-2">
          <div className={'mx-auto text-center'}>
            <div className={'upload-profile-image-parent mx-auto rounded-circle overflow-hidden'}>
              <div className="upload-profile-image" onClick={changeImageProfile}>
                <i className="bi bi-upload"></i>
                <input type="file" name="file" id="file" hidden={true} ref={inputUploadFile} onChange={onChangeImageProfile} />
              </div>
              <img src={avatar} className="img-fluid" alt="Avatar" />
            </div>
            <span className="text-uppercase fw-bold d-block mt-2">{user.firstname + ' ' + user.lastname}</span>
          </div>
        </div>
      </main>
    </>
  );
};

export default InfoProfile;
