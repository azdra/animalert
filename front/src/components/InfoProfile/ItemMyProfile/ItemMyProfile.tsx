import {NavLink} from 'react-router-dom';
import React, {FC} from 'react';
import './ItemMyProfile.scss';

interface ItemMyProfileProps {
  title: string;
  subtitle?: string | undefined;
  button: string;
  url: string;
  icon: string
}

const ItemMyProfile: FC<ItemMyProfileProps> = (props: ItemMyProfileProps) => {
  return <NavLink to={props.url} className="item-container text-decoration-none d-flex align-items-start align-items-md-center justify-content-between border rounded position-relative bg-white flex-column flex-md-row">
    <div className={'col-12 col-md'}>
      <i className={`item-icon ${props.icon}`}></i>
      <div className="item-title text-start ms-5">
        <span className="d-flex items-center space-x-8">{props.title}</span>
        {props.subtitle && <span className="item-subtitle d-flex mt-2 text-body-secondary">{props.subtitle}</span>}
      </div>
    </div>
    <div className={'col-12 col-md'}>
      <div className="item-content mt-3 mt-md-0 d-flex justify-content-end">
        <div className={'button-link'}>
          <button type="button" className="button-primary-link" title={props.button}>{props.button}</button>
        </div>
      </div>
    </div>
  </NavLink>;
};

export default ItemMyProfile;
