import React, {FC} from 'react';
import './ItemMyProfile.scss';

interface ActionItemMyProfileProps {
  title: string;
  button?: string;
  action?: () => void;
}

const ActionItemMyProfile: FC<ActionItemMyProfileProps> = (props: ActionItemMyProfileProps) => {
  return <div onClick={props.action} style={{cursor: 'pointer'}} className="item-container text-decoration-none d-flex align-items-center justify-content-between border rounded position-relative bg-white">
    <div className={`d-flex w-100 flex-column flex-md-row justify-content-between align-items-center`}>
      <div className="item-title text-start">
        <span className="d-flex items-center space-x-8">{props.title}</span>
      </div>
      {
        props.button && <div className="item-content d-flex flex-row align-items-center justify-content-between mt-2 mt-md-0">
          <div className={'button-link'}>
            <button type="button" className="button-primary-link" title={props.button}> {props.button} </button>
          </div>
        </div>
      }
    </div>
  </div>;
};

export default ActionItemMyProfile;
