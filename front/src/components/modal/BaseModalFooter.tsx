import React, {FC} from 'react';

interface IBaseModalFooter {
  children: React.ReactNode
}

const BaseModalFooter: FC<IBaseModalFooter> = (props: IBaseModalFooter) => {
  return (
    <div className="modal-footer">
      {props.children}
    </div>
  );
};

export default BaseModalFooter;
