import React, {FC} from 'react';
import BaseConfirmModal from '@components/modal/confirm/BaseConfirmModal';
import BaseModalBody from '@components/modal/BaseModalBody';
import {IIKnowThisAnimalOrItsMyAnimalModal} from '@app/interface/modal/IIKnowThisAnimalOrItsMyAnimalModal';
import {responseToAnAlert} from '@app/api/alertRequest';
import {useAppSelector} from '@app/hooks';

const IKnowThisAnimalOrItsMyAnimalModal: FC<IIKnowThisAnimalOrItsMyAnimalModal> = (props: IIKnowThisAnimalOrItsMyAnimalModal) => {
  const {user} = useAppSelector((state) => state.user);
  const accept = () => {
    if (user.id) {
      responseToAnAlert(props.alert).then(() => {
        console.log('ok');
      });
    }
  };

  return (
    <BaseConfirmModal id={props.id} title={`Je connais cet animal / C'est mon animal`} accept={accept}>
      <BaseModalBody>
        <p>Un email sera envoyé à l'utilisateur qui a posté l'alerte pour l'informer de votre réponse avec vos informations personnelles tel que votre email, numéro de téléphone ainsi que votre nom et prénom.</p>

        <div className="mb-3 form-check">
          <input type="checkbox" className="form-check-input" id="exampleCheck1" name={'exampleCheck1'} />
          <label className="form-check-label" htmlFor="exampleCheck1">En cochant cette case, vous confirmez que vous connaissez cet animal ou que c'est le vôtre.</label>
        </div>
      </BaseModalBody>
    </BaseConfirmModal>
  );
};

export default IKnowThisAnimalOrItsMyAnimalModal;
