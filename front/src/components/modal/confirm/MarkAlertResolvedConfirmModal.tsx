import React, {FC} from 'react';
import BaseConfirmModal from '@components/modal/confirm/BaseConfirmModal';
import BaseModalBody from '@components/modal/BaseModalBody';

interface IMarkAlertResolvedConfirmModal {
  id: string;
  accept: () => void;
}

const MarkAlertResolvedConfirmModal: FC<IMarkAlertResolvedConfirmModal> = (props: IMarkAlertResolvedConfirmModal) => {
  const accept = () => props.accept();

  return (
    <BaseConfirmModal id={props.id} title={`Validation d'action`} accept={accept}>
      <BaseModalBody>
        Êtes-vous sûr de vouloir marqué cette alerte comme résolue ?
      </BaseModalBody>
    </BaseConfirmModal>
  );
};

export default MarkAlertResolvedConfirmModal;
