import React, {FC} from 'react';
import BaseConfirmModal from '@components/modal/confirm/BaseConfirmModal';
import BaseModalBody from '@components/modal/BaseModalBody';
import {aToast} from '@app/assets/js/toast';
import {deleteMyAnimal} from '@app/api/animalsRequest';
import {animalSlicer} from '@app/store/animalStore';
import {useAppDispatch} from '@app/hooks';
import {IRemoveAnimalConfirmModal} from '@app/interface/modal/IRemoveAnimalConfirmModal';

const RemoveAnimalConfirmModal: FC<IRemoveAnimalConfirmModal> = (props: IRemoveAnimalConfirmModal) => {
  const dispatch = useAppDispatch();

  const accept = () => {
    deleteMyAnimal(props.animal.id.toString()).then((res) => {
      dispatch(animalSlicer.actions.initAnimals(res));
      aToast.success(`${props.animal.name} a été retiré de votre liste.`);
    }).catch((error) => {
      aToast.error(error.response.data.message);
    });
  };

  return (
    <BaseConfirmModal id={props.id} title={`Retirer ${props.animal.name} de mes animaux ?`} accept={accept}>
      <BaseModalBody>
        Êtes-vous sûr de vouloir retirer <b>{props.animal.name}</b> de votre liste d'animaux ?
      </BaseModalBody>
    </BaseConfirmModal>
  );
};

export default RemoveAnimalConfirmModal;
