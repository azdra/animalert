import React, {FC} from 'react';
import BaseConfirmModal from '@components/modal/confirm/BaseConfirmModal';
import BaseModalBody from '@components/modal/BaseModalBody';
import {aToast} from '@app/assets/js/toast';
import {createAlertFromAnimal} from '@app/api/animalsRequest';
import {IDeclareLostConfirmModal} from '@app/interface/modal/IDeclareLostConfirmModal';
import {useAppDispatch} from '@app/hooks';
import {animalSlicer} from '@app/store/animalStore';
import {alertSlicer} from '@app/store/alertStore';

const DeclareLostConfirmModal: FC<IDeclareLostConfirmModal> = (props: IDeclareLostConfirmModal) => {
  const dispatch = useAppDispatch();
  const accept = () => {
    if (props.animal.id) {
      createAlertFromAnimal(props.animal.id.toString()).then((res) => {
        dispatch(animalSlicer.actions.initAnimals(res.animals));
        dispatch(alertSlicer.actions.initAlerts(res.alerts));
        aToast.success(`L'animal ${props.animal.name} a été déclaré perdu.`);
      }).catch((error) => {
        aToast.error(error.response.data.message);
      });
    }
  };

  return (
    <BaseConfirmModal id={props.id} title={`Déclarer ${props.animal.name} perdu ?`} accept={accept}>
      <BaseModalBody>
        Êtes-vous sûr de vouloir déclarer cet animal comme perdu ?
      </BaseModalBody>
    </BaseConfirmModal>
  );
};

export default DeclareLostConfirmModal;
