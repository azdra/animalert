import React, {FC} from 'react';
import BaseModalFooter from '@components/modal/BaseModalFooter';
import BaseModal from '@components/modal/BaseModal';
import {Modal} from 'bootstrap';

interface IBaseConfirmModal {
  id: string;
  title: string;
  children: React.ReactNode
  accept: () => void;
  refuse?: () => void;
}

const BaseConfirmModal: FC<IBaseConfirmModal> = (props: IBaseConfirmModal) => {
  const accept = () => {
    try {
      if (props.accept) {
        props.accept();
        const modal = Modal.getOrCreateInstance(document.getElementById(props.id) as HTMLElement);
        modal.hide();
      }
    } catch (e) {
      console.error(e);
    }
  };
  const refuse = () => props.refuse && props.refuse();

  return (
    <BaseModal id={props.id} title={props.title}>
      {props.children}
      <BaseModalFooter>
        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={refuse}>Non</button>
        <button type="button" className="btn btn-primary text-white" onClick={accept}>Oui</button>
      </BaseModalFooter>
    </BaseModal>
  );
};

export default BaseConfirmModal;
