import React, {FC} from 'react';

interface IBaseModalBody {
  children: React.ReactNode
}

const BaseModalBody: FC<IBaseModalBody> = (props: IBaseModalBody) => {
  return (
    <div className="modal-body">
      {props.children}
    </div>
  );
};

export default BaseModalBody;
