import React, {FC} from 'react';

interface IBaseModal {
  id: string;
  title: string;
  children: React.ReactNode
}

const BaseModal: FC<IBaseModal> = (props: IBaseModal) => {
  return (
    <div className="modal fade" id={props.id} aria-labelledby={props.id+'Label'} data-bs-backdrop="static" data-bs-keyboard="false" aria-hidden="true">
      <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id={props.id+'Label'}>{props.title}</h5>
            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          {props.children}
        </div>
      </div>
    </div>
  );
};

export default BaseModal;
