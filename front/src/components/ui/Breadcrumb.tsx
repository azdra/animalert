import React, {FC} from 'react';
import {NavLink} from 'react-router-dom';

interface BreadcrumbProps {
  elements: {name: string, link?: string}[];
}

const Breadcrumb: FC<BreadcrumbProps> = (props: BreadcrumbProps) => {
  return <nav aria-label="breadcrumb">
    <ol className="breadcrumb flex-column flex-md-row mb-2 mb-md-0">
      {
        props.elements.map((element, index) => {
          return <li key={index} className="breadcrumb-item fs-7 active">
            {
              index < props.elements.length - 1 ?
                <NavLink className={'text-decoration-none fw-bold'} to={element.link??''}>{element.name}</NavLink> :
                <span className={'fw-bold'}>{element.name}</span>
            }
          </li>;
        })
      }
    </ol>
  </nav>;
};

export default Breadcrumb;
