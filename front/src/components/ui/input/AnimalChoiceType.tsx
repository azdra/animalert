import React, {FC} from 'react';
import CatIcon from '@components/Icon/CatIcon';
import DogIcon from '@components/Icon/DogIcon';
import OtherIcon from '@components/Icon/OtherIcon';
import {EAnimalType} from '@app/enum/EAnimalType';
import InputChoice from '@components/ui/input/InputChoice/InputChoice';
import {IAnimalChoiceTypeListOption, IInputChoice} from '@app/interface/input/InputChoice';

const AnimalChoiceType: FC<IInputChoice<IAnimalChoiceTypeListOption>> = ({error, onChange, register, defaultValue}: IInputChoice<IAnimalChoiceTypeListOption>) => {
  const options: IAnimalChoiceTypeListOption[] = [
    {value: EAnimalType.CAT, label: 'Chat', icon: <CatIcon/>},
    {value: EAnimalType.DOG, label: 'Chien', icon: <DogIcon/>},
    {value: EAnimalType.OTHER, label: 'Autre', icon: <OtherIcon/>},
  ];

  return <InputChoice name={'type'} error={error} onChange={onChange} register={register} options={options} defaultValue={defaultValue} />;
};

export default AnimalChoiceType;
