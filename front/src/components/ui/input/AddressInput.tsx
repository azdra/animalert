import React, {FC, useEffect, useRef} from 'react';
import {getAddress, getAddressByCoordinates} from '@app/api/addressRequest';
import {Controller} from 'react-hook-form';
import AsyncSelect from 'react-select/async';
import placeholderConfig from '@app/config/placeholder.config';
import Select from 'react-select/base';
import {ISelect} from '@app/interface/ISelect';
import {IApiAddress} from '@app/interface/IApiAddress';
import {IAddressInput} from '@app/interface/input/IAddressInput';
import {ActionMeta} from 'react-select';


const AddressInput: FC<IAddressInput> = ({name, control, errors, defaultInputValue}: IAddressInput) => {
  const [timeoutSearch, setTimeoutSearch] = React.useState<NodeJS.Timeout | null>(null);
  const asyncRef = useRef<Select>(null);
  const [address, setAddress] = React.useState<ISelect[]>([]);

  useEffect(() => {
    if (address && address.length) {
      asyncRef.current?.onMenuOpen();
    }
  }, [address]);

  const useCurrentLocation = () => {
    navigator.geolocation.getCurrentPosition((position) => {
      getAddressByCoordinates(position.coords.latitude, position.coords.longitude).then((response) => {
        setAddress(response.map((item: IApiAddress) => ({value: item.id, label: item.label})));
      });
    });
  };

  const loadOptions = (
      inputValue: string,
      callback: (options: any[]) => void,
  ) => {
    if (inputValue.length < 3) return;
    if (timeoutSearch) clearTimeout(timeoutSearch);
    const timeout = setTimeout(() => {
      getAddress(inputValue).then((res) => {
        callback(res.map((item: IApiAddress) => ({value: item.id, label: item.label})));
      });
    }, 750);
    setTimeoutSearch(timeout);
  };

  const onInputChange = (newValue: { label: string, value: string }, actionMeta: ActionMeta<unknown>): void => {
    control._formValues[name] = newValue.label;
  };

  return <div className="form-group required">
    <label htmlFor="confirmPassword" className="form-label">Adresse</label>

    <Controller
      name={name}
      control={control}
      rules={{required: true}}
      render={({field}) => (
        <div className={'has-validation'}>
          <AsyncSelect {...field} ref={asyncRef} classNamePrefix="react-select" className={`react-select__container ${
            errors[name] ? 'is-invalid' : ''
          }`} cacheOptions defaultOptions={address} loadOptions={loadOptions} onChange={onInputChange} defaultInputValue={defaultInputValue}
          placeholder={placeholderConfig.address}
          noOptionsMessage={() => 'Entrez 3 caractères pour commencer la recherche'}
          loadingMessage={() => 'Chargement ...'} />
          {errors[name] && <div className={'invalid-feedback'}>Ce champ est obligatoire</div>}
        </div>
      )}
    />

    <button type={'button'} className={'btn btn-link ps-0'} onClick={useCurrentLocation}>
      <i className="bi bi-geo-alt-fill"></i>
      Utiliser la position actuel
    </button>
  </div>;
};

export default AddressInput;
