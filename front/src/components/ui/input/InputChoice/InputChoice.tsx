import React, {FC, useEffect, useState} from 'react';
import './InputChoice.scss';
import {IInputChoiceListOption, IInputChoiceWithName} from '@app/interface/input/InputChoice';

const InputChoice: FC<IInputChoiceWithName> = ({name, onChange, register, error, defaultValue, options}: IInputChoiceWithName) => {
  const [selectedOption, setSelectedOption] = useState<string | null>(null);
  const [errorState, setErrorState] = useState(false);

  const handleOptionChange = (option: IInputChoiceListOption) => {
    setSelectedOption(option.value);
    onChange(option);
    if (error && errorState) {
      setErrorState(false);
    }
  };

  useEffect(() => {
    if (defaultValue) {
      setSelectedOption(defaultValue);
    }
  }, [defaultValue]);

  useEffect(() => {
    if (error) {
      setErrorState(true);
    }
  }, [error]);

  return (
    <ul className={`animal_choice_type_container row row-cols-1 row-cols-md-3 g-4 ${
      errorState ? 'is-invalid' : ''
    }`}>
      {options.map((option, index) => (
        <li className={'animal_choice_type_items'} key={index}>
          <label htmlFor={option.value}>
            <input
              type="radio"
              id={option.value}
              name={name}
              value={option.value}
              checked={option.value === selectedOption}
              onChange={() => handleOptionChange(option)}
              {...register}
            />
            <button
              type="button"
              className={`w-100 h-100 button-primary-link ${
                option.value === selectedOption ? 'active' : ''
              } ${
                errorState ? 'is-invalid' : ''
              }`}
              onClick={() => handleOptionChange(option)}
            >
              <div className={'d-flex flex-column justify-content-between h-100'}>
                {option.icon}
                <span>{option.label}</span>
              </div>
            </button>
          </label>
        </li>
      ))}
    </ul>
  );
};

export default InputChoice;
