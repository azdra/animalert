import React, {FC} from 'react';
import InputChoice from '@components/ui/input/InputChoice/InputChoice';
import {IAnimalChoiceSexListOption, IInputChoice} from '@app/interface/input/InputChoice';
import {EAnimalSex} from '@app/enum/EAnimalSex';
import FemaleIcon from '@components/Icon/FemaleIcon';
import MaleIcon from '@components/Icon/MaleIcon';

const AnimalChoiceSex: FC<IInputChoice<IAnimalChoiceSexListOption>> = ({error, onChange, register, defaultValue}: IInputChoice<IAnimalChoiceSexListOption>) => {
  const options: IAnimalChoiceSexListOption[] = [
    {value: EAnimalSex.FEMALE, label: 'Female', icon: <FemaleIcon/>},
    {value: EAnimalSex.MALE, label: 'Male', icon: <MaleIcon/>},
  ];

  return <InputChoice name={'sex'} error={error} onChange={onChange} register={register} options={options} defaultValue={defaultValue} />;
};

export default AnimalChoiceSex;
