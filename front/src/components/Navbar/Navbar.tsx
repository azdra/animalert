import React, {FC} from 'react';
import './navbar.scss';
import {NavLink} from 'react-router-dom';
import {RouteConfig} from '@app/config/route.config';
import {useAppDispatch, useAppSelector} from '@app/hooks';
import {clearUser} from '@app/store/userStore';
import {aToast} from '@app/assets/js/toast';

const Navbar: FC = () => {
  const user = useAppSelector((state) => state.user.user);
  const dispatch = useAppDispatch();
  const disconnect = () => {
    localStorage.removeItem('token');
    dispatch(clearUser());
    aToast.success('Vous êtes déconnecté');
  };

  return (
    <div className={'navbar-container bg-light border shadow-sm px-2 px-sm-0'}>
      <nav className="container navbar navbar-expand-lg navbar-light">
        <NavLink className="navbar-brand" to={RouteConfig.home.path}>
          <img src='/static/logo-test.png' alt="Logo" height="40" className="d-inline-block align-middle"/> Animalert
        </NavLink>

        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink className="nav-link mx-1" to={RouteConfig.foundPet.path}>J'ai trouvé un animal</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link mx-1" to={RouteConfig.lostPet.path}>J'ai perdu un animal</NavLink>
            </li>
            {/* <li className="nav-item">*/}
            {/*  <NavLink className="nav-link mx-1" to={RouteConfig.search.path}>Rechercher</NavLink>*/}
            {/* </li>*/}
            {
              user.id ? (
                <>
                  <li className="nav-item">
                    <NavLink className="nav-link mx-1" to={RouteConfig.myProfile.path}>Mon compte</NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink className="nav-link nav-link-button mx-1 bg-primary rounded text-white px-2"
                      onClick={disconnect}
                      to={RouteConfig.home.path}>Se déconnecter</NavLink>
                  </li>
                </>
              ) : (
                <li className="nav-item">
                  <NavLink className="nav-link nav-link-button mx-1 bg-primary rounded text-white px-2"
                    to={RouteConfig.login.path}>Se connecter</NavLink>
                </li>
              )
            }
          </ul>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
