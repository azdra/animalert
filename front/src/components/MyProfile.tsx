import React, {FC} from 'react';
import ItemMyProfile from '@components/InfoProfile/ItemMyProfile/ItemMyProfile';
import {RouteConfig} from '@app/config/route.config';

const MyProfile: FC = () => {
  return (
    <div className={'container'}>
      <div className={'my-4'}>
        <div className={'text-body-secondary text-uppercase'}>Mon Profil</div>
        <ItemMyProfile title={'Mes informations personnelles'} subtitle={'Gérer mes informations : nom, prénom, adresse'}
          button={'Editer mes informations personnelles'} url={RouteConfig.myProfileEdit.path}
          icon={'bi bi-person'}
        />

        <ItemMyProfile title={'Mes animaux'} subtitle={'Gérer mes animaux'}
          button={'Editer les informations de mes animaux'} url={RouteConfig.myProfileAnimals.path}
          icon={'bi bi-journal'} />

        <ItemMyProfile title={'Mes Alertes'} subtitle={'Gérer mes alertes'}
          button={'Voir mes alertes'} url={RouteConfig.myProfileAlerts.path}
          icon={'bi bi-exclamation-triangle'} />
      </div>

      <div className={'my-4'}>
        <div className={'text-body-secondary text-uppercase'}>Animalert</div>
        <ItemMyProfile title={'Mes paramètres de compte'} subtitle={'Gérer mes paramètres de connexion et de contact'}
          button={'Editer mes paramètres de compte'} url={RouteConfig.myProfileAccountParameters.path}
          icon={'bi bi-gear'} />
      </div>
    </div>
  );
};

export default MyProfile;
