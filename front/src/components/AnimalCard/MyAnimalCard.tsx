import React, {FC} from 'react';
import './AnimalCard.scss';
import {IAnimal} from '@app/interface/IAnimal';
import {hasActiveAlert} from '@app/store/animalStore';
import {EAnimalType} from '@app/enum/EAnimalType';
import CatIcon from '@components/Icon/CatIcon';
import DogIcon from '@components/Icon/DogIcon';
import OtherIcon from '@components/Icon/OtherIcon';
import DeclareLostConfirmModal from '@components/modal/confirm/DeclareLostConfirmModal';
import RemoveAnimalConfirmModal from '@components/modal/confirm/RemoveAnimalConfirmModal';
import {useNavigate} from 'react-router';
import {generatePath} from 'react-router-dom';
import {RouteConfig} from '@app/config/route.config';


const MyAnimalCard: FC<IAnimal> = (props: IAnimal) => {
  const navigate = useNavigate();
  const animalTypeIcon = (type: EAnimalType) => {
    switch (type) {
      case EAnimalType.CAT:
        return <CatIcon/>;
      case EAnimalType.DOG:
        return <DogIcon/>;
      default:
        return <OtherIcon/>;
    }
  };

  const animalImage = () => {
    if (props.photos.length > 0) {
      return <img src={`http://localhost:3000/${props.photos[0].path}`} className="card-img-top card-animal-thumbnail" alt="Animal 1" height={'250px'} onClick={onClick} />;
    }
    return <div className={'card-img-top card-animal-thumbnail no-preview'} onClick={onClick}>
      {animalTypeIcon(props.type)}
    </div>;
  };

  const onClick = () => {
    navigate(generatePath(RouteConfig.editMyAnimal.path, {id: props.id}));
  };

  return <div className="col mb-4">
    <div className="card card-animal">
      {
        props.photos && props.photos.length > 0 && <div className="type-tag">
          {animalTypeIcon(props.type)}
        </div>
      }
      {animalImage()}
      <div className="card-body card-animal-body">
        <h5 className="card-title">{props.name}</h5>
        <div className={'d-flex flex-column flex-xxl-row justify-content-between'}>
          <button type="button" disabled={hasActiveAlert(props)} data-bs-toggle="modal" data-bs-target={`#declare_lost_animal_${props.id}`} className={`btn btn-primary btn-sm text-white mb-2 mb-xxl-0 ${
            hasActiveAlert(props) ? 'disabled' : ''
          }`}>Déclarer perdu</button>
          <button type={'button'} disabled={hasActiveAlert(props)} data-bs-toggle="modal" data-bs-target={`#remove_animal_${props.id}`} className={`button-primary-link btn-sm ${
            hasActiveAlert(props) ? 'disabled' : ''
          }`}>Retirer</button>
        </div>
      </div>
    </div>
    <DeclareLostConfirmModal id={`declare_lost_animal_${props.id}`} animal={props}/>
    <RemoveAnimalConfirmModal id={`remove_animal_${props.id}`} animal={props}/>
  </div>;
};

export default MyAnimalCard;
