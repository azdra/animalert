import React, {FC} from 'react';
import './AnimalCard.scss';
import {getAlertName} from '@app/store/alertStore';
import {IAlert} from '@app/interface/IAlert';
import {EAnimalType} from '@app/enum/EAnimalType';
import CatIcon from '@components/Icon/CatIcon';
import DogIcon from '@components/Icon/DogIcon';
import OtherIcon from '@components/Icon/OtherIcon';
import {EAlertCategory} from '@app/enum/EAlertCategory';
import IKnowThisAnimalOrItsMyAnimalModal from '@components/modal/confirm/IKnowThisAnimalOrItsMyAnimalModal';
import {useAppSelector} from '@app/hooks';
import {useNavigate} from 'react-router';
import {RouteConfig} from '@app/config/route.config';

const AlertCard: FC<IAlert> = (props: IAlert) => {
  const {user} = useAppSelector((state) => state.user);
  const navigate = useNavigate();
  const animalTypeIcon = (type: EAnimalType) => {
    switch (type) {
      case EAnimalType.CAT:
        return <CatIcon/>;
      case EAnimalType.DOG:
        return <DogIcon/>;
      default:
        return <OtherIcon/>;
    }
  };

  const animalImage = () => {
    if (props.photos.length > 0) {
      return <img src={`http://localhost:3000/${props.photos[0].path}`} className="card-img-top card-animal-thumbnail" alt="Animal 1" height={'250px'} />;
    }
    return <div className={'card-img-top card-animal-thumbnail no-preview'}>
      {animalTypeIcon(props.type)}
    </div>;
  };

  const categoryHumanized = (category: EAlertCategory) => {
    switch (category) {
      case EAlertCategory.FOUND:
        return 'Trouvé';
      case EAlertCategory.LOST:
        return 'Perdu';
      case EAlertCategory.SEEN:
        return 'Aperçu';
      case EAlertCategory.STOLEN:
        return 'Volé';
      default:
        return 'Autre';
    }
  };


  return <div className={'col'}>
    <div className="card card-animal">
      {
        <div className="type-tag">
          {animalTypeIcon(props.type)}
        </div>
      }
      {animalImage()}
      <div className="card-body card-animal-body">
        <div className={`category-tag ${props.category}`}><small>{categoryHumanized(props.category)}</small></div>
        <h6 className="card-title mb-0 fw-bold border-bottom">{getAlertName(props)}</h6>
        <p className="card-text fs-7 mb-0 mt-2">
          <i className="bi bi-geo-fill"></i>
          <span className={'ms-1 fw-bold'}>{props.location.city}</span>
        </p>

        <div className={'d-flex flex-column flex-xxl-row justify-content-between mt-2'}>
          {
            user.id ? (
              <button type={'button'} className={`button-primary-link btn-sm`} data-bs-toggle="modal"
                data-bs-target={`#i_know_this_animal_or_its_my_animal_${props.id}`}>
                Je connais cet animal / C’est mon animal
              </button>
            ) : (
              <button type={'button'} className={`button-primary-link btn-sm`} onClick={() => navigate(RouteConfig.login.path)} >
                Connectez-vous pour prendre contact
              </button>
            )
          }
        </div>
      </div>
      <IKnowThisAnimalOrItsMyAnimalModal alert={props} id={`i_know_this_animal_or_its_my_animal_${props.id}`} />
    </div>
  </div>;
};

export default AlertCard;
