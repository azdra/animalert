import React, {FC} from 'react';
import InfoProfileNoData from '@components/InfoProfile/InfoProfileNoData';
import MyAnimalCard from '@components/AnimalCard/MyAnimalCard';
import {useAppSelector} from '@app/hooks';

interface MyAnimalsListProps {
  showAddForm: () => void;
}

const MyAnimalsList: FC<MyAnimalsListProps> = (props: MyAnimalsListProps) => {
  const {animals} = useAppSelector((state) => state.animals);

  return <div className="border py-3 px-4 rounded">
    <div className="d-flex flex-row justify-content-between align-items-center mb-3">
      <span className="fw-bold text-body-secondary">Mes animaux</span>
      <button type="button" className="btn btn-primary btn-sm text-white text-decoration-none fw-bold"
        onClick={props.showAddForm} title="Ajouter">
        <small>Ajouter</small>
      </button>
    </div>
    <div>
      {
        animals.length === 0 &&
        <InfoProfileNoData title={'Vous n\'avez pas encore de bestiaux enregistrés'}
          icon={'bi bi-journal'} />
      }
      {
        animals.length >= 1 && <div className="container">
          <div className="row row-cols-1 row-cols-md-5 g-4">
            {
              animals.map((animal, index) => {
                return <MyAnimalCard key={index} {...animal} />; // <AnimalCard type={animal.type} name={animal.name} view={AnimalCardView.MY_ANIMALS} />
              })
            }
          </div>
        </div>
      }
    </div>
  </div>;
};

export default MyAnimalsList;
