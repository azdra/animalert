import React, {FC} from 'react';
import FilterSearchOffCanvas from '@components/FilterSearchOffcanvas/FilterSearchOffCanvas';

const FilterSearch: FC = () => {
  return (
    <>
      <div className={'d-flex mb-2'}>
        <button className="button-primary-link w-100 me-2" type="button" data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasExample"
          aria-controls="offcanvasExample">
          <i className="bi bi-filter me-2"></i>
          Filtrer
        </button>

        <button className="button-primary-link w-100 ms-2" type="button" disabled data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasExample"
          aria-controls="offcanvasExample">
          Trier
        </button>
      </div>

      <FilterSearchOffCanvas/>
    </>
  );
};

export default FilterSearch;
