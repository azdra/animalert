import React, {FC} from 'react';
import SearchAlertForm from '@app/form/SearchAlertForm';

const FilterSearchOffCanvas: FC = () => {
  return <div className="offcanvas offcanvas-start w-100" tabIndex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
    <div className="offcanvas-header border-bottom text-center">
      <h5 className="offcanvas-title text-center mx-auto" id="offcanvasExampleLabel"><i className="bi bi-filter"></i> Filtrer
      </h5>
      <button type="button" className="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div className="offcanvas-body">
      <SearchAlertForm />
    </div>
  </div>;
};

export default FilterSearchOffCanvas;
