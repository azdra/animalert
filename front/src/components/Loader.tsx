import React, {FC} from 'react';

const Loader: FC = () => {
  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div className={'spinner-grow text-primary'}></div>
    </div>
  );
};

export default Loader;
