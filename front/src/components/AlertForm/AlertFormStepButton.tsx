import React, {FC} from 'react';
import {IAlertFormStepButtonsProps} from '@app/interface/form/alert/IAlertFormStepButtonsProps';

const AlertFormStepButton: FC<IAlertFormStepButtonsProps> = (props: IAlertFormStepButtonsProps) => {
  return <div className="d-flex flex-sm-row flex-column-reverse mt-4">
    <button disabled={props.step === 0} className={`button-primary-link mt-2 mt-sm-0 ${
      props.step === 0 ? 'disabled' : ''
    }`} type="button" onClick={props.previousStep}>Précédent</button>
    <button type={'submit'} className="btn btn-primary py-2 text-light flex-grow-1 ms-0 ms-sm-2">{props.step === 3 ? 'Enregistrer' : 'Suivant'}</button>
  </div>;
};

export default AlertFormStepButton;
