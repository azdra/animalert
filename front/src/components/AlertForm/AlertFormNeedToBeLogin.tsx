import React, {FC} from 'react';
import Navbar from '@components/Navbar/Navbar';
import {Link} from 'react-router-dom';
import {RouteConfig} from '@app/config/route.config';
import AlertFormDescription from '@components/AlertForm/AlertFormDescription';

const AlertFormNeedToBeLogin: FC = () => {
  return <div className={'d-flex flex-column min-vh-100 lost-pet-screen-container'}>
    <Navbar />
    <div className="d-flex flex-column h-100 flex-grow-1">
      <main className={'container my-auto'}>
        <div className="row">
          <div className="col-12">
            <AlertFormDescription title="Vous devez être connecté pour accéder à cette page">
              <span>Pour ce faire, vous devez vous <Link to={RouteConfig.login.path}>connecter</Link> ou si vous n'avez pas de compte, vous <Link
                to={RouteConfig.register.path}>inscrire</Link></span>
            </AlertFormDescription>
          </div>
        </div>
      </main>
    </div>
  </div>;
};

export default AlertFormNeedToBeLogin;
