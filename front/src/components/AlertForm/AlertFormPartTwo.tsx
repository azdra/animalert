import React, {FC} from 'react';
import AlertFormDescription from '@components/AlertForm/AlertFormDescription';
import AlertFormStepTwoForm from '@app/form/AlertForm/AlertFormStepTwoForm';
import {IAlertMultipleFormData} from '@app/interface/form/alert/IAlertMultipleFormData';
import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';

const AlertFormPartOne: FC<IAlertMultipleFormData> = (props: IAlertMultipleFormData) => {
  const description = () => {
    switch (props.view) {
      case ECreateAlertScreenView.LOST_PET:
        return <AlertFormDescription title={'Informations sur l\'animal perdu'} description={'Veuillez fournir les détails suivants concernant votre animal perdu : son nom et son espèce. Si l\'espèce de votre animal n\'est pas répertoriée, veuillez sélectionner \'Autre\'.'} />;
      case ECreateAlertScreenView.FOUND_PET:
        return <AlertFormDescription title={'Information sur l\'animal trouvé'} description={'Veuillez fournir les détails suivants concernant l\'animal trouvé : son nom et son espèce. Si l\'espèce de votre animal n\'est pas répertoriée, veuillez sélectionner \'Autre\'.'} />;
      default:
        return '';
    }
  };

  return (
    <>
      {description()}
      <AlertFormStepTwoForm step={props.step} handleSubmit={props.handleSubmit} control={props.control} errors={props.errors} nextStep={props.nextStep} previousStep={props.previousStep} view={props.view}/>
    </>
  );
};

export default AlertFormPartOne;
