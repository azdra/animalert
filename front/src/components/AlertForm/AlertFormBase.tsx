import React, {FC} from 'react';
import {useForm} from 'react-hook-form';
import Navbar from '@components/Navbar/Navbar';
import {EAlertCategory} from '@app/enum/EAlertCategory';
import AlertFormPartOne from '@components/AlertForm/AlertFormPartOne';
import AlertFormPartTwo from '@components/AlertForm/AlertFormPartTwo';
import AlertFormPartThree from '@components/AlertForm/AlertFormPartThee';
import {IAlertFormData} from '@app/interface/form/alert/IAlertFormData';
import {IAlertFormBaseProps} from '@app/interface/form/alert/IAlertFormBaseProps';
import {EAlertStatus} from '@app/enum/EAlertStatus';
import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';
import AlertFormPartFour from '@components/AlertForm/AlertFormPartFour';
import {IAnimal} from '@app/interface/IAnimal';

export interface IAddMyAnimalForm extends IAnimal {
  files: []
}


const AlertFormBase: FC<IAlertFormBaseProps> = (props: IAlertFormBaseProps) => {
  const {control, handleSubmit, formState: {errors}} = useForm<IAlertFormData>({
    defaultValues: {
      name: null,
      category: props.view === ECreateAlertScreenView.LOST_PET ? EAlertCategory.LOST : EAlertCategory.FOUND,
      type: undefined,
      breed: null,
      hair: null,
      color: null,
      description: null,
      status: EAlertStatus.NEW,
      trackNumber: null,
      location: undefined,
      sex: null,
      sterilized: false,
      files: [],
    },
  });

  const [step, setStep] = React.useState(0);

  const nextStep = () => {
    if (step === formStep.length - 1) {
      return;
    }
    setStep(step + 1);
  };

  const previousStep = () => {
    if (step === 0) {
      return;
    }
    setStep(step - 1);
  };

  const formStep = [
    <AlertFormPartOne key={1} step={step} handleSubmit={handleSubmit} control={control} errors={errors} nextStep={nextStep} previousStep={previousStep} view={props.view}/>,
    <AlertFormPartTwo key={2} step={step} handleSubmit={handleSubmit} control={control} errors={errors} nextStep={nextStep} previousStep={previousStep} view={props.view}/>,
    <AlertFormPartThree key={3} step={step} handleSubmit={handleSubmit} control={control} errors={errors} nextStep={nextStep} previousStep={previousStep} view={props.view}/>,
    <AlertFormPartFour key={4} step={step} handleSubmit={handleSubmit} control={control} errors={errors} nextStep={nextStep} previousStep={previousStep} view={props.view}/>,
  ];

  const progress = (step + 1) * 100 / formStep.length;
  return <div className={'d-flex flex-column min-vh-100 lost-pet-screen-container'}>
    <Navbar/>
    <div className="d-flex flex-column h-100 flex-grow-1">
      <div className={'container'}>
        <div className="row">
          <div className="progress-container">
            <div className="progress mx-auto mt-4 mx-5" role="progressbar" aria-label="Warning example"
              aria-valuenow={progress} aria-valuemin={0} aria-valuemax={100}>
              <div className="progress-bar bg-dark" style={{width: progress + '%'}}>Etape {step + 1} sur {formStep.length}</div>
            </div>
          </div>
        </div>
      </div>

      <main className={'container my-auto'}>
        <div className="row">
          {
            formStep.filter((_, index) => index === step).map((form, index) => (
              <div key={index} className={'col-12 col-lg-8 mx-auto my-3'}>
                {form}
              </div>
            ))
          }
        </div>
      </main>
    </div>
  </div>;
};

export default AlertFormBase;
