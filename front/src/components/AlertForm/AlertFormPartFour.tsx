import React, {FC} from 'react';
import AlertFormDescription from '@components/AlertForm/AlertFormDescription';
import {IAlertMultipleFormData} from '@app/interface/form/alert/IAlertMultipleFormData';
import AlertFormStepFourForm from '@app/form/AlertForm/AlertFormStepFourForm';

const AlertFormPartFour: FC<IAlertMultipleFormData> = (props: IAlertMultipleFormData) => {
  const description = () => {
    switch (props.view) {
      case 'LOST_PET':
        return <AlertFormDescription title={'Ajouter des images de l\'animal animal'} description={'Merci de fournir des images de votre animal perdu. Cela permettra de mieux le reconnaître et d\'augmenter les chances de le retrouver.'} />;
      case 'FOUND_PET':
        return <AlertFormDescription title={'Ajouter des image de l\'animal trouvé'} description={'Merci de fournir des images de l\'animal trouvé. Cela permettra de mieux le reconnaître et d\'augmenter les chances que son propriétaire le retrouve.'} />;
    }
  };

  return (
    <>
      {description()}
      <AlertFormStepFourForm step={props.step} handleSubmit={props.handleSubmit} control={props.control} errors={props.errors} nextStep={props.nextStep} previousStep={props.previousStep} view={props.view}/>
    </>
  );
};

export default AlertFormPartFour;
