import React, {FC} from 'react';
import AlertFormDescription from '@components/AlertForm/AlertFormDescription';
import AlertFormStepOne from '@app/form/AlertForm/AlertFormStepOne';
import {IAlertMultipleFormData} from '@app/interface/form/alert/IAlertMultipleFormData';
import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';

const AlertFormPartOne: FC<IAlertMultipleFormData> = (props: IAlertMultipleFormData) => {
  const description = () => {
    switch (props.view) {
      case ECreateAlertScreenView.LOST_PET:
        return <AlertFormDescription title="Où votre animal s'est-il perdu ?" description="Vous pouvez indiquer une adresse précise. Nous ne communiquerons jamais votre emplacement exact." />;
      case ECreateAlertScreenView.FOUND_PET:
        return <AlertFormDescription title="Où avez-vous trouvé l'animal ?" description="Vous pouvez indiquer une adresse précise. Nous ne communiquerons jamais votre emplacement exact." />;
      default:
        return '';
    }
  };

  return (
    <>
      {description()}
      <AlertFormStepOne step={props.step} handleSubmit={props.handleSubmit} control={props.control} errors={props.errors} nextStep={props.nextStep} previousStep={props.previousStep} view={props.view}/>
    </>
  );
};

export default AlertFormPartOne;
