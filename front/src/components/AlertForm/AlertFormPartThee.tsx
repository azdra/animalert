import React, {FC} from 'react';
import AlertFormDescription from '@components/AlertForm/AlertFormDescription';
import AlertFormStepThreeForm from '@app/form/AlertForm/AlertFormStepThreeForm';
import {IAlertMultipleFormData} from '@app/interface/form/alert/IAlertMultipleFormData';

const AlertFormPartThree: FC<IAlertMultipleFormData> = (props: IAlertMultipleFormData) => {
  const description = () => {
    return <AlertFormDescription title={'Description de l\'apparence de l\'animal'} description={'Veuillez préciser la couleur et le type de robe de votre animal perdu. Ces informations aideront à identifier votre animal plus facilement.'} />;
  };

  return (
    <>
      {description()}
      <AlertFormStepThreeForm step={props.step} handleSubmit={props.handleSubmit} control={props.control} errors={props.errors} nextStep={props.nextStep} previousStep={props.previousStep} view={props.view}/>
    </>
  );
};

export default AlertFormPartThree;
