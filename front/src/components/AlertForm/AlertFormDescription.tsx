import React, {FC} from 'react';
import {IAlertFormDescriptionProps} from '@app/interface/form/alert/IAlertFormDescriptionProps';

const AlertFormDescription: FC<IAlertFormDescriptionProps> = (props: IAlertFormDescriptionProps) => {
  return <div className="text-center">
    <p className="text-overline font-petco break-words"></p>
    <h1 className="fs-1 mb-3 fw-bold">{props.title}</h1>
    {props.description && <p>{props.description}</p>}
    {props.children}
  </div>;
};

export default AlertFormDescription;
