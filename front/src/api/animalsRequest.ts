import {makeRequest} from '@app/api/makeRequest';
import {generatePath} from 'react-router-dom';
import {IAnimal} from '@app/interface/IAnimal';
import {IAlert} from '@app/interface/IAlert';

export const addMyAnimalRequest = async (data: FormData) => {
  return makeRequest('/animal/my-animal', 'POST', data, true, true);
};

export const deleteMyAnimal = async (id: string): Promise<void> => {
  return makeRequest(generatePath('/animal/my-animal/:id', {id}), 'DELETE');
};

type test = {animals: IAnimal[];
  alerts: { active: IAlert[]; resolved: IAlert[] };}

export const createAlertFromAnimal = async (id: string): Promise<test> => {
  return makeRequest(generatePath('/animal/my-animal/:id/alert', {id}), 'POST');
};

export const updateMyAnimalRequest = async (id: string, data: FormData): Promise<IAnimal> => {
  return makeRequest(generatePath('/animal/my-animal/:id', {id}), 'PATCH', data, true, true);
};

export const getMyAnimal = async (id: string): Promise<IAnimal> => {
  return makeRequest(generatePath('/animal/my-animal/:id', {id}));
};

export const removeImageFromAnimalRequest = async (id: string, imageId: string): Promise<void> => {
  return makeRequest(generatePath('/animal/my-animal/:id/image/:imageId', {id, imageId}), 'DELETE');
};
