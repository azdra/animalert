import {makeRequest} from '@app/api/makeRequest';
import {IRegisterFormData} from '@app/interface/form/IRegisterFormData';
import {UserState} from '@app/store/userStore';

export const loginRequest = async (username: string, password: string): Promise<UserState> => {
  return makeRequest('/auth/login', 'POST', {username, password}, false);
};

export const registerRequest = async (data: IRegisterFormData) => {
  return makeRequest('/auth/register', 'POST', data, false);
};
