import {makeRequest} from '@app/api/makeRequest';
import {IMyAccount} from '@app/interface/IMyAccount';
import {IUser} from '@app/interface/IUser';
import {IUpdateBaseUser} from '@app/interface/IUpdateBaseUser';
import {IUpdatePassword} from '@app/interface/form/IUpdatePassword';

export const myAccountRequest = async (): Promise<IMyAccount> => {
  return makeRequest('/user/my-account', 'GET', null);
};

export const updateMyAccountRequest = async (data: IUpdateBaseUser): Promise<IUser> => {
  return makeRequest('/user/my-account', 'PATCH', data);
};

export const updateAccountPasswordRequest = async (data: IUpdatePassword) => {
  return makeRequest('/user/my-account/my-password', 'PATCH', data);
};

export const updateAccountEmailRequest = async (email: string) => {
  return makeRequest('/user/my-account/my-email', 'PATCH', {email});
};

export const updateAccountPhoneRequest = async (phone: string) => {
  return makeRequest('/user/my-account/my-phone-number', 'PATCH', {phone});
};

export const updateAccountAvatarRequest = async (data: FormData) => {
  return makeRequest('/user/my-account/my-avatar', 'PATCH', data, true, true);
};
