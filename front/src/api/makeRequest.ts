import axios, {Method} from 'axios';

const getHeaders = (auth: boolean = true, formData: boolean = false) => {
  return auth && localStorage.getItem('token') ? {
    'Content-Type': formData ? 'multipart/form-data' : 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
  } : {};
};

const baseURL = 'http://localhost:3000/api/';

export const makeRequest = async (url: string, method: Method, data?: any, auth = true, formData: boolean = false): Promise<any> => {
  return new Promise((resolve, reject) => {
    return axios({
      url,
      method,
      data,
      baseURL,
      headers: getHeaders(auth, formData),
    }).then((res) => {
      resolve(res.data);
    }).catch((err) => {
      reject(err);
    });
  });
};

