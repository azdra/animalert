import {makeRequest} from '@app/api/makeRequest';
import {IApiAddress} from '@app/interface/IApiAddress';

export const getAddress = async (address: string): Promise<IApiAddress[]> => {
  return makeRequest(`/address`, 'POST', {address});
};

export const getAddressByCoordinates = async (lat: number, lng: number): Promise<IApiAddress[]> => {
  return makeRequest(`/address/${lat}/${lng}`, 'POST', {lat, lng});
};
