import {makeRequest} from '@app/api/makeRequest';
import {generatePath} from 'react-router-dom';
import {IAlert} from '@app/interface/IAlert';
import {IIndexScreenProps} from '@app/interface/form/search/IIndexScreenProps';

export const createAlert = async (alert: FormData) => {
  return makeRequest('/alert', 'POST', alert, true, true);
};

export const markAlertResolved = async (id: string) => {
  return makeRequest(generatePath('/alert/:id/resolve', {id}), 'POST');
};

export const searchAlertRequest = async (data: IIndexScreenProps): Promise<IAlert[]> => {
  return makeRequest('/alert/search', 'POST', data);
};

export const responseToAnAlert = async (alert: IAlert) => {
  return makeRequest(generatePath('/alert/:id/response', {id: alert.id}), 'POST');
};

