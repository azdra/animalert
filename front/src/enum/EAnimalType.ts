export enum EAnimalType {
  CAT = 'cat',
  DOG = 'dog',
  OTHER = 'other',
}
