export enum ECreateAlertScreenView {
  FOUND_PET = 'FOUND_PET',
  LOST_PET = 'LOST_PET',
}
