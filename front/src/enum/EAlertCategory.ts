export enum EAlertCategory {
  LOST = 'lost',
  SEEN = 'seen',
  FOUND = 'found',
  STOLEN = 'stolen',
}
