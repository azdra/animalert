import React, {FC} from 'react';
import MyAccountParameterBreadcrumb from '@screens/MyProfile/MyAccountParameter/MyAccountParameterBreadcrumb';
import {RouteConfig} from '@app/config/route.config';
import ItemMyProfile from '@components/InfoProfile/ItemMyProfile/ItemMyProfile';
import {useAppSelector} from '@app/hooks';
import MyProfileBaseScreen from '@components/MyProfile/MyProfileBaseScreen';

const MyAccountParameterScreen: FC = () => {
  const {user} = useAppSelector((state) => state.user);

  return <MyProfileBaseScreen>
    <div className="mt-4">
      <MyAccountParameterBreadcrumb />
      <h2>Mes paramètres de compte</h2>
    </div>

    <div className="my-4">

      <ItemMyProfile title={user.phone ?? 'Non renseigné'}
        button={'Modifier mon numéro'} url={RouteConfig.myProfileAccountParametersPhone.path}
        icon={'bi bi-telephone'} />

      <ItemMyProfile title={user.email ?? 'Non renseigné'}
        button={'Modifier mon adresse mail'} url={RouteConfig.myProfileAccountParametersEmail.path}
        icon={'bi bi-envelope'} />


      <ItemMyProfile title={'Modifier mon mot de passe'}
        button={'Modifier mon mot de passe'}
        url={RouteConfig.myProfileAccountParametersPassword.path}
        icon={'bi bi-unlock'} />
    </div>
  </MyProfileBaseScreen>;
};

export default MyAccountParameterScreen;
