import React, {FC} from 'react';
import {RouteConfig} from '@app/config/route.config';
import Breadcrumb from '@components/ui/Breadcrumb';

const MyAccountParameterPhoneNumberBreadcrumb: FC = () => {
  return <Breadcrumb elements={[
    {name: 'Mon profil', link: RouteConfig.myProfile.path},
    {name: 'Mes paramètres de compte', link: RouteConfig.myProfileAccountParameters.path},
    {name: 'Mon numéro de téléphone'},
  ]}/>;
};

export default MyAccountParameterPhoneNumberBreadcrumb;

