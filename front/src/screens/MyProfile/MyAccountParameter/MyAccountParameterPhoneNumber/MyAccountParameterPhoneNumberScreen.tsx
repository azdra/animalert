import React, {FC} from 'react';
import MyAccountParameterPhoneNumberBreadcrumb
  from '@screens/MyProfile/MyAccountParameter/MyAccountParameterPhoneNumber/MyAccountParameterPhoneNumberBreadcrumb';
import MyAccountParameterPhoneNumberForm from '@app/form/MyAccountParameter/MyAccountParameterPhoneNumberForm';
import MyProfileBaseScreen from '@components/MyProfile/MyProfileBaseScreen';

const MyAccountParameterPhoneNumberScreen: FC = () => {
  return <MyProfileBaseScreen>
    <div className="mt-4">
      <MyAccountParameterPhoneNumberBreadcrumb />
      <h2>Mon numéro de téléphone</h2>
    </div>

    <div className="my-5">
      <MyAccountParameterPhoneNumberForm />
    </div>
  </MyProfileBaseScreen>;
};

export default MyAccountParameterPhoneNumberScreen;
