import React, {FC} from 'react';
import MyAccountParameterEmailBreadcrumb
  from '@screens/MyProfile/MyAccountParameter/MyAccountParameterEmail/MyAccountParameterEmailBreadcrumb';
import MyAccountParameterEmailForm from '@app/form/MyAccountParameter/MyAccountParameterEmailForm';
import MyProfileBaseScreen from '@components/MyProfile/MyProfileBaseScreen';

const MyAccountParameterEmailScreen: FC = () => {
  return <MyProfileBaseScreen>
    <div className="mt-4">
      <MyAccountParameterEmailBreadcrumb />
      <h2>Mon adresse mail</h2>
    </div>

    <div className="my-5">
      <MyAccountParameterEmailForm />
    </div>
  </MyProfileBaseScreen>;
};

export default MyAccountParameterEmailScreen;
