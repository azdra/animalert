import React, {FC} from 'react';
import MyAccountParameterPasswordBreadcrumb
  from '@screens/MyProfile/MyAccountParameter/MyAccountParameterPassword/MyAccountParameterPasswordBreadcrumb';
import MyAccountParameterPasswordForm from '@app/form/MyAccountParameter/MyAccountParameterPasswordForm';
import MyProfileBaseScreen from '@components/MyProfile/MyProfileBaseScreen';

const MyAccountParameterPasswordScreen: FC = () => {
  return <MyProfileBaseScreen>
    <div className="mt-4">
      <MyAccountParameterPasswordBreadcrumb />
      <h2>Mon mot de passe</h2>
    </div>

    <div className="my-5">
      <MyAccountParameterPasswordForm />
    </div>
  </MyProfileBaseScreen>;
};

export default MyAccountParameterPasswordScreen;
