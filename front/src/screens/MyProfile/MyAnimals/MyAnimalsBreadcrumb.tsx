import React, {FC} from 'react';
import {RouteConfig} from '@app/config/route.config';
import Breadcrumb from '@components/ui/Breadcrumb';

const MyAnimalsBreadcrumb: FC = () => {
  return <Breadcrumb elements={[
    {name: 'Mon profil', link: RouteConfig.myProfile.path},
    {name: 'Mes animaux'},
  ]}/>;
};

export default MyAnimalsBreadcrumb;

