import React, {FC, useState} from 'react';
import MyAnimalsBreadcrumb from '@screens/MyProfile/MyAnimals/MyAnimalsBreadcrumb';
import AddMyAnimalForm from '@app/form/AddMyAnimalForm';
import MyProfileBaseScreen from '@components/MyProfile/MyProfileBaseScreen';
import MyAnimalsList from '@components/MyAnimals/MyAnimalsList';

const MyAnimalsScreen: FC = () => {
  const [show, setShow] = useState(false);

  const cancel = () => setShow(false);
  const showAddForm = () => setShow(true);

  return <MyProfileBaseScreen>
    <div className="mt-4">
      <MyAnimalsBreadcrumb/>
      <h2>Mes animaux</h2>
    </div>

    <div className={`mx-auto my-4`}>
      {show ? <AddMyAnimalForm cancel={cancel}/> : <MyAnimalsList showAddForm={showAddForm}/>}
    </div>
  </MyProfileBaseScreen>;
};

export default MyAnimalsScreen;

