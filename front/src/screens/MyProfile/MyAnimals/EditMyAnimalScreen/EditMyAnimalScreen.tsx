import MyProfileBaseScreen from '@components/MyProfile/MyProfileBaseScreen';
import React, {useEffect, useState} from 'react';
import EditMyAnimalScreenBreadcrumb from '@screens/MyProfile/MyAnimals/EditMyAnimalScreen/EditMyAnimalScreenBreadcrumb';
import {useNavigate, useParams} from 'react-router';
import {getMyAnimal} from '@app/api/animalsRequest';
import {IAnimal} from '@app/interface/IAnimal';
import AddMyAnimalForm from '@app/form/AddMyAnimalForm';
import {RouteConfig} from '@app/config/route.config';
import Loader from '@components/Loader';

const EditMyAnimalScreen = () => {
  const navigate = useNavigate();
  const [animal, setAnimal] = useState<IAnimal>();
  const params = useParams();

  useEffect(() => {
    if (!params.id) {
      navigate('*');
      return;
    }
    getMyAnimal(params.id).then((res) => {
      setAnimal(res);
    }).catch(() => {
      navigate('*');
    });
  }, []);

  const cancel = () => {
    navigate(RouteConfig.myProfileAnimals.path);
  };

  return !animal?.id ? <Loader/> : <MyProfileBaseScreen>
    <div className="mt-4">
      <EditMyAnimalScreenBreadcrumb name={animal?.name} />
      <h2>Edition de {animal?.name}</h2>

      <div className={`mx-auto my-4`}>
        {animal && <AddMyAnimalForm cancel={cancel} defaultValues={animal} />}
      </div>
    </div>
  </MyProfileBaseScreen>;
};

export default EditMyAnimalScreen;
