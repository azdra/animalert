import React, {FC} from 'react';
import {RouteConfig} from '@app/config/route.config';
import Breadcrumb from '@components/ui/Breadcrumb';

interface IEditMyAnimalScreenBreadcrumbProps {
  name: string
}

const EditMyAnimalScreenBreadcrumb: FC<IEditMyAnimalScreenBreadcrumbProps> = (props: IEditMyAnimalScreenBreadcrumbProps) => {
  return <Breadcrumb elements={[
    {name: 'Mon profil', link: RouteConfig.myProfile.path},
    {name: 'Mes animaux', link: RouteConfig.myProfileAnimals.path},
    {name: `Edition de ${props.name}`},
  ]}/>;
};

export default EditMyAnimalScreenBreadcrumb;

