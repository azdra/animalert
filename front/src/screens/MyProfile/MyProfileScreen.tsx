import React, {FC} from 'react';
import Navbar from '@components/Navbar/Navbar';
import MyProfile from '@components/MyProfile';
import InfoProfile from '@components/InfoProfile/InfoProfile';

const MyProfileScreen: FC = () => {
  return (
    <>
      <Navbar/>
      <main>
        <InfoProfile/>
        <MyProfile/>
      </main>
    </>
  );
};

export default MyProfileScreen;
