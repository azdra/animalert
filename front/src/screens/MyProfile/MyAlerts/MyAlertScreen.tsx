import React, {FC} from 'react';
import MyAlertsBreadcrumb from '@screens/MyProfile/MyAlerts/MyAlertsBreadcrumb';
import {getActiveAlert, getAlertName, getResolvedAlert, initAlerts} from '@app/store/alertStore';
import {useAppDispatch, useAppSelector} from '@app/hooks';
import ActionItemMyProfile from '@components/InfoProfile/ItemMyProfile/ActionItemMyProfile';
import {IAlert} from '@app/interface/IAlert';
import {markAlertResolved} from '@app/api/alertRequest';
import {aToast} from '@app/assets/js/toast';
import InfoProfileNoData from '@components/InfoProfile/InfoProfileNoData';
import MarkAlertResolvedConfirmModal from '@components/modal/confirm/MarkAlertResolvedConfirmModal';
import {Modal} from 'bootstrap';
import MyProfileBaseScreen from '@components/MyProfile/MyProfileBaseScreen';
import {animalSlicer} from '@app/store/animalStore';

const MyAlertScreen: FC = () => {
  const activeAlert = useAppSelector(getActiveAlert);
  const resolvedAlert = useAppSelector(getResolvedAlert);
  const dispatch = useAppDispatch();

  const markResolve = (alert: IAlert, modalId: string) => {
    markAlertResolved(alert.id.toString()).then((res) => {
      aToast.success('Alerte marquée comme resolue');
      dispatch(initAlerts(res));
      if (alert.animal && alert.animal.id) {
        dispatch(animalSlicer.actions.markAlertResolved(alert.animal.id));
      }
      const modal = Modal.getOrCreateInstance(document.getElementById(modalId) as HTMLElement);
      modal.hide();
    });
  };

  const openModal = (id: string) => {
    const modal = Modal.getOrCreateInstance(document.getElementById(id) as HTMLElement);
    modal.show();
  };

  return <MyProfileBaseScreen>
    <div className="mt-4">
      <MyAlertsBreadcrumb />
      <h2>Mes alertes</h2>
    </div>

    <div className={`mx-auto my-4`}>
      <div className="border py-3 px-4 rounded">
        <div className="d-flex flex-row justify-content-between align-items-center">
          <span className="fw-bold text-body-secondary">Mes alertes actives</span>
        </div>
        <div className="mt-2">
          {
            activeAlert.length > 0 ? activeAlert.map((alert, index) => (
              <div className={'my-2'} key={index}>
                <MarkAlertResolvedConfirmModal id={`mark_alert_resolved_${index}`}
                  accept={() => markResolve(alert, `mark_alert_resolved_${index}`)} />
                <ActionItemMyProfile title={getAlertName(alert)} button={'Marquer comme resolue'}
                  action={() => openModal(`mark_alert_resolved_${index}`)} />
              </div>
            )) : <InfoProfileNoData title={'Vous n’avez pas encore d\'alerte active'}
              icon={'bi bi-exclamation-triangle'} />
          }

        </div>
      </div>
    </div>

    <div className={`mx-auto my-4`}>
      <div className="border py-3 px-4 rounded">
        <div className="d-flex flex-row justify-content-between align-items-center">
          <span className="fw-bold text-body-secondary">Mes alertes passées</span>
        </div>
        <div className="mt-2">
          {
            resolvedAlert.length > 0 ? resolvedAlert.map((alert, index) => <ActionItemMyProfile key={index}
              title={getAlertName(alert)} />) :
              <InfoProfileNoData title={'Vous n’avez pas encore d\'alerte passée'}
                icon={'bi bi-exclamation-triangle'} />
          }
        </div>
      </div>
    </div>
  </MyProfileBaseScreen>;
};

export default MyAlertScreen;

