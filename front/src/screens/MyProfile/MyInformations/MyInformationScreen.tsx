import React, {FC} from 'react';
import MyInformationBreadcrumb from '@screens/MyProfile/MyInformations/MyInformationBreadcrumb';
import MyInformationForm from '@app/form/MyInformationForm';
import MyProfileBaseScreen from '@components/MyProfile/MyProfileBaseScreen';

const MyInformationScreen: FC = () => {
  return <MyProfileBaseScreen>
    <div className="mt-4">
      <MyInformationBreadcrumb />
      <h2>Mes informations personnelles</h2>
    </div>

    <div className="my-4">
      <MyInformationForm />
    </div>
  </MyProfileBaseScreen>;
};

export default MyInformationScreen;
