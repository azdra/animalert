import React, {FC} from 'react';
import {RouteConfig} from '@app/config/route.config';
import Breadcrumb from '@components/ui/Breadcrumb';

const MyInformationBreadcrumb: FC = () => {
  return <Breadcrumb elements={[
    {name: 'Mon profil', link: RouteConfig.myProfile.path},
    {name: 'Mes informations personnelles'},
  ]}/>;
};

export default MyInformationBreadcrumb;

