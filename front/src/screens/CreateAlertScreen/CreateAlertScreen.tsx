import React, {FC} from 'react';
import './CreateAlertScreen.scss';
import {useAppSelector} from '@app/hooks';
import {isUserLogged} from '@app/store/userStore';
import AlertFormBase from '@components/AlertForm/AlertFormBase';
import AlertFormNeedToBeLogin from '@components/AlertForm/AlertFormNeedToBeLogin';
import {ICreateAlertScreenProps} from '@app/interface/form/alert/ICreateAlertScreenProps';

const CreateAlertScreen: FC<ICreateAlertScreenProps> = (props: ICreateAlertScreenProps) => {
  const isLogin = useAppSelector(isUserLogged);

  return !isLogin ? (
    <AlertFormNeedToBeLogin/>
  ) : <AlertFormBase view={props.view} />;
};

export default CreateAlertScreen;
