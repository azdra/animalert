import React, {FC} from 'react';
import Navbar from '@components/Navbar/Navbar';
import SearchAlertForm from '@app/form/SearchAlertForm';
import {useAppDispatch, useAppSelector} from '@app/hooks';
import AlertCard from '@components/AnimalCard/AlertCard';
import FilterSearch from '@components/FilterSearchOffcanvas/FilterSearch';
import {IIndexScreenProps} from '@app/interface/form/search/IIndexScreenProps';
import {searchAlertRequest} from '@app/api/alertRequest';
import {searchSlicer} from '@app/store/searchStore';

const SearchScreen: FC = () => {
  const dispatch = useAppDispatch();
  const {alerts, filter, maxResult} = useAppSelector((state) => state.search);

  const searchAlert = (data: IIndexScreenProps) => {
    searchAlertRequest(data).then((res) => {
      dispatch(searchSlicer.actions.setAlerts(res[0]));
      dispatch(searchSlicer.actions.setMaxResult(res[1]));
      // const el = document.getElementById('offcanvasExample')
      // const modalInstance = bootstrap.Offcanvas.getOrCreateInstance(el as HTMLElement);
      // if (modalInstance && modalInstance._backdrop) {
      //   modalInstance.hide();
      // }
      // const modalInstance = bootstrap.Offcanvas.getOrCreateInstance(document.getElementById('offcanvasExample') as HTMLElement);
      // console.log(modalInstance);
      // if (modalInstance) {
      //   modalInstance.hide();
      // }
    });
  };

  const goToPage = (page: number) => {
    const newFilter = {...filter, page};
    dispatch(searchSlicer.actions.setFilter(newFilter));
    searchAlert(newFilter);
  };

  return (
    <>
      <Navbar />
      <div className="container my-4">
        <div className="row">
          <div className="col-3 d-none d-lg-block border-end">
            <SearchAlertForm searchAlert={searchAlert} />
          </div>
          <div className={'d-block d-lg-none'}>
            <FilterSearch/>
          </div>
          <div className="col-12 col-lg-9">
            {
              alerts.length === 0 && <p>Aucun résultat trouvé</p>
            }
            {
              alerts.length > 0 && (
                <>
                  <p>{maxResult} résultat(s) trouvé(s)</p>
                  <div className="row row-cols-1 row-cols-sm-2 row-cols-lg-3 row-cols-xl-4 row-cols-xxl-4 g-4">
                    {
                      alerts.map((alert, index) => {
                        return <AlertCard key={index} {...alert} />;
                      })
                    }
                  </div>
                </>
              )
            }
            <nav aria-label="Page navigation example">
              <ul className="pagination justify-content-center mt-5">
                <li className="page-item">
                  <button className={`page-link ${
                    filter.page === 1 ? 'disabled' : ''
                  }`} disabled={filter.page === 1} onClick={() => goToPage(1)} aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </button>
                </li>

                {/* Calcul des pages à afficher */}
                {(() => {
                  const lastPage = Math.ceil(maxResult / filter.take);
                  let pagesToShow = [];
                  if (lastPage <= 5) {
                    pagesToShow = [...Array(lastPage).keys()].map((i) => i + 1);
                  } else if (filter.page <= 2) {
                    pagesToShow = [1, 2, 3, 4, 5];
                  } else if (filter.page >= lastPage - 3) {
                    pagesToShow = [lastPage - 4, lastPage - 3, lastPage - 2, lastPage - 1, lastPage];
                  } else {
                    pagesToShow = [filter.page - 2, filter.page - 1, filter.page, filter.page + 1, filter.page + 2];
                  }
                  // Affichage des pages
                  return pagesToShow.map((pageNum, idx) => (
                    <li className={`page-item ${filter.page === pageNum ? 'active' : ''}`} key={idx}>
                      <button
                        className="page-link"
                        onClick={() => goToPage(pageNum)}
                      >
                        {pageNum}
                      </button>
                    </li>
                  ));
                })()}

                <li className="page-item">
                  <button className={`page-link ${
                    filter.page === Math.ceil(maxResult/filter.take) ? 'disabled' : ''
                  }`} disabled={filter.page === Math.ceil(maxResult/filter.take)} onClick={() => goToPage(Math.ceil(maxResult/filter.take))} aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </button>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </>
  );
};

export default SearchScreen;
