import React, {FC} from 'react';
import CreateAlertScreen from '@screens/CreateAlertScreen/CreateAlertScreen';
import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';

const FoundPetScreen: FC = () => {
  return <CreateAlertScreen view={ECreateAlertScreenView.FOUND_PET}/>;
};

export default FoundPetScreen;
