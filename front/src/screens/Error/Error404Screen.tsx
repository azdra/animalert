import React, {FC} from 'react';
import Navbar from '@components/Navbar/Navbar';
import {NavLink} from 'react-router-dom';
import {RouteConfig} from '@app/config/route.config';

const Error404Screen: FC = () => {
  return (
    <>
      <div className={'d-flex flex-column vh-100 '}>
        <Navbar/>
        <main className={'d-flex align-items-center justify-content-start position-relative mx-auto flex-grow-1'}>
          <div className={'text-center'}>
            <div>
              <img src="/static/404.webp" alt="" width={'512'}/>
              <p className={'fw-bold mb-1'}>Oups votre page ne charge pas</p>
              <small className={'text-body-secondary'}>Pas d’inquiétude, nous allons vous indiquez un autre chemin</small>
            </div>
            <NavLink className={'btn btn-primary text-white mt-5 fw-bold'} to={RouteConfig.home.path}>Retourner vers la page d'accueil</NavLink>
          </div>
        </main>
      </div>
    </>
  );
};

export default Error404Screen;
