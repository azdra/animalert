import React, {FC} from 'react';
import CreateAlertScreen from '@screens/CreateAlertScreen/CreateAlertScreen';
import {ECreateAlertScreenView} from '@app/enum/ECreateAlertScreenView';


const LostPetScreen: FC = () => {
  return <CreateAlertScreen view={ECreateAlertScreenView.LOST_PET}/>;
};

export default LostPetScreen;
