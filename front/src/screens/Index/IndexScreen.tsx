import {FC, useEffect} from 'react';
import {useNavigate} from 'react-router';
import {RouteConfig} from '@app/config/route.config';

const IndexScreen: FC = () => {
  const navigate = useNavigate();

  useEffect(() => {
    navigate(RouteConfig.search.path);
  }, []);

  return null;
};
export default IndexScreen;
