import React, {FC} from 'react';
import RegisterForm from '@app/form/auth/RegisterForm';
import Navbar from '@components/Navbar/Navbar';

const RegisterScreen: FC = () => {
  return (
    <div className={'register-screen-container'}>
      <Navbar />
      <main className="register-screen d-flex align-items-center justify-content-start">
        <div className="container">
          <div className="row">
            <RegisterForm />
          </div>
          <div className="register-screen-overlay" />
        </div>
      </main>
    </div>
  );
};

export default RegisterScreen;
