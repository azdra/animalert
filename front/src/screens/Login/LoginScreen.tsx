import React, {FC} from 'react';
import LoginForm from '@app/form/auth/LoginForm';
import Navbar from '@components/Navbar/Navbar';

const LoginScreen: FC = () => {
  return (
    <div className={'login-screen-container'}>
      <Navbar />
      <main className="login-screen">
        <div className="container">
          <div className="row">
            <LoginForm />
          </div>
          <div className="login-screen-overlay" />
        </div>
      </main>
    </div>
  );
};

export default LoginScreen;
