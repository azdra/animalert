import React, {FC, useEffect, useState} from 'react';
import {createBrowserRouter, createRoutesFromElements, Route} from 'react-router-dom';
import MyProfileScreen from '@screens/MyProfile/MyProfileScreen';
import Error404Screen from '@screens/Error/Error404Screen';
import LoginScreen from '@screens/Login/LoginScreen';
import RegisterScreen from '@screens/Register/RegisterScreen';
import {RouteConfig} from '@app/config/route.config';
import {myAccountRequest} from '@app/api/userRequest';
import {setUser} from '@app/store/userStore';
import {useAppDispatch} from '@app/hooks';
import Loader from '@components/Loader';
import MyAnimalsScreen from '@screens/MyProfile/MyAnimals/MyAnimalsScreen';
import {initAnimals} from '@app/store/animalStore';
import MyInformationScreen from '@screens/MyProfile/MyInformations/MyInformationScreen';
import MyAccountParameterScreen from '@screens/MyProfile/MyAccountParameter/MyAccountParameterScreen';
import MyAccountParameterPasswordScreen
  from '@screens/MyProfile/MyAccountParameter/MyAccountParameterPassword/MyAccountParameterPasswordScreen';
import MyAccountParameterEmailScreen
  from '@screens/MyProfile/MyAccountParameter/MyAccountParameterEmail/MyAccountParameterEmailScreen';
import MyAccountParameterPhoneNumberScreen
  from '@screens/MyProfile/MyAccountParameter/MyAccountParameterPhoneNumber/MyAccountParameterPhoneNumberScreen';
import LostPetScreen from '@screens/LostPet/LostPetScreen';
import FoundPetScreen from '@screens/FoundPet/FoundPetScreen';
import MyAlertScreen from '@screens/MyProfile/MyAlerts/MyAlertScreen';
import {initAlerts} from '@app/store/alertStore';
import {RouterProvider} from 'react-router';
import AuthenticateRouter from '@app/router/AuthenticateRouter';
import SearchScreen from '@screens/Search/SearchScreen';
import IndexScreen from '@screens/Index/IndexScreen';
import EditMyAnimalScreen from '@screens/MyProfile/MyAnimals/EditMyAnimalScreen/EditMyAnimalScreen';

const Router: FC = () => {
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);
  const [userLogged, setUserLogged] = useState(false);

  useEffect(() => {
    if (localStorage.getItem('token')) {
      setLoading(true);
      myAccountRequest().then((res) => {
        dispatch(setUser(res));
        dispatch(initAnimals(res.animals));
        dispatch(initAlerts(res.alerts));
        setTimeout(() => {
          setLoading(false);
          setUserLogged(true);
        }, 500);
      });
    }
  }, []);

  const router = createBrowserRouter(
      createRoutesFromElements(
          <>
            <Route element={<AuthenticateRouter loading={loading} userLogged={userLogged}/>}>
              <Route path={RouteConfig.myProfile.path} element={<MyProfileScreen />} />
              <Route path={RouteConfig.myProfileAlerts.path} element={<MyAlertScreen />} />
              <Route path={RouteConfig.myProfileAnimals.path} element={<MyAnimalsScreen />} />
              <Route path={RouteConfig.editMyAnimal.path} element={<EditMyAnimalScreen />} />
              <Route path={RouteConfig.myProfileEdit.path} element={<MyInformationScreen />} />
              <Route path={RouteConfig.myProfileAccountParameters.path} element={<MyAccountParameterScreen />} />
              <Route path={RouteConfig.myProfileAccountParametersPassword.path} element={<MyAccountParameterPasswordScreen />} />
              <Route path={RouteConfig.myProfileAccountParametersEmail.path} element={<MyAccountParameterEmailScreen />} />
              <Route path={RouteConfig.myProfileAccountParametersPhone.path} element={<MyAccountParameterPhoneNumberScreen />} />
            </Route>

            <Route path={RouteConfig.home.path} element={<IndexScreen />}/>
            <Route path={RouteConfig.login.path} element={<LoginScreen />} />
            <Route path={RouteConfig.register.path} element={<RegisterScreen />} />
            <Route path={RouteConfig.search.path} element={<SearchScreen />} />

            <Route path={RouteConfig.lostPet.path} element={<LostPetScreen />} />
            <Route path={RouteConfig.foundPet.path} element={<FoundPetScreen />} />
            <Route path="*" element={<Error404Screen/>} />
          </>,
      ),
  );

  return (
    <>
      {
        loading ? <Loader/> : <RouterProvider router={router} />
      }
    </>
  );
};

export default Router;
