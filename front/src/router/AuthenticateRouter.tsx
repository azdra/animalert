import React, {FC, useEffect} from 'react';
import {Outlet, useNavigate} from 'react-router';
import {RouteConfig} from '@app/config/route.config';
import {useAppSelector} from '@app/hooks';
import {isUserLogged} from '@app/store/userStore';

const AuthenticateRouter: FC<{loading: boolean, userLogged: boolean}> = (props) => {
  const navigate = useNavigate();
  const user = useAppSelector(isUserLogged);

  useEffect(() => {
    if (props.userLogged && !user) {
      return navigate(RouteConfig.login.path);
    }
  }, [props.userLogged]);

  return <Outlet />;
};

export default AuthenticateRouter;
