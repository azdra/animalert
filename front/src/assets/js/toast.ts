import {Bounce, toast} from 'react-toastify';
import {ToastOptions} from 'react-toastify/dist/types';

const toastConfig: ToastOptions = {
  position: 'top-center',
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
  theme: 'colored',
  transition: Bounce,
};

export const aToast = {
  success(content: string) {
    return toast.success(content, toastConfig);
  },
  error(content: string) {
    return toast.error(content, toastConfig);
  },
};
