import {createSlice} from '@reduxjs/toolkit';
import {RootState} from '@app/store/store';
import {IAlert} from '@app/interface/IAlert';
import {EAlertCategory} from '@app/enum/EAlertCategory';
import {EAnimalType} from '@app/enum/EAnimalType';


const initialState: {active: IAlert[], resolved: IAlert[]} = {active: [], resolved: []};

export const alertSlicer = createSlice({
  name: 'alerts',
  initialState: {
    alerts: initialState,
  },
  reducers: {
    initAlerts: (state, action) => {
      state.alerts = action.payload;
    },
  },
});

export const {initAlerts} = alertSlicer.actions;

export const getActiveAlert = (state: RootState) => state.alerts.alerts.active;
export const getResolvedAlert = (state: RootState) => state.alerts.alerts.resolved;

export const getAlertName = (alert: IAlert) => {
  return alert.name ?? `${getAlertType(alert)} ${getAlertCategory(alert)} à ${alert.location.city}`;
};

export const getAlertCategory = (alert: IAlert) => {
  switch (alert.category) {
    case EAlertCategory.LOST:
      return 'Perdu';
    case EAlertCategory.FOUND:
      return 'Trouvé';
    case EAlertCategory.SEEN:
      return 'Aperçu';
    case EAlertCategory.STOLEN:
      return 'Volé';
    default:
      return alert.category;
  }
};

export const getAlertType = (alert: IAlert) => {
  switch (alert.type) {
    case EAnimalType.DOG:
      return 'Chien';
    case EAnimalType.CAT:
      return 'Chat';
    case EAnimalType.OTHER:
      return 'Autre';
    default:
      return alert.type;
  }
};

export default alertSlicer.reducer;
