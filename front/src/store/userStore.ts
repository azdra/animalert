import {createSlice} from '@reduxjs/toolkit';
import {IUser} from '@app/interface/IUser';
import {RootState} from '@app/store/store';

export interface UserState extends IUser {
  loading: boolean
}

const initialState: UserState = {
  id: null,
  firstname: null,
  lastname: null,
  email: null,
  password: null,
  phone: null,
  address: null,
  access_token: '',
  loading: false,
  avatar: null,
};

export const userSlicer = createSlice({
  name: 'user',
  initialState: {
    user: initialState,
  },
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload;
      state.user.loading = false;
    },
    clearUser: (state) => {
      state.user = initialState;
    },
    updatePhoneNumber: (state, action) => {
      state.user.phone = action.payload;
    },
    updateAvatar: (state, action) => {
      state.user.avatar = action.payload;
    },
  },
});

export const isUserLogged = (state: RootState) => state.user.user.id !== null;

export const {setUser, clearUser} = userSlicer.actions;

export default userSlicer.reducer;
