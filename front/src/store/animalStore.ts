import {createSlice} from '@reduxjs/toolkit';
import {IAnimal} from '@app/interface/IAnimal';

const initialState: IAnimal[] = [];

export const animalSlicer = createSlice({
  name: 'animal',
  initialState: {
    animals: initialState,
  },
  reducers: {
    initAnimals: (state, action) => {
      state.animals = action.payload;
    },
    addAnimal: (state, action) => {
      state.animals.push(action.payload);
    },
    updateOneAnimal: (state, action) => {
      const index = state.animals.findIndex((animal) => animal.id === action.payload.id);
      if (index !== -1) {
        state.animals.splice(index, 1, action.payload);
      }
    },
    deleteAnimal: (state, action) => {
      state.animals = state.animals.filter((animal) => animal.id !== action.payload);
    },
    removePhoto: (state, action) => {
      const animal = state.animals.find((animal) => animal.id === action.payload.animalId);
      if (animal) {
        animal.photos = animal.photos.filter((photo) => photo.id !== action.payload.id);
      }
    },
    markAlertResolved: (state, action) => {
      const animal = state.animals.find((animal) => animal.id === action.payload);
      if (animal) {
        const alerts = animal.alerts.filter((alert) => alert.status === 'new');
        if (alerts.length) {
          alerts.map((alert) => (alert.status = 'resolved'));
        }
      }
    },
  },
});

export const {initAnimals, addAnimal, deleteAnimal, updateOneAnimal, removePhoto} = animalSlicer.actions;
export default animalSlicer.reducer;

export const hasActiveAlert = (animal: IAnimal) => {
  return animal.alerts.some((alert) => alert.status === 'new');
};
