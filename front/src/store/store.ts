import {configureStore} from '@reduxjs/toolkit';
import userStore from '@app/store/userStore';
import animalStore from '@app/store/animalStore';
import alertStore from '@app/store/alertStore';
import searchStore from '@app/store/searchStore';

export const store = configureStore({
  reducer: {
    user: userStore,
    animals: animalStore,
    alerts: alertStore,
    search: searchStore,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
