import {createSlice} from '@reduxjs/toolkit';
import {EAlertStatus} from '@app/enum/EAlertStatus';
import {IAlert} from '@app/interface/IAlert';
import {IIndexScreenProps} from '@app/interface/form/search/IIndexScreenProps';

interface SearchState {
  filter : IIndexScreenProps
  alerts: IAlert[]
}

export const searchInitialState: IIndexScreenProps = {
  category: null,
  type: null,
  status: EAlertStatus.NEW,
  trackNumber: null,
  take: 30,
  page: 1,
};

export const initialState: SearchState = {
  filter: searchInitialState,
  alerts: [],
  maxResult: 0,
};

export const searchSlicer = createSlice({
  name: 'search',
  initialState: initialState,
  reducers: {
    resetSearch: (state) => {
      state.filter = searchInitialState;
    },
    setFilter: (state, action) => {
      state.filter = action.payload;
    },
    setAlerts: (state, action) => {
      state.alerts = action.payload;
    },
    setMaxResult: (state, action) => {
      state.maxResult = action.payload;
    },
  },
});

export const {resetSearch, setFilter, setAlerts} = searchSlicer.actions;

export default searchSlicer.reducer;
