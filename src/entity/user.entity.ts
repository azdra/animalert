import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import AnimalEntity from './animal.entity';
import AlertEntity from './alert.entity';
import { ApiProperty } from '@nestjs/swagger';
import AddressEntity from './address.entity';

@Entity({ name: 'user' })
@Unique(['email', 'phone'])
export default class UserEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @ApiProperty({ example: 'John' })
  @Column()
  public firstname: string;

  @ApiProperty({ example: 'Doe' })
  @Column()
  public lastname: string;

  @ApiProperty({ example: 'john.doe@animalert.com' })
  @Column()
  public email: string;

  @ApiProperty({ example: 'password' })
  @Column()
  public password: string;

  @ApiProperty({ example: '0612345678' })
  @Column()
  public phone: string;

  @ApiProperty({ example: 'avatar.png' })
  @Column({ nullable: false })
  public avatar: string;

  @ApiProperty({ example: '1 rue de la Paix' })
  @OneToOne(() => AddressEntity, (address) => address.user, { nullable: false })
  @JoinColumn()
  public address: AddressEntity;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @OneToMany(() => AnimalEntity, (animal) => animal.user)
  public animals: AnimalEntity[];

  @OneToMany(() => AlertEntity, (alert) => alert.user)
  public alerts: AlertEntity[];
}
