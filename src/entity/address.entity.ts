import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import UserEntity from './user.entity';
import { IAddressProperty } from '../interface/IAddressProperty';
import AlertEntity from './alert.entity';

@Entity('address')
export default class AddressEntity {
  constructor(address?: IAddressProperty) {
    if (address) {
      this.label = address.label;
      this.city = address.city;
      this.postcode = address.postcode;
      this.street = address.street;
      this.housenumber = address.housenumber;
      this.latitude = address.x;
      this.longitude = address.y;
    }
  }
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  public label: string;

  @Column()
  public city: string;

  @Column()
  public postcode: string;

  @Column()
  public street: string;

  @Column()
  public housenumber: string;

  @Column()
  public latitude: number;

  @Column()
  public longitude: number;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @OneToOne(() => UserEntity, (user) => user.address)
  public user: UserEntity;

  @OneToMany(() => AlertEntity, (alert) => alert.location)
  public alert: AlertEntity;
}
