import { EAnimalType } from '../enum/EAnimalType';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import UserEntity from './user.entity';
import PhotoEntity from './photo.entity';
import AlertEntity from './alert.entity';
import { EAnimalSex } from '../enum/EAnimalSex';

@Entity({ name: 'animal' })
@Unique(['trackNumber'])
export default class AnimalEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name: string;

  @Column({
    type: 'enum',
    enum: EAnimalType,
  })
  public type: EAnimalType;

  @Column({
    type: 'enum',
    enum: EAnimalSex,
  })
  public sex: EAnimalSex;

  @Column({ default: false, type: 'boolean' })
  public sterilized;

  @Column({ nullable: true })
  public breed: string | null;

  @Column({ nullable: true })
  public color: string | null;

  @Column({ nullable: true })
  public hair: string | null;

  @Column({ nullable: true })
  public trackNumber: string | null;

  @Column({ nullable: true })
  public description: string | null;

  @Column({ nullable: true })
  public weight: number | null;

  @Column({ nullable: true })
  public width: number | null;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @ManyToOne(() => UserEntity, (user) => user.animals, { nullable: false })
  public user: UserEntity;

  @OneToMany(() => PhotoEntity, (photo) => photo.animal, { cascade: true })
  public photos: PhotoEntity[];

  @OneToMany(() => AlertEntity, (alert) => alert.animal, { cascade: true })
  public alerts: AlertEntity[];
}
