import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { EAlertCategory } from '../enum/EAlertCategory';
import { EAnimalType } from '../enum/EAnimalType';
import { EAlertStatus } from '../enum/EAlertStatus';
import UserEntity from './user.entity';
import PhotoEntity from './photo.entity';
import AddressEntity from './address.entity';
import AnimalEntity from './animal.entity';
import { EAnimalSex } from '../enum/EAnimalSex';

@Entity({ name: 'alert' })
@Unique(['trackNumber'])
export default class AlertEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  public name: string | null;

  @Column({
    type: 'enum',
    enum: EAlertCategory,
  })
  public category: EAlertCategory;

  @Column({
    type: 'enum',
    enum: EAnimalType,
  })
  public type: EAnimalType;

  @Column({
    type: 'enum',
    enum: EAnimalSex,
    nullable: true,
  })
  public sex: EAnimalSex | null;

  @Column({ default: false, type: 'boolean' })
  public sterilized;

  @Column({ nullable: true })
  public breed: string | null;

  @Column({ nullable: true })
  public hair: string | null;

  @Column({ nullable: true })
  public color: string | null;

  @Column({ nullable: true })
  public description: string | null;

  @Column()
  public status: EAlertStatus;

  @Column({ nullable: true })
  public trackNumber: string | null;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @ManyToOne(() => AddressEntity, (address) => address.alert, {
    nullable: false,
  })
  @JoinColumn()
  public location: AddressEntity;

  @ManyToOne(() => UserEntity, (user) => user.alerts)
  public user: UserEntity;

  @OneToMany(() => PhotoEntity, (photo) => photo.alert)
  public photos: PhotoEntity[];

  @ManyToOne(() => AnimalEntity, (animal) => animal.alerts, {
    nullable: true,
  })
  public animal?: AnimalEntity | null;
}
