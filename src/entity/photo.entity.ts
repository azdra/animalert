import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import AnimalEntity from './animal.entity';
import AlertEntity from './alert.entity';

@Entity({ name: 'photo' })
export default class PhotoEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  public name: string;

  @Column()
  public path: string;

  @CreateDateColumn()
  public createdAt: Date;

  @UpdateDateColumn()
  public updatedAt: Date;

  @ManyToOne(() => AnimalEntity, (animal) => animal.photos)
  public animal: AnimalEntity;

  @ManyToOne(() => AlertEntity, (alert) => alert.photos)
  public alert: AlertEntity;
}
