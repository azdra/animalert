import { IProcessor } from 'typeorm-fixtures-cli';
import AlertEntity from '../entity/alert.entity';

export default class AlertProcessor implements IProcessor<AlertEntity> {
  async preProcess(name: string, object: any): Promise<any> {
    return { ...object };
  }

  postProcess(name: string, object: any): any {
    return { ...object };
  }
}
