import { IProcessor } from 'typeorm-fixtures-cli';
import UserEntity from '../entity/user.entity';
import * as argon2 from 'argon2';
import * as fs from 'fs';
import * as util from 'util';

export default class UserProcessor implements IProcessor<UserEntity> {
  async preProcess(name: string, object: UserEntity): Promise<any> {
    object.password = await argon2.hash(object.password);
    const files = fs.readdirSync('/public/avatar');
    object.avatar = util.format(
      '/avatar/%s',
      files[Math.floor(Math.random() * files.length)],
    );
    return { ...object };
  }

  postProcess(name: string, object: any): any {
    return { ...object };
  }
}
