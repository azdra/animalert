import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './module/app.module';
import { ConfigService } from '@nestjs/config';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import setupSwagger from './setupSwagger';
import * as express from 'express';

const configService = new ConfigService();
(async () => {
  const app = await NestFactory.create(AppModule);
  const reflector = app.get(Reflector);
  app.enableCors();
  app.setGlobalPrefix('api');

  app.use(express.static('/public'));

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalInterceptors(new ClassSerializerInterceptor(reflector));

  setupSwagger(app);
  await app.listen(configService.get('APP_PORT'));
})();
