import UserEntity from '../entity/user.entity';

export interface IVoter {
  entity;
  canAdd(user: UserEntity, entity): boolean;
  canUpdate(user: UserEntity, entity): boolean;
  canDelete(user: UserEntity, entity): boolean;
  canGet(user: UserEntity, entity): boolean;
  voteOnAttribute(user: UserEntity, entity): boolean;
}
