import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { nanoid } from 'nanoid';
import { extname } from 'path';
import { diskStorage } from 'multer';
import * as fs from 'fs';

export default {
  storage: diskStorage({
    destination: async (req, file, cb) => {
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = currentDate.getMonth() + 1;
      const day = currentDate.getDate();

      const path = `/public/${year}/${month}/${day}`;
      fs.mkdirSync(path, { recursive: true });
      cb(null, path);
    },
    filename: (req, file, cb) => {
      cb(null, `${nanoid()}${extname(file.originalname)}`);
    },
  }),
} as MulterOptions;
