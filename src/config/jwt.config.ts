import { JwtSignOptions } from '@nestjs/jwt/dist/interfaces';

export const jwtConstants: JwtSignOptions = {
  secret:
    'DO NOT USE THIS VALUE. INSTEAD, CREATE A COMPLEX SECRET AND KEEP IT SAFE OUTSIDE OF THE SOURCE CODE.',
};
