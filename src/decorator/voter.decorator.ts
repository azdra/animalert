import { applyDecorators, UseGuards } from '@nestjs/common';
import { AuthGuard } from '../guard/auth.guard';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { SecurityDecorator } from './security.decorator';
import AppException from '../exception/app.exception';
import { IVoter } from '../interface/IVoter';

export function VoterDecorator(voter: IVoter) {
  return applyDecorators(
    SecurityDecorator(voter),
    UseGuards(AuthGuard),
    ApiBearerAuth('JWT-auth'),
    ApiUnauthorizedResponse({
      description: 'Unauthorized',
      status: 401,
      type: AppException,
    }),
  );
}
