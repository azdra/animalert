import { IVoter } from '../interface/IVoter';
import typeormConfig from '../config/typeorm.config';
import { UnauthorizedException } from '@nestjs/common';

export function SecurityDecorator(voter: IVoter) {
  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor,
  ) {
    const originalMethod = descriptor.value;
    descriptor.value = async function (...args: any[]) {
      const [request] = args;
      const user = request['user'];
      if (request.params && request.params.id) {
        const repository = typeormConfig.getRepository(voter.entity);
        const entity = await repository.findOne({
          where: {
            id: parseInt(request.params.id),
          },
          relations: ['user'],
        });
        if (!entity) throw new UnauthorizedException();
        const check = voter.voteOnAttribute(user, entity);
        if (!check) throw new UnauthorizedException();
        return originalMethod.apply(this, args);
      }
      return originalMethod.apply(this, args);
    };
    return descriptor;
  };
}
