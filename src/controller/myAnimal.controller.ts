import AbstractController from '../abstract/controller.abstract';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Req,
  Res,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import MyAnimalApiService from '../apiService/myAnimal.apiService';
import { Request, Response } from 'express';
import CreateMyAnimalDto from '../dto/myAnimal/createMyAnimal.dto';
import AppException from '../exception/app.exception';
import { VoterDecorator } from '../decorator/voter.decorator';
import AnimalVoter from '../voter/animal.voter';
import { EVoter } from '../enum/EVoter';
import { FilesInterceptor } from '@nestjs/platform-express';
import multerConfig from '../config/multer.config';

@ApiTags('My Animal')
@Controller('animal/my-animal')
export default class MyAnimalController extends AbstractController {
  public constructor(private animalApiService: MyAnimalApiService) {
    super();
  }

  @ApiOperation({ summary: 'Add my animal' })
  @ApiResponse({
    status: 201,
    description: 'My animal added',
    type: CreateMyAnimalDto,
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @Post('/')
  @UseInterceptors(FilesInterceptor('files', null, multerConfig))
  @VoterDecorator(new AnimalVoter(EVoter.CREATE))
  public addMyAnimal(
    @Res() response: Response,
    @Req() request: Request,
    @UploadedFiles() files: Express.Multer.File[],
    @Body() data: CreateMyAnimalDto,
  ) {
    return this.handleRequest(response, {
      service: this.animalApiService,
      fn: 'addMyAnimal',
      args: [request.user, data, files],
    });
  }

  @ApiOperation({ summary: 'Update my animal' })
  @ApiResponse({
    status: 200,
    description: 'My animal updated',
    type: CreateMyAnimalDto,
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @Patch('/:id')
  @UseInterceptors(FilesInterceptor('files', null, multerConfig))
  @VoterDecorator(new AnimalVoter(EVoter.UPDATE))
  public updateMyAnimal(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
    @UploadedFiles() files: Express.Multer.File[],
    @Body() data: CreateMyAnimalDto,
  ) {
    return this.handleRequest(response, {
      service: this.animalApiService,
      fn: 'updateMyAnimal',
      args: [request.user, id, data, files],
    });
  }

  @ApiOperation({ summary: 'Delete my animal' })
  @ApiResponse({
    status: 200,
    description: 'My animal deleted',
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @Delete('/:id')
  @VoterDecorator(new AnimalVoter(EVoter.DELETE))
  public deleteMyAnimal(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    return this.handleRequest(response, {
      service: this.animalApiService,
      fn: 'deleteMyAnimal',
      args: [request.user, id],
    });
  }

  @ApiOperation({ summary: 'Get my animals' })
  @ApiResponse({
    status: 200,
    description: 'My animals',
    type: CreateMyAnimalDto,
    isArray: true,
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @Get('/')
  @VoterDecorator(new AnimalVoter(EVoter.CREATE))
  public getMyAnimals(@Res() response: Response, @Req() request: Request) {
    return this.handleRequest(response, {
      service: this.animalApiService,
      fn: 'getMyAnimals',
      args: [request.user],
    });
  }

  @ApiOperation({ summary: 'Get my animal' })
  @ApiResponse({
    status: 200,
    description: 'My animal',
    type: CreateMyAnimalDto,
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @Get('/:id')
  @VoterDecorator(new AnimalVoter(EVoter.GET))
  public getMyAnimal(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    return this.handleRequest(response, {
      service: this.animalApiService,
      fn: 'getMyAnimal',
      args: [request.user, id],
    });
  }

  @ApiOperation({ summary: 'Create alert' })
  @ApiResponse({
    status: 201,
    description: 'Alert created',
    type: CreateMyAnimalDto,
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @ApiResponse({ status: 401, description: 'Unauthorized', type: AppException })
  @Post('/:id/alert')
  @VoterDecorator(new AnimalVoter(EVoter.CREATE))
  public createAlert(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    return this.handleRequest(response, {
      service: this.animalApiService,
      fn: 'createAlert',
      args: [request.user, id],
    });
  }

  @ApiOperation({ summary: 'Remove one image' })
  @ApiResponse({
    status: 200,
    description: 'Image removed',
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @Delete('/:id/image/:imageId')
  @VoterDecorator(new AnimalVoter(EVoter.DELETE))
  public removeImageFromAnimal(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
    @Param('imageId') imageId: number,
  ) {
    return this.handleRequest(response, {
      service: this.animalApiService,
      fn: 'removeImageFromAnimal',
      args: [request.user, id, imageId],
    });
  }
}
