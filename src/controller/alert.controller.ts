import AbstractController from '../abstract/controller.abstract';
import {
  Body,
  Controller,
  Param,
  Post,
  Req,
  Res,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import CreateAlertDto from '../dto/alert/createAlert.dto';
import AlertApiService from '../apiService/alert.apiService';
import AppException from '../exception/app.exception';
import { VoterDecorator } from '../decorator/voter.decorator';
import AlertVoter from '../voter/alert.voter';
import { EVoter } from '../enum/EVoter';
import SearchAlertDto from '../dto/alert/searchAlert.dto';
import { AuthGuard } from '../guard/auth.guard';
import { FilesInterceptor } from '@nestjs/platform-express';
import multerConfig from '../config/multer.config';

@ApiTags('alert')
@Controller('alert')
export default class AlertController extends AbstractController {
  public constructor(private alertApiService: AlertApiService) {
    super();
  }

  @ApiOperation({ summary: 'Create alert' })
  @ApiResponse({
    status: 201,
    description: 'Alert created',
    type: CreateAlertDto,
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @ApiResponse({ status: 401, description: 'Unauthorized', type: AppException })
  @Post('/')
  @UseInterceptors(FilesInterceptor('files', null, multerConfig))
  @VoterDecorator(new AlertVoter(EVoter.CREATE))
  public createAlert(
    @Res() response: Response,
    @Req() request: Request,
    @UploadedFiles() files: Express.Multer.File[],
    @Body() data: CreateAlertDto,
  ) {
    return this.handleRequest(response, {
      service: this.alertApiService,
      fn: 'createAlert',
      args: [request.user, data, files],
    });
  }

  @ApiOperation({ summary: 'Resolve alert' })
  @ApiResponse({
    status: 201,
    description: 'Alert resolved',
    type: CreateAlertDto,
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @ApiResponse({ status: 401, description: 'Unauthorized', type: AppException })
  @Post('/:id/resolve')
  @VoterDecorator(new AlertVoter(EVoter.UPDATE))
  public resolveAlert(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    return this.handleRequest(response, {
      service: this.alertApiService,
      fn: 'resolveAlert',
      args: [request.user, id],
    });
  }

  @ApiOperation({ summary: 'Search alert' })
  @ApiResponse({
    status: 201,
    description: 'Alerts found',
    type: CreateAlertDto,
    isArray: true,
  })
  @Post('/search')
  public searchAlert(
    @Res() response: Response,
    @Body() search: SearchAlertDto,
  ) {
    return this.handleRequest(response, {
      service: this.alertApiService,
      fn: 'searchAlert',
      args: [search],
    });
  }

  @ApiOperation({ summary: 'Response to an alert' })
  @Post('/:id/response')
  @UseGuards(AuthGuard)
  public responseAlert(
    @Res() response: Response,
    @Req() request: Request,
    @Param('id') id: number,
  ) {
    return this.handleRequest(response, {
      service: this.alertApiService,
      fn: 'responseAlert',
      args: [request.user, id],
    });
  }

  @ApiOperation({ summary: 'receive the last 5 active or inactive alerts' })
  @ApiResponse({
    status: 201,
    description: 'Alerts found',
    type: CreateAlertDto,
    isArray: true,
  })
  @Post('/last')
  @VoterDecorator(new AlertVoter(EVoter.CREATE))
  public lastAlerts(
    @Res() response: Response,
    @Req() request: Request,
    @Body() search: SearchAlertDto,
  ) {
    return this.handleRequest(response, {
      service: this.alertApiService,
      fn: 'lastAlerts',
      args: [request.user, search],
    });
  }
}
