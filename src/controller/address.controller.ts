import AbstractController from '../abstract/controller.abstract';
import { ApiTags } from '@nestjs/swagger';
import { Body, Controller, Post, Res } from '@nestjs/common';
import { Response } from 'express';
import AddressApiService from '../apiService/address.apiService';

@ApiTags('address')
@Controller('address')
export default class AddressController extends AbstractController {
  public constructor(private addressApiService: AddressApiService) {
    super();
  }
  @Post('/')
  public async getAddressByZipCode(
    @Res() response: Response,
    @Body('address') address: string,
  ) {
    return this.handleRequest(response, {
      service: this.addressApiService,
      fn: 'searchAddressByLabel',
      args: [address],
    });
  }

  @Post('/:lat/:lng')
  public async getAddressByCoordinates(
    @Res() response: Response,
    @Body('lat') lat: number,
    @Body('lng') lng: number,
  ) {
    return this.handleRequest(response, {
      service: this.addressApiService,
      fn: 'searchAddressByCoordinates',
      args: [lat, lng],
    });
  }
}
