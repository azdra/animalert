import AbstractController from '../abstract/controller.abstract';
import {
  Body,
  Controller,
  Get,
  Patch,
  Req,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { Request, Response } from 'express';
import UserApiService from '../apiService/user.apiService';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import UserEntity from '../entity/user.entity';
import AppException from '../exception/app.exception';
import UpdateUserDto from '../dto/user/updateUser.dto';
import { VoterDecorator } from '../decorator/voter.decorator';
import { EVoter } from '../enum/EVoter';
import MyAccountVoter from '../voter/myAccount.voter';
import { UpdateUserAccountPasswordDto } from '../dto/user/updateUserAccountPassword.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import multerConfig from '../config/multer.config';

@ApiTags('user')
@Controller('/user')
export default class UserController extends AbstractController {
  public constructor(private userApiService: UserApiService) {
    super();
  }

  @ApiOperation({ summary: 'Get user account' })
  @ApiResponse({
    status: 200,
    description: 'Get user account',
    type: UserEntity,
  })
  @Get('/my-account')
  @VoterDecorator(new MyAccountVoter(EVoter.GET))
  public myAccount(
    @Res() response: Response,
    @Req() request: Request,
  ): Promise<unknown> {
    return this.handleRequest(response, {
      service: this.userApiService,
      fn: 'myAccount',
      args: [request.user],
    });
  }

  @ApiOperation({ summary: 'Update user account' })
  @ApiResponse({
    status: 200,
    description: 'Update user account',
    type: UserEntity,
  })
  @Patch('/my-account')
  @VoterDecorator(new MyAccountVoter(EVoter.UPDATE))
  public updateMyAccount(
    @Res() response: Response,
    @Req() request: Request,
    @Body() body: UpdateUserDto,
  ): Promise<unknown> {
    return this.handleRequest(response, {
      service: this.userApiService,
      fn: 'updateMyAccount',
      args: [request.user, body],
    });
  }

  @ApiOperation({ summary: 'Update user password' })
  @ApiResponse({
    status: 200,
    description: 'Update user password',
    type: UserEntity,
  })
  @ApiResponse({
    status: 400,
    description: 'The passwords do not match',
    type: AppException,
  })
  @Patch('/my-account/my-password')
  @VoterDecorator(new MyAccountVoter(EVoter.UPDATE))
  public updateMyPassword(
    @Res() response: Response,
    @Req() request: Request,
    @Body() body: UpdateUserAccountPasswordDto,
  ): Promise<unknown> {
    return this.handleRequest(response, {
      service: this.userApiService,
      fn: 'updateMyAccountPassword',
      args: [request.user, body],
    });
  }

  @ApiOperation({ summary: 'Update user email' })
  @ApiResponse({
    status: 200,
    description: 'Update user email',
    type: UserEntity,
  })
  @Patch('/my-account/my-email')
  @VoterDecorator(new MyAccountVoter(EVoter.UPDATE))
  public updateMyEmail(
    @Res() response: Response,
    @Req() request: Request,
    @Body('email') email: string,
  ): Promise<unknown> {
    return this.handleRequest(response, {
      service: this.userApiService,
      fn: 'updateMyEmail',
      args: [request.user, email],
    });
  }

  @ApiOperation({ summary: 'Update user phone number' })
  @ApiResponse({
    status: 200,
    description: 'Update user email',
    type: UserEntity,
  })
  @Patch('/my-account/my-phone-number')
  @VoterDecorator(new MyAccountVoter(EVoter.UPDATE))
  public updateMyPhoneNumber(
    @Res() response: Response,
    @Req() request: Request,
    @Body('phone') phone: string,
  ): Promise<unknown> {
    return this.handleRequest(response, {
      service: this.userApiService,
      fn: 'updateMyPhone',
      args: [request.user, phone],
    });
  }

  @ApiOperation({ summary: 'Update user profile picture' })
  @ApiResponse({
    status: 200,
    description: 'Update user profile picture',
    type: UserEntity,
  })
  @Patch('/my-account/my-avatar')
  @UseInterceptors(FileInterceptor('file', multerConfig))
  @VoterDecorator(new MyAccountVoter(EVoter.UPDATE))
  public updateProfilePicture(
    @Res() response: Response,
    @Req() request: Request,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<unknown> {
    return this.handleRequest(response, {
      service: this.userApiService,
      fn: 'updateProfilePicture',
      args: [request.user, file],
    });
  }
}
