import AbstractController from '../abstract/controller.abstract';
import AuthApiService from '../apiService/auth.apiService';
import { Body, Controller, Post, Res } from '@nestjs/common';
import { Response } from 'express';
import CreateUserDto from '../dto/user/createUser.dto';
import LoginUserDto from '../dto/user/loginUser.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import UserEntity from '../entity/user.entity';
import UserLoginModel from '../model/user/userLogin.model';
import AppException from '../exception/app.exception';

@Controller('auth')
@ApiTags('auth')
export class AuthController extends AbstractController {
  public constructor(private readonly authApiService: AuthApiService) {
    super();
  }

  @ApiOperation({ summary: 'Register user' })
  @ApiResponse({
    status: 201,
    description: 'User registered',
    type: UserEntity,
  })
  @ApiResponse({ status: 400, description: 'Bad request', type: AppException })
  @Post('/register')
  public register(
    @Res() response: Response,
    @Body() user: CreateUserDto,
  ): Promise<unknown> {
    return this.handleRequest(response, {
      service: this.authApiService,
      fn: 'register',
      args: [user],
    });
  }

  @ApiOperation({ summary: 'Login user' })
  @ApiResponse({
    status: 200,
    description: 'User logged in',
    type: UserLoginModel,
  })
  @ApiResponse({
    status: 404,
    description: 'User not found',
    type: AppException,
  })
  @ApiResponse({
    status: 400,
    description: 'Invalid password',
    type: AppException,
  })
  @Post('/login')
  public login(
    @Res() response: Response,
    @Body() data: LoginUserDto,
  ): Promise<unknown> {
    return this.handleRequest(response, {
      service: this.authApiService,
      fn: 'login',
      args: [data.username, data.password],
    });
  }
}
