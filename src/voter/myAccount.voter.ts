import { IVoter } from '../interface/IVoter';
import AbstractVoter from '../abstract/voter.abstract';
import UserEntity from '../entity/user.entity';

export default class MyAccountVoter extends AbstractVoter implements IVoter {
  public entity = UserEntity;

  public canAdd(): boolean {
    return true;
  }

  public canUpdate(): boolean {
    return true;
  }

  public canDelete(): boolean {
    return true;
  }

  public canGet(): boolean {
    return true;
  }
}
