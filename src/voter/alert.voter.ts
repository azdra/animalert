import { IVoter } from '../interface/IVoter';
import AbstractVoter from '../abstract/voter.abstract';
import UserEntity from '../entity/user.entity';
import AlertEntity from '../entity/alert.entity';

export default class AlertVoter extends AbstractVoter implements IVoter {
  public entity = AlertVoter;

  public canAdd(): boolean {
    return true;
  }

  public canUpdate(user: UserEntity, entity: AlertEntity): boolean {
    return user.id === entity.user.id;
  }

  public canDelete(user: UserEntity, entity: AlertEntity): boolean {
    return user.id === entity.user.id;
  }

  public canGet(user: UserEntity, entity: AlertEntity): boolean {
    return user.id === entity.user.id;
  }
}
