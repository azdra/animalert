import { IVoter } from '../interface/IVoter';
import AnimalEntity from '../entity/animal.entity';
import AbstractVoter from '../abstract/voter.abstract';
import UserEntity from '../entity/user.entity';

export default class AnimalVoter extends AbstractVoter implements IVoter {
  public entity = AnimalEntity;

  public canAdd(): boolean {
    return true;
  }

  public canUpdate(user: UserEntity, entity: AnimalEntity): boolean {
    return user.id === entity.user.id;
  }

  public canDelete(user: UserEntity, entity: AnimalEntity): boolean {
    return user.id === entity.user.id;
  }

  public canGet(user: UserEntity, entity: AnimalEntity): boolean {
    return user.id === entity.user.id;
  }
}
