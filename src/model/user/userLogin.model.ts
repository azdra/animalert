import UserEntity from '../../entity/user.entity';
import { ApiProperty } from '@nestjs/swagger';

export default class UserLoginModel extends UserEntity {
  @ApiProperty({ example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9' })
  public access_token: string;
}
