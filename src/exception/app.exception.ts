import { ApiProperty } from '@nestjs/swagger';

export default class AppException extends Error {
  @ApiProperty({ example: 400 })
  public status: number;

  @ApiProperty({ example: 'Bad request' })
  public message: string;
  constructor(message: string, status = 400) {
    super(message);
    this.status = status;
  }
}
