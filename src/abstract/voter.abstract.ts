import { EVoter } from '../enum/EVoter';
import UserEntity from '../entity/user.entity';

export default abstract class AbstractVoter {
  constructor(private attribute: EVoter) {}

  public abstract canAdd(user: UserEntity): boolean;
  public abstract canUpdate(user: UserEntity, entity): boolean;
  public abstract canDelete(user: UserEntity, entity): boolean;
  public abstract canGet(user: UserEntity, entity): boolean;

  public voteOnAttribute(user: UserEntity, entity): boolean {
    switch (this.attribute) {
      case EVoter.CREATE:
        return this.canAdd(user);
      case EVoter.UPDATE:
        return this.canUpdate(user, entity);
      case EVoter.DELETE:
        return this.canDelete(user, entity);
      case EVoter.GET:
        return this.canGet(user, entity);
      default:
        return false;
    }
  }
}
