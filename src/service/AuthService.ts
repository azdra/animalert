import UserEntity from '../entity/user.entity';
import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from '../config/jwt.config';

@Injectable()
export default class AuthService {
  constructor(private jwtService: JwtService) {}
  public async signToken(user: UserEntity): Promise<Record<string, string>> {
    const payload = { sub: user.id, username: user.email };
    return {
      access_token: await this.jwtService.signAsync(payload, jwtConstants),
    };
  }

  public verifyToken(token: string) {
    return this.jwtService.verify(token, jwtConstants);
  }
}
