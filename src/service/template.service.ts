import { Injectable } from '@nestjs/common';
import * as Twig from 'twig';
import * as util from 'util';

@Injectable()
export default class TemplateService {
  public render = async (template: string, data: any = {}) => {
    return new Promise((resolve, reject) => {
      Twig.renderFile(
        util.format('templates/%s', template),
        data,
        (err, html) => {
          if (err) {
            reject(err);
          } else {
            resolve(html);
          }
        },
      );
    });
  };
}
