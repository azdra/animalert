import { Injectable } from '@nestjs/common';
import Mail from 'nodemailer/lib/mailer';
import * as nodemailer from 'nodemailer';

@Injectable()
export default class EmailService {
  public send = (options: Mail.Options) => {
    const transporter = nodemailer.createTransport({
      host: 'maildev',
      port: 1025,
    });

    return transporter.sendMail(options, function (error, info) {
      if (error) {
        throw new Error(
          "Erreur lors de l'envoi de l'e-mail : " + error.message,
        );
      } else {
        console.log('E-mail envoyé: ' + info.response);
      }
    });
  };
}
