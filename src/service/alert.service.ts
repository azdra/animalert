import AnimalEntity from '../entity/animal.entity';
import AlertEntity from '../entity/alert.entity';
import { AlertRepository } from '../repository/alert.repository';
import { Injectable } from '@nestjs/common';
import { EAlertCategory } from '../enum/EAlertCategory';
import { EAlertStatus } from '../enum/EAlertStatus';
import EmailService from './email.service';
import TemplateService from './template.service';
import UserEntity from '../entity/user.entity';

@Injectable()
export default class AlertService {
  constructor(
    private emailService: EmailService,
    private templateService: TemplateService,
  ) {}

  public createAlertFromAnimal(animal: AnimalEntity): AlertEntity {
    const alert = AlertRepository.merge(new AlertEntity(), animal);
    delete alert.id;
    alert.category = EAlertCategory.LOST;
    alert.status = EAlertStatus.NEW;
    alert.location = animal.user.address;
    alert.user = animal.user;
    alert.animal = animal;
    return alert;
  }

  public async sendEmailToAlertCreator(alert: AlertEntity, user: UserEntity) {
    return this.emailService.send({
      to: alert.user.email,
      subject: 'Message concernant votre alerte',
      html: await this.templateService.render('alert/responseAlert.html.twig', {
        user: alert.user,
        sender: user,
      }),
    });
  }

  public async lastAlerts(user: UserEntity) {
    return {
      active: await AlertRepository.lastAlerts(EAlertStatus.NEW, user),
      resolved: await AlertRepository.lastAlerts(EAlertStatus.RESOLVED, user),
    };
  }
}
