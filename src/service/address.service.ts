import axios from 'axios';
import { IAddressProperty } from '../interface/IAddressProperty';

export default class AddressService {
  public async searchAddressByLabel(label): Promise<IAddressProperty[]> {
    const uri = `https://api-adresse.data.gouv.fr/search/?q=${encodeURIComponent(
      label,
    )}&limit=7`;
    return new Promise((resolve) => {
      return axios.get(uri).then((res) => {
        resolve(res.data.features.map((feature) => feature.properties));
      });
    });
  }

  public async searchAddressByCoordinates(
    lat: number,
    lng: number,
  ): Promise<IAddressProperty[]> {
    const uri = `https://api-adresse.data.gouv.fr/reverse/?lat=${lat}&lon=${lng}&limit=7`;
    return new Promise((resolve) => {
      return axios.get(uri).then((res) => {
        resolve(res.data.features.map((feature) => feature.properties));
      });
    });
  }

  public async countSearchAddressByLabel(label): Promise<number> {
    return this.searchAddressByLabel(label).then((res) => res.length);
  }
}
