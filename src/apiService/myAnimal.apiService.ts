import CreateMyAnimalDto from '../dto/myAnimal/createMyAnimal.dto';
import UserEntity from '../entity/user.entity';
import AnimalEntity from '../entity/animal.entity';
import { AnimalRepository } from '../repository/animal.repository';
import AppException from '../exception/app.exception';
import { Injectable } from '@nestjs/common';
import { AlertRepository } from '../repository/alert.repository';
import { EAlertStatus } from '../enum/EAlertStatus';
import { PhotoRepository } from '../repository/photo.repository';
import AlertService from '../service/alert.service';
import AlertEntity from '../entity/alert.entity';

@Injectable()
export default class MyAnimalApiService {
  public constructor(private alertService: AlertService) {}
  public async addMyAnimal(
    user: UserEntity,
    data: CreateMyAnimalDto,
    files: Express.Multer.File[],
  ): Promise<AnimalEntity[]> {
    const animal = AnimalRepository.merge(new AnimalEntity(), data);
    animal.sterilized = false;
    animal.user = user;
    animal.alerts = [];
    animal.photos = [];
    await AnimalRepository.save(animal);

    if (files && files.length) {
      for (const file of files) {
        const image = PhotoRepository.create();
        image.name = file.filename;
        image.animal = animal;
        image.path = file.path.replace('public/', '');
        await PhotoRepository.save(image);
      }
    }

    return AnimalRepository.findAnimalFromUser(user);
  }

  public async updateMyAnimal(
    user: UserEntity,
    id: number,
    data: CreateMyAnimalDto,
    files: Express.Multer.File[],
  ): Promise<AnimalEntity[]> {
    const animal = await AnimalRepository.findOneById(id);
    if (!animal) throw new AppException('Animal not found', 404);

    animal.sterilized = data.sterilized === 'true';
    animal.name = data.name;
    animal.type = data.type;
    animal.sex = data.sex;
    animal.breed = data.breed;
    animal.color = data.color;
    animal.hair = data.hair;
    animal.trackNumber = data.trackNumber;
    animal.description = data.description;
    animal.weight = data.weight;
    animal.width = data.width;
    await AnimalRepository.update(animal.id, animal);

    animal.alerts = animal?.alerts ?? [];
    animal.photos = animal?.photos ?? [];

    if (files && files.length) {
      for (const file of files) {
        const image = PhotoRepository.create();
        image.name = file.filename;
        image.animal = animal;
        image.path = file.path.replace('public/', '');
        await PhotoRepository.save(image);
      }
    }

    return AnimalRepository.findAnimalFromUser(user);
  }

  public async deleteMyAnimal(
    user: UserEntity,
    id: number,
  ): Promise<AnimalEntity[]> {
    const animal = await AnimalRepository.findOneAnimalFromUser(user, id);
    if (!animal) throw new AppException('Animal not found', 404);
    if (animal.alerts.length) {
      const check = animal.alerts.filter(
        (alert) => alert.status === EAlertStatus.NEW,
      ).length;
      if (check > 0) {
        throw new AppException(
          'Vous ne pouvez pas supprimer un animal avec une alerte',
          400,
        );
      }
    }

    await AnimalRepository.delete(animal.id);

    return AnimalRepository.findAnimalFromUser(user);
  }

  public async getMyAnimals(user: UserEntity): Promise<AnimalEntity[]> {
    return AnimalRepository.findAnimalFromUser(user);
  }

  public async getMyAnimal(
    user: UserEntity,
    id: number,
  ): Promise<AnimalEntity> {
    const animal = await AnimalRepository.findOneAnimalFromUser(user, id);
    if (!animal) throw new AppException('Animal not found', 404);
    return animal;
  }

  public async createAlert(
    user: UserEntity,
    id: number,
  ): Promise<{
    animals: AnimalEntity[];
    alerts: { active: AlertEntity[]; resolved: AlertEntity[] };
  }> {
    const animal = await AnimalRepository.findOneAnimalFromUser(user, id);
    if (!animal) throw new AppException('Animal not found', 404);

    const checkAlertAlreadyExist = await AlertRepository.findOneAlertFromAnimal(
      animal,
    );
    if (checkAlertAlreadyExist)
      throw new AppException(
        'Vous avez déjà une alert pour cette bestiol',
        400,
      );

    const alert = await this.alertService.createAlertFromAnimal(animal);
    await AlertRepository.save(alert);
    return {
      animals: await AnimalRepository.findAnimalFromUser(user),
      alerts: await this.alertService.lastAlerts(user),
    };
  }

  public async removeImageFromAnimal(
    user: UserEntity,
    id: number,
    imageId: number,
  ): Promise<string> {
    const image = await PhotoRepository.findOneByAnimalImage(id, imageId);
    if (!image) throw new AppException('Image not found', 404);
    await PhotoRepository.delete(image.id);
    return image;
  }
}
