import UserEntity from '../entity/user.entity';
import CreateAlertDto from '../dto/alert/createAlert.dto';
import { AlertRepository } from '../repository/alert.repository';
import AlertEntity from '../entity/alert.entity';
import { EAlertStatus } from '../enum/EAlertStatus';
import AppException from '../exception/app.exception';
import SearchAlertDto from '../dto/alert/searchAlert.dto';
import AddressService from '../service/address.service';
import AddressEntity from '../entity/address.entity';
import { AddressRepository } from '../repository/address.repository';
import { Injectable } from '@nestjs/common';
import { EAlertCategory } from '../enum/EAlertCategory';
import { EAnimalType } from '../enum/EAnimalType';
import AlertService from '../service/alert.service';
import { PhotoRepository } from '../repository/photo.repository';

@Injectable()
export default class AlertApiService {
  constructor(
    private addressService: AddressService,
    private alertService: AlertService,
  ) {}
  public async createAlert(
    user: UserEntity,
    data: CreateAlertDto,
    files: Express.Multer.File[],
  ) {
    const checkAddressFound = await this.addressService.searchAddressByLabel(
      data.location,
    );
    if (checkAddressFound.length === 0 || checkAddressFound.length > 1)
      throw new AppException('Address not found', 404);

    const location = new AddressEntity(checkAddressFound[0]);
    await AddressRepository.save(location);

    const newAlert = { ...data, location };
    const alert = AlertRepository.merge(new AlertEntity(), newAlert);

    alert.sterilized = data.sterilized === 'true';
    alert.user = user;
    alert.status = EAlertStatus.NEW;
    await AlertRepository.save(alert);

    if (files && files.length) {
      for (const file of files) {
        const image = PhotoRepository.create();
        image.name = file.filename;
        image.alert = alert;
        image.path = file.path.replace('public/', '');
        await PhotoRepository.save(image);
      }
    }

    return this.alertService.lastAlerts(user);
  }

  public async resolveAlert(user: UserEntity, id: number) {
    const alert = await AlertRepository.findAlertByUserAndId(user, id);
    if (!alert) throw new AppException('Alert not found', 404);
    alert.status = EAlertStatus.RESOLVED;
    await AlertRepository.save(alert);
    return this.alertService.lastAlerts(user);
  }

  public async searchAlert(params: SearchAlertDto) {
    return AlertRepository.searchAlert(params);
  }

  public transFormType(type: EAnimalType) {
    switch (type) {
      case EAnimalType.CAT:
        return 'Chat';
      case EAnimalType.DOG:
        return 'Chien';
      case EAnimalType.OTHER:
        return 'Autre';
      default:
        return type;
    }
  }

  public transFormCategory(category: EAlertCategory) {
    switch (category) {
      case EAlertCategory.LOST:
        return 'perdu';
      case EAlertCategory.FOUND:
        return 'trouvé';
      default:
        return category;
    }
  }

  public async responseAlert(user: UserEntity, id: number) {
    const alert = await AlertRepository.findOne({
      where: { id },
      relations: ['user'],
    });
    if (!alert) throw new AppException('Alert not found');

    return this.alertService.sendEmailToAlertCreator(alert, user);
  }
}
