import UserEntity from '../entity/user.entity';
import AppException from '../exception/app.exception';
import UpdateUserDto from '../dto/user/updateUser.dto';
import { UserRepository } from '../repository/user.repository';
import * as argon2 from 'argon2';
import { Injectable } from '@nestjs/common';
import AddressService from '../service/address.service';
import { AddressRepository } from '../repository/address.repository';
import AddressEntity from '../entity/address.entity';
import { UpdateUserAccountPasswordDto } from '../dto/user/updateUserAccountPassword.dto';
import { PhotoRepository } from '../repository/photo.repository';

@Injectable()
export default class UserApiService {
  constructor(private addressService: AddressService) {}

  public myAccount(user: UserEntity) {
    return user;
  }

  public async updateMyAccount(user: UserEntity, body: UpdateUserDto) {
    const checkAddressFound = await this.addressService.searchAddressByLabel(
      body.address,
    );
    if (checkAddressFound.length === 0 || checkAddressFound.length > 1)
      throw new AppException('Address not found', 404);

    await AddressRepository.update(
      user.address.id,
      new AddressEntity(checkAddressFound[0]),
    );
    const address = await AddressRepository.findOneBy({ id: user.address.id });
    const updateUser = { ...body, address };
    await UserRepository.update(user.id, updateUser);
    return updateUser;
  }

  public async updateMyAccountPassword(
    user: UserEntity,
    body: UpdateUserAccountPasswordDto,
  ) {
    if (!(await argon2.verify(user.password, body.oldPassword)))
      throw new AppException('The old password is incorrect');

    if (body.password !== body.confirmPassword)
      throw new AppException('The passwords do not match');

    const updatedUser = await UserRepository.findOneById(user.id);
    updatedUser.password = await argon2.hash(body.password);

    await UserRepository.update(user.id, updatedUser);
    return user;
  }

  public async updateMyEmail(user: UserEntity, email: string) {
    user.email = email;
    await UserRepository.update(user.id, { email });
    return user;
  }

  public async updateMyPhone(user: UserEntity, phone: string) {
    user.phone = phone;
    await UserRepository.update(user.id, { phone });
    return user;
  }

  public async updateProfilePicture(
    user: UserEntity,
    file: Express.Multer.File,
  ) {
    if (!file) throw new AppException('No file provided', 400);

    const image = PhotoRepository.create();
    image.name = file.filename;
    image.path = file.path.replace('public/', '');
    await PhotoRepository.save(image);

    user.avatar = image.path;
    await UserRepository.update(user.id, { avatar: user.avatar });
    return user;
  }
}
