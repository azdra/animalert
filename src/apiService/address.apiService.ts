import { Injectable } from '@nestjs/common';
import AddressService from '../service/address.service';

@Injectable()
export default class AddressApiService {
  constructor(private addressService: AddressService) {}
  public async searchAddressByLabel(address: string) {
    return this.addressService.searchAddressByLabel(address);
  }

  public async searchAddressByCoordinates(lat: number, lng: number) {
    return this.addressService.searchAddressByCoordinates(lat, lng);
  }
}
