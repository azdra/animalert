import { Injectable } from '@nestjs/common';
import CreateUserDto from '../dto/user/createUser.dto';
import AppException from '../exception/app.exception';
import UserEntity from '../entity/user.entity';
import * as argon2 from 'argon2';
import { UserRepository } from '../repository/user.repository';
import AuthService from '../service/AuthService';
import AddressService from '../service/address.service';
import AddressEntity from '../entity/address.entity';
import { AddressRepository } from '../repository/address.repository';
import * as fs from 'fs';
import * as util from 'util';
import AlertService from '../service/alert.service';

@Injectable()
export default class AuthApiService {
  constructor(
    private authService: AuthService,
    private addressService: AddressService,
    private alertService: AlertService,
  ) {}

  public async register(data: CreateUserDto): Promise<unknown> {
    const userExists = await UserRepository.findByEmailOrPhone(data.email);
    if (userExists) throw new AppException('Phone or email already exist', 409);

    if (data.password !== data.confirmPassword)
      throw new AppException('The passwords do not match');

    const checkAddressFound = await this.addressService.searchAddressByLabel(
      data.address,
    );
    if (checkAddressFound.length === 0 || checkAddressFound.length > 1)
      throw new AppException('Address not found', 404);

    const address = new AddressEntity(checkAddressFound[0]);
    await AddressRepository.save(address);
    const newUser = {
      ...data,
      address,
    };

    const user = UserRepository.merge(new UserEntity(), newUser);
    user.password = await argon2.hash(data.password);
    const files = fs.readdirSync('/public/avatar');
    user.avatar = util.format(
      '/avatar/%s',
      files[Math.floor(Math.random() * files.length)],
    );
    await UserRepository.save(user);

    return user;
  }

  public async login(username: string, password: string): Promise<unknown> {
    const user = await UserRepository.findByEmailOrPhone(username);
    if (!user) throw new AppException('User not found', 404);

    const validPassword = await argon2.verify(user.password, password);
    if (!validPassword) throw new AppException('Invalid password');

    user.alerts = await this.alertService.lastAlerts(user);

    return { ...user, ...(await this.authService.signToken(user)) };
  }
}
