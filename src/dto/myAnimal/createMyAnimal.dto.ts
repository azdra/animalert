import { EAnimalType } from '../../enum/EAnimalType';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { EAnimalSex } from '../../enum/EAnimalSex';

export default class CreateMyAnimalDto {
  @ApiProperty({ example: 'Kiera' })
  @IsString()
  public name: string;

  @ApiProperty({ example: EAnimalType.CAT, enum: EAnimalType })
  @IsEnum(EAnimalType)
  public type: EAnimalType;

  @ApiProperty({ example: EAnimalSex.FEMALE, enum: EAnimalSex })
  @IsEnum(EAnimalSex)
  public sex: EAnimalSex;

  @ApiPropertyOptional({ example: false })
  public sterilized;

  @ApiPropertyOptional({ example: 'Europeen' })
  public breed: string | null = null;

  @ApiPropertyOptional({ example: 'black' })
  public color: string | null = null;

  @ApiPropertyOptional({ example: 'long' })
  public hair: string | null = null;

  @ApiPropertyOptional({ example: '123456789' })
  public trackNumber: string | null = null;

  @ApiPropertyOptional({ example: 'description' })
  public description: string | null = null;

  @ApiPropertyOptional({ example: 10 })
  public weight: number | null = null;

  @ApiPropertyOptional({ example: 10 })
  public width: number | null = null;
}
