import { EAnimalType } from '../../enum/EAnimalType';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { EAlertCategory } from '../../enum/EAlertCategory';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { EAlertStatus } from '../../enum/EAlertStatus';

export default class SearchAlertDto {
  @ApiPropertyOptional({ example: '1Z 999 AA1 01 2345 6784' })
  @IsString()
  @IsOptional()
  public trackNumber?: string;

  @ApiPropertyOptional({
    enum: EAlertCategory,
    type: EAlertCategory,
    example: EAlertCategory.LOST,
  })
  @IsEnum(EAlertCategory)
  @IsOptional()
  public category?: EAlertCategory;

  @ApiPropertyOptional({
    enum: EAnimalType,
    type: EAnimalType,
    example: EAnimalType.CAT,
  })
  @IsEnum(EAnimalType)
  @IsOptional()
  public type?: EAnimalType;

  @ApiPropertyOptional({
    enum: EAlertStatus,
    type: EAlertStatus,
    example: EAlertStatus.NEW,
  })
  @IsEnum(EAlertStatus)
  @IsOptional()
  public status?: EAlertStatus;

  @ApiPropertyOptional({ example: 20 })
  @IsOptional()
  public take?: number = 20;

  @ApiPropertyOptional({ example: 1 })
  @IsOptional()
  public page?: number = 1;
}
