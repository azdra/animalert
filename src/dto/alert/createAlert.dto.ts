import { EAlertCategory } from '../../enum/EAlertCategory';
import { EAnimalType } from '../../enum/EAnimalType';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import PhotoEntity from '../../entity/photo.entity';
import { EAnimalSex } from '../../enum/EAnimalSex';

export default class CreateAlertDto {
  @ApiProperty({ example: 'Kiera' })
  @IsOptional()
  @IsString()
  public name: string | null;

  @ApiProperty({ example: EAlertCategory.LOST, enum: EAlertCategory })
  @IsEnum(EAlertCategory)
  public category: EAlertCategory;

  @ApiProperty({ example: EAnimalType.CAT, enum: EAnimalType })
  @IsEnum(EAnimalType)
  public type: EAnimalType;

  @ApiProperty({ example: EAnimalSex.FEMALE, enum: EAnimalSex })
  @IsOptional()
  @IsEnum(EAnimalSex)
  public sex: EAnimalSex | null;

  @ApiPropertyOptional({ example: false })
  public sterilized;

  @ApiPropertyOptional({ example: 'Europeen' })
  @IsOptional()
  @IsString()
  public breed: string | null;

  @ApiPropertyOptional({ example: 'long' })
  @IsOptional()
  @IsString()
  public hair: string | null;

  @ApiPropertyOptional({ example: 'black' })
  @IsOptional()
  @IsString()
  public color: string | null;

  @ApiPropertyOptional({ example: 'description' })
  @IsOptional()
  @IsString()
  public description: string | null;

  @ApiPropertyOptional({ example: '123456789' })
  @IsOptional()
  @IsString()
  public trackNumber: string | null;

  @ApiProperty({ example: '1 rue de la paix' })
  @IsString()
  public location: string;

  @ApiProperty({ type: PhotoEntity, isArray: true })
  @IsOptional()
  public photos: PhotoEntity[] | null;
}
