import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsPhoneNumber } from 'class-validator';
import UpdateUserDto from './updateUser.dto';

export default class UserDto extends UpdateUserDto {
  @ApiProperty({ example: 'john.doe@animalert.com' })
  @IsEmail()
  public email: string;

  @ApiProperty({ example: '0612345678' })
  @IsPhoneNumber('FR')
  public phone: string;
}
