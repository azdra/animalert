import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export default class UpdateUserDto {
  @ApiProperty({ example: 'John' })
  @IsString()
  public firstname: string;

  @ApiProperty({ example: 'Doe' })
  @IsString()
  public lastname: string;

  @ApiProperty({ example: '1 rue de la Paix' })
  @IsString()
  public address: string;
}
