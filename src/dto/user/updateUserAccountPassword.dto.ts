import UpdateUserPasswordDto from './updateUserPassword.dto';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateUserAccountPasswordDto extends UpdateUserPasswordDto {
  @ApiProperty({ example: 'password' })
  public oldPassword: string;
}
