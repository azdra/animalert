import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export default class LoginUserDto {
  @ApiProperty({ example: 'User email or phone' })
  @IsString()
  public username: string;

  @ApiProperty({ example: 'User password' })
  public password: string;
}
