import { IsStrongPassword } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export const passwordOptions = {
  minLength: 8,
  minLowercase: 1,
  minUppercase: 1,
  minNumbers: 1,
  minSymbols: 1,
};

export default class UpdateUserPasswordDto {
  @ApiProperty({ example: 'password' })
  @IsStrongPassword(passwordOptions)
  public password: string;

  @ApiProperty({ example: 'password' })
  @IsStrongPassword(passwordOptions)
  public confirmPassword: string;
}
