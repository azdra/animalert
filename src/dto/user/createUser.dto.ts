import { IsEmail, IsPhoneNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import UpdateUserPasswordDto from './updateUserPassword.dto';

export default class CreateUserDto extends UpdateUserPasswordDto {
  @ApiProperty({ example: 'John' })
  @IsString()
  public firstname: string;

  @ApiProperty({ example: 'Doe' })
  @IsString()
  public lastname: string;

  @ApiProperty({ example: 'john.doe@animalert.com' })
  @IsEmail()
  public email: string;

  @ApiProperty({ example: '0612345678' })
  @IsPhoneNumber('FR')
  public phone: string;

  @ApiProperty({ example: '1 rue de la Paix 38000 Grenoble' })
  @IsString()
  public address: string;
}
