import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import AuthService from '../service/AuthService';
import { Request } from 'express';
import { UserRepository } from '../repository/user.repository';
import AlertService from '../service/alert.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private alertService: AlertService,
  ) {}
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);
    if (!token) throw new UnauthorizedException();

    const payload = this.authService.verifyToken(token);
    const user = await UserRepository.findByEmailOrPhone(payload.username);
    if (!user) throw new UnauthorizedException();
    user.alerts = await this.alertService.lastAlerts(user);
    request['user'] = user;
    return true;
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
