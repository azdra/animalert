import { Module } from '@nestjs/common';
import MyAnimalController from '../controller/myAnimal.controller';
import MyAnimalApiService from '../apiService/myAnimal.apiService';
import AuthService from '../service/AuthService';
import AlertService from '../service/alert.service';
import EmailService from '../service/email.service';
import TemplateService from '../service/template.service';

@Module({
  controllers: [MyAnimalController],
  providers: [
    MyAnimalApiService,
    AuthService,
    AlertService,
    EmailService,
    TemplateService,
  ],
})
export default class MyAnimalModule {}
