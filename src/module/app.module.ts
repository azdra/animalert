import { Module } from '@nestjs/common';
import { DatabaseModule } from './database.module';
import AuthModule from './auth.module';
import { JwtModule } from './jwt.module';
import UserModule from './user.module';
import MyAnimalModule from './myAnimal.module';
import AlertModule from './alert.module';
import AddressModule from './address.module';

@Module({
  imports: [
    DatabaseModule,
    JwtModule,
    AuthModule,
    UserModule,
    MyAnimalModule,
    AlertModule,
    AddressModule,
  ],
})
export class AppModule {}
