import { Module } from '@nestjs/common';
import UserController from '../controller/user.controller';
import UserApiService from '../apiService/user.apiService';
import AuthService from '../service/AuthService';
import AddressService from '../service/address.service';
import AlertService from '../service/alert.service';
import EmailService from '../service/email.service';
import TemplateService from '../service/template.service';

@Module({
  controllers: [UserController],
  providers: [
    UserApiService,
    AuthService,
    AddressService,
    AlertService,
    EmailService,
    TemplateService,
  ],
})
export default class UserModule {}
