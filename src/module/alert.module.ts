import { Module } from '@nestjs/common';
import AlertController from '../controller/alert.controller';
import AlertApiService from '../apiService/alert.apiService';
import AuthService from '../service/AuthService';
import AddressService from '../service/address.service';
import AlertService from '../service/alert.service';
import EmailService from '../service/email.service';
import TemplateService from '../service/template.service';

@Module({
  controllers: [AlertController],
  providers: [
    AlertApiService,
    AlertService,
    AuthService,
    AddressService,
    EmailService,
    TemplateService,
  ],
})
export default class AlertModule {}
