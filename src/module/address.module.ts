import { Module } from '@nestjs/common';
import AddressController from '../controller/address.controller';
import { HttpModule } from '@nestjs/axios';
import AddressApiService from '../apiService/address.apiService';
import AddressService from '../service/address.service';

@Module({
  imports: [HttpModule],
  controllers: [AddressController],
  providers: [AddressApiService, AddressService],
})
export default class AddressModule {}
