import { Module } from '@nestjs/common';
import { jwtConstants } from '../config/jwt.config';
import { JwtModule as BaseJwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    BaseJwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '30d' },
    }),
  ],
})
export class JwtModule {}
