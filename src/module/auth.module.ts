import { Module } from '@nestjs/common';
import { AuthController } from '../controller/auth.controller';
import AuthApiService from '../apiService/auth.apiService';
import AuthService from '../service/AuthService';
import AddressService from '../service/address.service';
import AlertService from '../service/alert.service';
import EmailService from '../service/email.service';
import TemplateService from '../service/template.service';

@Module({
  controllers: [AuthController],
  providers: [
    AuthApiService,
    AuthService,
    AddressService,
    AlertService,
    EmailService,
    TemplateService,
  ],
})
export default class AuthModule {}
