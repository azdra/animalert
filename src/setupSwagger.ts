import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

const documentBuilder = new DocumentBuilder()
  .setTitle('Animalert')
  .setDescription('The Animalert API description')
  .setVersion('1.0')
  .addBearerAuth(
    {
      type: 'http',
      scheme: 'bearer',
      bearerFormat: 'JWT',
      name: 'JWT',
      description: 'Enter JWT token',
      in: 'header',
    },
    'JWT-auth',
  )
  .build();

export default (app: any) => {
  const document = SwaggerModule.createDocument(app, documentBuilder);
  SwaggerModule.setup('api-documentation', app, document, {
    swaggerOptions: { defaultModelsExpandDepth: -1 },
  });
};
