import typeormConfig from '../config/typeorm.config';
import AlertEntity from '../entity/alert.entity';
import UserEntity from '../entity/user.entity';
import SearchAlertDto from '../dto/alert/searchAlert.dto';
import { EAlertStatus } from '../enum/EAlertStatus';
import AnimalEntity from '../entity/animal.entity';

export const AlertRepository = typeormConfig.getRepository(AlertEntity).extend({
  findAlertByUserAndId(user: UserEntity, id: number) {
    return this.createQueryBuilder('alert')
      .where('alert.id = :id')
      .andWhere('alert.user = :user')
      .setParameter('user', user.id)
      .setParameter('id', id)
      .getOne();
  },
  searchAlert(params: SearchAlertDto) {
    const take = params.take || 10;
    const page = params.page || 1;
    const skip = (page - 1) * take;
    const qb = AlertRepository.createQueryBuilder('alert');
    qb.leftJoinAndSelect('alert.location', 'location');
    qb.leftJoinAndSelect('alert.photos', 'photos');

    if (params.trackNumber)
      qb.andWhere('alert.trackNumber = :trackNumber').setParameter(
        'trackNumber',
        params.trackNumber,
      );

    if (params.category)
      qb.andWhere('alert.category = :category').setParameter(
        'category',
        params.category,
      );

    if (params.type)
      qb.andWhere('alert.type = :type').setParameter('type', params.type);

    if (params.status)
      qb.andWhere('alert.status = :status').setParameter(
        'status',
        params.status,
      );

    qb.take(take);
    qb.skip(skip);
    qb.orderBy('alert.createdAt', 'DESC');
    return qb.getManyAndCount();
  },
  findOneAlertFromAnimal(animal: AnimalEntity) {
    return this.createQueryBuilder('alert')
      .where('alert.animal = :animal')
      .andWhere('alert.status = :status')
      .setParameter('animal', animal.id)
      .setParameter('status', EAlertStatus.NEW)
      .getOne();
  },
  lastAlerts(status: EAlertStatus, user: UserEntity) {
    return this.createQueryBuilder('alert')
      .leftJoinAndSelect('alert.user', 'user')
      .leftJoinAndSelect('alert.animal', 'animal')
      .leftJoinAndSelect('alert.location', 'location')
      .where('alert.status = :status')
      .andWhere('alert.user = :user')
      .setParameter('status', status)
      .setParameter('user', user.id)
      .orderBy('alert.createdAt', 'DESC')
      .limit(5)
      .getMany();
  },
});
