import typeormConfig from '../config/typeorm.config';
import UserEntity from '../entity/user.entity';

export const UserRepository = typeormConfig.getRepository(UserEntity).extend({
  findByEmailOrPhone(data: string) {
    return this.findOne({
      where: [{ email: data }, { phone: data }],
      relations: ['address', 'animals', 'animals.alerts', 'animals.photos'],
    });
  },
});
