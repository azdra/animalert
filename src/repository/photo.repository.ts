import typeormConfig from '../config/typeorm.config';
import PhotoEntity from '../entity/photo.entity';

export const PhotoRepository = typeormConfig.getRepository(PhotoEntity).extend({
  findOneByAnimalImage(id: number, photoId: number) {
    return this.createQueryBuilder('photo')
      .leftJoin('photo.animal', 'animal')
      .where('animal.id = :id')
      .andWhere('photo.id = :photoId')
      .setParameter('id', id)
      .setParameter('photoId', photoId)
      .getOne();
  },
});
