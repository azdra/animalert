import typeormConfig from '../config/typeorm.config';
import AddressEntity from '../entity/address.entity';

export const AddressRepository = typeormConfig.getRepository(AddressEntity);
