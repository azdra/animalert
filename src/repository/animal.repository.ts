import typeormConfig from '../config/typeorm.config';
import AnimalEntity from '../entity/animal.entity';
import UserEntity from '../entity/user.entity';

export const AnimalRepository = typeormConfig
  .getRepository(AnimalEntity)
  .extend({
    findOneAnimalFromUser: async (user: UserEntity, id: number) => {
      return AnimalRepository.createQueryBuilder('animal')
        .leftJoinAndSelect('animal.photos', 'photos')
        .leftJoinAndSelect('animal.user', 'user')
        .leftJoinAndSelect('user.address', 'address')
        .leftJoinAndSelect('animal.alerts', 'alerts')
        .where('animal.id = :id')
        .andWhere('animal.user = :userId')
        .setParameter('id', id)
        .setParameter('userId', user.id)
        .getOne();
    },
    findAnimalFromUser: async (user: UserEntity) => {
      return AnimalRepository.createQueryBuilder('animal')
        .leftJoinAndSelect('animal.photos', 'photos')
        .leftJoinAndSelect('animal.alerts', 'alerts')
        .andWhere('animal.user = :userId')
        .setParameter('userId', user.id)
        .getMany();
    },
  });
